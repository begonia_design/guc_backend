<?php
include "common.php";

use admin\lib\AdminPortal;
use admin\lib\Html;
use enum\BoardCloseType;
use enum\BoardType;
use model\InvCorBoardDao;
use model\InvCorBoardTitleDao;

$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

InvCorBoardTitleDao::setBackend();
InvCorBoardDao::setBackend();

$page->field->id();
$page->field->int('大類別' ,'title_id' ,1 ,11)->setEnum(InvCorBoardTitleDao::getTitleList())->setParentKey();
$page->field->txt('小類別標題' ,'title' ,0 ,200);
$page->field->int('收折方式', 'close_type', 1, 1)->setEnum(BoardCloseType::get());
$page->field->int('內容類型', 'type', 1, 1)->setEnum(BoardType::get());
$page->field->txt('內容' ,'content' ,0 ,51200);
$page->field->txt('表格內容' ,'table_content' ,0 ,51200);
$page->field->txt('Note標題','note',0,200);
$page->field->txt('Note連結','note_url',0,200);
$page->field->txt('Note內文','note_txt',0,200);
$page->field->status();
$page->field->sort('排序', 'sort', ($page->getMaxSort()+1));
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(1);
$page->search->setDefault('title_id', $_GET['title_id']);
$page->search->setDefaultSort('sort', 'ASC');
$page->search->select('title_id');
$page->search->select('status');

//表格===========================
//$page->table->txt('id', 70, 'text-center')->setTdClass('text-center');
//$page->table->txt('title_id');
$page->table->txt('title');
$page->table->select('close_type','100');
$page->table->txt('type');
$page->table->select('status');
$page->table->input('sort');
$page->table->mod();
$page->table->del();

$page->callback->setBeforeSaveTableSelect(function($key, $field, $value){
    if ($field == 'close_type') {
        $data = InvCorBoardDao::getByKey($key);
        if ($value == BoardCloseType::NOT_CLOSE) {
            $pData = InvCorBoardTitleDao::getByKey($data['title_id']);
            if ($pData['close_type'] == 1) {
                exit(json_encode('大類別設為全部收折，小類別只能設收折'));
            }
        }
        if ($value == BoardCloseType::CLOSE) {
            if (trim($data['title']) == "") {
                exit(json_encode("標題空白不可收折"));
            }
        }
    }
});

//新增刪除===========================
$page->editor->select2('title_id')->setOnchange("changeTitleId(this);");
$page->editor->text('title');
$page->editor->select('close_type');
$page->editor->select('type')->setOnchange("changeType(this);");
$page->editor->ckeditor('content');
$page->editor->txtHtml('table_content');
$page->editor->text('note');
$page->editor->text('note_url')->setMemo("註：連結若為前往 email 應用程式並帶入收件人，請輸入 \"mailto:該email資料\"，如 mailto:GUC@@guc-asic.com");
$page->editor->text('note_txt');
$page->editor->select('status');
$page->editor->text('sort');

$page->callback->setBeforeEditor(function($data){
    $data['table_content'] = getTableEditHtml($data['table_content']);
    return $data;
});

$page->callback->setAfterEditorHtml(function ($keyId) {
    ?>
    <script>
        function changeTitleId(t) {
            $.post("ajax.php?type=changeBoardCloseType",{id:$(t).val()},function(rs){
                if (rs > 0) {
                    $('#set-close_type').val(rs);
                }
            });
        }

        function changeType(t) {
            let t1 = $('.class-set-content');
            let t2 = $('.class-set-table_content,.class-set-note,.class-set-note_url,.class-set-note_txt');
            switch($(t).val()) {
                case '1':
                    t1.show();
                    t2.hide();
                    break;
                case '2':
                    t1.hide();
                    t2.show();
                    break;
                case '3':
                    t1.show();
                    t2.show();
                    break;
                default:
                    t1.hide();
                    t2.hide();
            }
        }
        changeType($('#set-type'));
        changeTitleId($('#set-title_id'));

        function deleteTr(t)
        {
            $(t).parent().parent().remove();
        }

        function addTr()
        {
            let rowCount = $('#rowCount').val();
            rowCount++;
            let html = "<tr><td><textarea class='form-control' placeholder='請輸入內容1' name='set[table_content][" + rowCount + "][0]'></textarea></td>";
            html += "<td><textarea class='form-control' placeholder='請輸入內容2' name='set[table_content][" + rowCount + "][1]'></textarea></td>";
            html += "<td><textarea class='form-control' placeholder='請輸入內容3' name='set[table_content][" + rowCount + "][2]'></textarea></td>";
            html += "<td><button class='btn btn-default' type='button' onclick='deleteTr(this);'>X</button></td></tr>";
            $('#table_editor').append(html);
            $('#rowCount').val(rowCount);
        }
    </script>
    <?php
});

$page->callback->setBeforeInsertSave(function($data, $customData){
    return checkAndReturn($data, []);
});
$page->callback->setBeforeUpdateSave(function($data, $originalData, $customData){
    return checkAndReturn($data, $originalData);
});

function checkAndReturn($data, $originalData){
    if ($data['close_type'] == BoardCloseType::NOT_CLOSE) {
        $pData = InvCorBoardTitleDao::getByKey($data['title_id']);
        if ($pData['close_type'] == \enum\BoardTitle::ALL_CLOSE) {
            exit(json_encode(array('err' => '大類別設為全部收折，小類別只能設收折')));
        }
    }
    if(($data['close_type'] == BoardCloseType::CLOSE) && trim($data['title']) == "") {
        exit(json_encode(array('err' => '選擇收折需填寫小類別標題')));
    }
    if(($data['type'] == BoardType::TEXT || $data['type'] == BoardType::TEXT_AND_TABLE) && trim($data['content']) == "") {
        exit(json_encode(array('err' => '內容不可空白')));
    }
    if(($data['type'] == BoardType::TABLE || $data['type'] == BoardType::TEXT_AND_TABLE)) {
        $tableData = [];
        if(is_array($data['table_content'])){
            foreach ($data['table_content'] as $k => $v) {
                if ($k == 0) {
                    foreach($v as $k1 => $v1) {
                        if (trim($v1) == "") {
                            exit(json_encode(array('err' => "表格標題{$k1}不可空白")));
                        }
                    }
                    $tableData[0] = $v;
                } else {
                    $txt = "";
                    foreach($v as $k1 => $v1) {
                        $txt .= trim($v1);
                    }
                    if (trim($txt) == "") {
                        exit(json_encode(array('err' => "表格不可以整行空白")));
                    }
                    $tableData[] = $v;
                }
            }
        }
        $data['table_content'] = json_encode($tableData);
    } else {
        if (isset($originalData['table_content'])) {
            $data['table_content'] = $originalData['table_content'];
        }
    }
    return $data;
}


function getTableEditHtml($json): string
{
    $rowCount = 1;
    $table = Html::table('table_editor');
    if ($json != '') {
        $arrayList = json_decode($json, true);
        if (is_array($arrayList)) {
            foreach ($arrayList as $row => $arr) {
                if ($row == 0) {
                    $table->setTh([
                        getThInputHtml(0, $arr[0]),
                        getThInputHtml(1, $arr[1]),
                        getThInputHtml(2, $arr[2]),
                        ''
                    ]);
                } else {
                    $table->setTd([
                        getTdInputHtml($row, 0, $arr[0]),
                        getTdInputHtml($row, 1, $arr[1]),
                        getTdInputHtml($row, 2, $arr[2]),
                        Html::btnClick('X','deleteTr(this);')
                    ]);
                }
            }
            $rowCount = count($arrayList);
        }
    } else {
        $table->setTh([
            getThInputHtml(0, ""),
            getThInputHtml(1, ""),
            getThInputHtml(2, ""),
            '']);
        $table->setTd([
            getTdInputHtml(1, 0, ''),
            getTdInputHtml(1, 1, ''),
            getTdInputHtml(1, 2, ''),
            Html::btnClick('X', 'deleteTr(this);')
        ]);
    }
    $html = $table->tableHtml() . Html::btnClick(Html::icon(Html::ICON_PLUS), 'addTr();');
    $html .= "<input type='hidden' id='rowCount' value='{$rowCount}'>";
    return $html;
}

function getThInputHtml($index, $value): string
{
    $s = $index + 1;
    return "<input class='form-control' type='text' placeholder='請輸入標題{$s}' name='set[table_content][0][{$index}]' value='{$value}'>";
}

function getTdInputHtml($row, $index, $value): string
{
    $s = $index + 1;
    return "<textarea class='form-control' placeholder='請輸入內容{$s}' name='set[table_content][{$row}][{$index}]'>{$value}</textarea>";
}