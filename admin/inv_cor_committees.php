<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->txt('標題' ,'title' ,1 ,200);
$page->field->txt('內容', 'html', 1, 51200, Common::getDefaultHtml());

$page->field->date('上架日' ,'start_date' ,10 , date("Y-m-d"));
$page->field->date('下架日' ,'end_date' ,0 );
$page->field->sort('排序', 'sort', ($page->getMaxSort()+1));
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(1);
$page->search->setDefaultSort('sort', 'ASC');
$page->search->text('title');

//表格===========================
//$page->table->txt('id', 70, 'text-center')->setTdClass('text-center');
$page->table->txt('title');
$page->table->txt('start_date');
$page->table->txt('end_date');
$page->table->input('sort');
$page->table->mod();
$page->table->del();

//新增刪除===========================
$page->editor->text('title');
$page->editor->ckeditor('html');
$page->editor->date('start_date');
$page->editor->date('end_date');
$page->editor->text('sort');

$page->callback->setBeforeInsertSave(function($data, $customData){
    Common::checkDateAndReturnErrMsg($data);
    return $data;
});

$page->callback->setBeforeUpdateSave(function($data, $originalData, $customData){
    Common::checkDateAndReturnErrMsg($data);
    return $data;
});

