<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->txt('類別' ,'title' ,1 ,50);
$page->field->txt('內容' ,'value' ,1 ,100);
\model\InvFinMonthlyDao::setBackend();
$yearList = \model\InvFinMonthlyDao::getAllYear();
foreach ($yearList as $y) {
    $page->field->custom($y.'年' ,'v'.$y ,0 ,50);
}
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(0);


//表格===========================
$page->table->txt('title');
$page->table->txt('value');
$page->table->txt('update_time');
$page->table->mod();

$page->callback->setBeforeTableData(function($row){
    if ($row['id'] == 2) {
        $yoyList = json_decode($row['value']);
        $html = "";
        foreach($yoyList as $k => $v) {
            $html .= "<p>{$k}年：{$v}</p>";
        }
        $row["value"] = $html;
    }
    return $row;
});
//新增刪除===========================
$page->editor->txt('title');

if ($_POST['id'] == 1 || $_POST['set']['id'] == 1) {
    $page->editor->file('value', 'pdf,xls,xlsx');
}
if ($_POST['id'] == 2 || $_POST['set']['id'] == 2) {
    $page->callback->setBeforeEditor(function($data){
        $yoyList = json_decode($data['value']);
        foreach($yoyList as $k => $v) {
            $data['v'.$k] = $v;
        }
        return $data;
    });
    foreach ($yearList as $y) {
        $page->editor->text('v'.$y);
    }
}


$page->callback->setBeforeUpdateSave(function($data, $originalData, $customData)use($yearList){
    if ($originalData['id'] == 2) {
        $yoyData = [];
        foreach($yearList as $y) {
            $yoyData[$y] = $_POST['set']['v'.$y];
        }
        $data['value'] = json_encode($yoyData);
    }
    return $data;
});

