<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->int('年份' ,'year' ,4 ,4);
$page->field->int('季度' ,'quarterly' ,1 ,1)->setEnum(\enum\Quarterly::get());
$page->field->int('金額' ,'value' ,1 ,11);
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(0);
$page->search->setDefaultSort('year', 'DESC');

//表格===========================
$page->table->txt('year');
$page->table->txt('quarterly');
$page->table->txt('value');
$page->table->mod();
$page->table->del();

//新增刪除===========================
$page->editor->text('year');
$page->editor->select('quarterly');
$page->editor->text('value');

