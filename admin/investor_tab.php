<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->int('類別', 'type', 1, 1)->setEnum(\enum\InvestorTabType::get());
$page->field->txt('Key', 'key_txt', 1, 20);
$page->field->txt('標題' ,'title' ,1 ,200);
$page->field->txt('內容', 'html', 1, 51200);
$page->field->status();
$page->field->sort('排序', 'sort', ($page->getMaxSort()+1));
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(0);
$page->search->setDefaultSort('sort', 'ASC');
$page->search->select('type');
$page->search->text('title');
$page->search->select('status');

//表格===========================
//$page->table->txt('id', 70, 'text-center')->setTdClass('text-center');
$page->table->txt('type');
$page->table->txt('title');
$page->table->select('status');
$page->table->input('sort');
$page->table->mod();
$page->table->del();

//新增刪除===========================
$page->editor->select('type');
$page->editor->text('key_txt');
$page->editor->text('title');
$page->editor->ckeditor('html');
$page->editor->select('status');
$page->editor->text('sort');

