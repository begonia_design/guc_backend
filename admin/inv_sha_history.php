<?php
include "common.php";

use admin\lib\AdminPortal;
use lib\Config;
use lib\Db;

$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->int('Period' ,'year' ,4 ,4);
$page->field->int('Stock Dividend', 'stock', 0, 40);
$page->field->int('Cash Dividend	', 'cash', 0, 40);
$page->field->date('Ex-Dividend Date' ,'dividend' ,0);
$page->field->date('Record Date' ,'record' ,0);
$page->field->date('Distribution Date' ,'distribution' ,0);
$page->field->date('Shareholders\' Meeting' ,'meeting' ,0);
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(1);
$page->search->setDefaultSort('year', 'DESC');
$page->search->text('year');

//表格===========================
$page->table->txt('year');
$page->table->txt('stock');
$page->table->txt('cash');
$page->table->txt('dividend');
$page->table->txt('record');
$page->table->txt('distribution');
$page->table->txt('meeting');
$page->table->mod();
$page->table->del();

//新增刪除===========================
$page->editor->text('year');
$page->editor->text('stock');
$page->editor->text('cash');
$page->editor->date('dividend');
$page->editor->date('record');
$page->editor->date('distribution');
$page->editor->date('meeting');