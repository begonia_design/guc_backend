<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->file('圖片', 'img', true);
$page->field->file('圖片(小)', 'img2', true);
$page->field->txt('標題' ,'title' ,1 ,50);
$page->field->txt('次標題' ,'title2' ,0 ,80);
$page->field->txt('連結' ,'url' ,0 ,200);
//$page->field->date('結束日期', 'end_date', 0 );
$page->field->status();
$page->field->sort('排序', 'sort', ($page->getMaxSort()+1));
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(0);
$page->search->setDefaultSort('sort', 'ASC');
$page->search->text('title');
$page->search->select('status');

//表格===========================
//$page->table->txt('id', 70, 'text-center')->setTdClass('text-center');
$page->table->txt('title');
$page->table->img('img');

$page->table->select('status');
$page->table->input('sort');
$page->table->mod();
$page->table->del();

//新增刪除===========================
$page->editor->text('title')->setMemo("不可大於 50 個字");
$page->editor->text('title2')->setMemo("不可大於 80 個字");
$page->editor->file('img')->setMemo("建議尺寸 1300*870");
$page->editor->file('img2')->setMemo("建議尺寸 1125*1200");;
$page->editor->text('url');
$page->editor->select('status');
$page->editor->text('sort');
