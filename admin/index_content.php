<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->file('圖片', 'img', true);
$page->field->txt('連結' ,'url' ,0 ,200);
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(0);
$page->search->setDefaultSort('id', 'DESC');

//表格===========================
$page->table->txt('url');
$page->table->img('img');
$page->table->mod();

//新增刪除===========================
$page->editor->file('img')->setMemo("圖片建議尺寸 1920*1380，圖片左側 60% 為文字放置區域");
$page->editor->text('url');

