<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->txt('內容' ,'html' ,1 ,102400);
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(1);

//表格===========================
$page->table->txt('html');
$page->table->txt('update_time');
$page->table->mod();

$page->editor->ckeditor('html');


