<?php
include "common.php";

use admin\lib\AdminPortal;
use lib\Config;
use lib\Db;

$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->int('Number' ,'number' ,1 ,4);
$page->field->txt('Name of Shareholder', 'name', 0, 9999);
$page->field->int('Shareholding (Shares)', 'shares', 0, 40);
$page->field->int('Shareholding ratio (%)' ,'ratio' ,0, 40);
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(2);
$page->search->setDefaultSort('number', 'ASC');

//表格===========================
$page->table->txt('number');
$page->table->txt('name');
$page->table->txt('shares');
$page->table->txt('ratio');
$page->table->mod();
$page->table->del();

//新增刪除===========================
$page->editor->text('number');
$page->editor->text('name');
$page->editor->text('shares');
$page->editor->text('ratio');

$page->callback->setTopButton(function(){
    $data = \model\AdminConfigDao::getMajorLastUpdate($_SESSION['lang']);
    ?>
    <div class="form-inline col-lg-5 text-left" role="form">
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon">更新日期</div>
                <input id="quick-update" name="quick_update" data-date-format="YYYY-MM-DD" class="form-control" type="text" placeholder="更新日期" value="<?php echo $data ?>">
            </div>
        </div>
        <button type="button" class="btn btn-default" onclick="quickUpdate();"><span class="glyphicon"></span>更新</button>
    </div>
    <script>
        $(function(){
            $('#quick-update').datetimepicker({pickTime: false});
        })
        function quickUpdate(){
            $.post("ajax.php?type=quickUpdate",{'date' : $('#quick-update').val()},function(rs){
                if (rs.status === 'err' || rs.status === 'success') {
                    alert(rs.msg);
                }
            },'json');
        }
    </script>
    <?php
});
