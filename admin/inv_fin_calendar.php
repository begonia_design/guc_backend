<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->datetime('日期' ,'date' ,1,20);
$page->field->txt('內容', 'event', 1, 200);
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(1);
$page->search->setDefaultSort('date', 'ASC');

//表格===========================
$page->table->txt('date');
$page->table->txt('event');
$page->table->txt('update_time');
$page->table->mod();
$page->table->del();

//新增刪除===========================
$page->editor->datetime('date');
$page->editor->text('event');