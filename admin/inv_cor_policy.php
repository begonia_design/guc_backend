<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->txt('標題' ,'title' ,1 ,200);
$page->field->file('檔案' ,'file' ,true);
$page->field->date('上架日' ,'start_date' ,10 , date("Y-m-d"));
$page->field->date('下架日' ,'end_date' ,0 );
$page->field->createTime();
$page->field->updateTime();
$page->field->sort('排序', 'sort', ($page->getMaxSort()+1));

//搜尋===========================
$page->setSearchStyle(1);
$page->search->setDefaultSort('sort', 'ASC');
$page->search->text('title');

//表格===========================
$page->table->txt('title');
$page->table->txt('start_date');
$page->table->txt('end_date');
$page->table->txt('update_time');
$page->table->input('sort');
$page->table->mod();
$page->table->del();

//新增刪除===========================
$page->editor->text('title');
$page->editor->file('file','xls,xlsx,pdf');
$page->editor->date('start_date');
$page->editor->date('end_date');
$page->editor->text('sort');

$page->callback->setBeforeInsertSave(function($data, $customData){
    Common::checkDateAndReturnErrMsg($data);
    return $data;
});

$page->callback->setBeforeUpdateSave(function($data, $originalData, $customData){
    Common::checkDateAndReturnErrMsg($data);
    return $data;
});