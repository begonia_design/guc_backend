<?php
include "common.php";

if ($_GET['type']) {
    $functionName = $_GET['type'];
    if (function_exists($functionName)) {
        $functionName();
    }
    exit;
}


function changeBoardCloseType()
{
    $id = $_POST['id'];
    \model\InvCorBoardTitleDao::setBackend();
    $data = \model\InvCorBoardTitleDao::getByKey($id);
    switch($data['close_type']) {
        case \enum\BoardTitle::ALL_CLOSE:
            echo \enum\BoardCloseType::CLOSE;
            exit;
            break;
    }
    echo 0;
}

//inv_sha_major.php
function quickUpdate()
{
    $date = $_POST['date'];
    if ($date != date("Y-m-d", strtotime($date))) {
        responseError("日期格式錯誤");
    }

    \model\AdminConfigDao::updateMajorLastUpdate($_SESSION['lang'], $date);
    responseSuccess("更新成功");
}


function getIndexNews(){
    \model\NewsDao::setBackend();
    \model\NewsSpotlightDao::setBackend();
    $data = \model\IndexSort::getIndexSortForAdmin();
    exit(json_encode($data));
}

function saveIndexNewsSort(){
    $sortArray = $_POST['sort'];
    \model\NewsDao::setBackend();
    \model\NewsSpotlightDao::setBackend();
    foreach ($sortArray as $sort => $idString) {
        $typeId = explode(',',$idString);
        if ($typeId[0] == 'news') {
            \model\NewsDao::updateIndexSort($typeId[1],($sort+1));
        } else {
            \model\NewsSpotlightDao::updateIndexSort($typeId[1],($sort+1));
        }
    }
    responseSuccess("更新成功");
}


function responseSuccess($msg = null)
{
    response('success', $msg);
}

function responseError($msg = null)
{
    response('err', $msg);
}

function response($status, $msg = null)
{
    $rs = ['status' => $status];
    if ($msg != null) {
        $rs['msg'] = $msg;
    }
    exit(json_encode($rs));
}