//顯示表格資料
function table(){
	$.post('<?=$this->name?>.php?type=show',$('#form').serializeArray(),function(rs){
		$('#table_area tbody').html(rs.tbody);
		var star = rs.pagebar.data_star + 1;
		var end = rs.pagebar.data_end;
		var count = rs.pagebar.data_count;
		var page_now = rs.pagebar.page_now;
		var page_count = rs.pagebar.page_count;
		$('#table_info_page').text('第 '+star+' 到 '+end+' 筆，共 '+count+' 筆');
		$('input[name=page_now]').val(page_now);
		$('#orderby_orderby').val(rs.orderby['orderby']);
		$('#orderby_sort').val(rs.orderby['sort']);
		
		var css = (rs.orderby['sort']=='ASC')?'sorting_asc':'sorting_desc';
		var th_class = rs.orderby['orderby'];
		$('#table_area th>div.orderby').removeClass("sorting_asc sorting_desc").addClass("sorting");
		$('#table_area th>div.orderby_'+th_class).removeClass("sorting").addClass(css);
		createPageBar(page_count);
		
		return false;
	},'json');
}

function createPageBar(page_count){
	var page_now = $('input[name=page_now]').val();
	var star = parseInt(page_now) - 3;
	var end = parseInt(page_now) + 3;
	var prev = parseInt(page_now) - 1;
	var next = parseInt(page_now) + 1
	star = (star < 1)?1:star;
	end  = (end > page_count)?page_count:end;
	var css = (page_now==1)?'class="disabled"':'';
	var js = (page_now==1)?'':'onclick="page('+prev+')"';
	var html = '<li '+css+'><a href="javascript:void(0);" '+js+'>&laquo;</a></li>';
	for(i=star;i<=end;i++){
		css = (i==page_now)?'class="active"':'';
		js = (i==page_now)?'':'onclick="page('+i+')"';
		html += '<li '+css+'><a href="javascript:void(0);" '+js+'>'+i+'</a></li>';
	}
	css = (end==page_now)?'class="disabled"':'';
	js = (end==page_now)?'':'onclick="page('+next+')"';
	html += '<li '+css+'><a href="javascript:void(0);" '+js+'>&raquo;</a></li>';
	
	$('#table_info ul.pagination').html(html);	
}

function page(no){
	$('input[name=page_now]').val(no);
	table();
}

function orderby(id){
	var orderby_sort = 'ASC';
	if($('#orderby_orderby').val()==id){
		orderby_sort = ($('#orderby_sort').val()==orderby_sort)?'DESC':'ASC';
	}
	$('#orderby_orderby').val(id);
	$('#orderby_sort').val(orderby_sort);
	table();
}

//新增或修改頁面
function btn_show_set(id){
	$('#form').hide();
	$('#form_set_area').show();	
	
	var txt = (id==0)?'新增資料':'修改資料';
	$('#form_set_area span.set_title').text(txt);
	$.post('<?=$this->name?>.php?type=set',{id:id},function(rs){
		if(rs.err != undefined){
			alert(rs.err);	
		}else{
			//alert(rs.html);
			$('#form_set_html').html(rs.html);
		}
	},'json');
}

//返回資料頁面
function btn_show_data(){
	$('#form_set_area').hide();
	$('#form').show();	
}

//刪除資料
function btn_del(id){
	if(confirm("確定刪除？")){
		$.post('<?=$this->name?>.php?type=del',{id:id},function(rs){
			if(rs != ''){
				alert(rs);
			}
			table();
		},'json');
	}
}

//建立ckeditor編輯器
function ckeditor(id){
	if( CKEDITOR.instances[id] ){ 
		CKEDITOR.remove(CKEDITOR.instances[id]); 
	}
	CKEDITOR.replace(id);
} 

//建立上傳功能
function fileupload(id,file_type,del){
	var FU_id = "FU-"+id;
	var btn = (del==0)?'':'<button class="FU-del btn btn-warning" type="button" onclick="FU_del(\''+id+'\')">刪除</button>';
	
	var file_type = (file_type==undefined)?'':file_type;
	var file_type_arr = (file_type=='')?'':file_type.split(',');
	var file_type_txt = (file_type=='')?'不限':file_type;
		
	var uploader = new qq.FileUploader({
		element: document.getElementById(FU_id),
		action: 'upload.php',
		allowedExtensions: file_type_arr,
		debug: true,
		onSubmit: function(uploadid, fileName){
			$('#'+FU_id+' span.qq-upload-list>div.btn').remove();
			$('#'+FU_id+' button.FU-del').remove();
		},
		onProgress: function(uploadid, fileName, loaded, total){
			
		},
		onComplete: function(uploadid, fileName, responseJSON){
			var filename = responseJSON.filename;
			$('#'+FU_id+' .qq-upload-file').text(filename);	
			$('#'+id).val(filename);
			$('#'+FU_id+' .qq-upload-list').after(btn);
		},
		onCancel: function(uploadid, fileName){},
		showMessage: function(message){
			alert(message);
		}
	});
	
	//如果有資料
	if($('#'+id).val()!=''){
		var html = '<div class="btn btn-info qq-upload-success">' +
			'<span class="qq-upload-file">'+$('#'+id).val()+'</span></div>';
		$('#'+FU_id+' span.qq-upload-list').html(html);
		$('#'+FU_id+' .qq-upload-list').after(btn);
	}
}

//刪除檔案功能
function FU_del(id){
	var FU_id = "FU-"+id;
	$('#'+FU_id+' span.qq-upload-list>div.btn').remove();
	$('#'+FU_id+' button.FU-del').remove();
	$('#'+id).val('');
}

//修改表單傳送
function form_set_submit(){
	$('#form_set textarea').each(function() {
		var id = $(this).attr('id');
		if( CKEDITOR.instances[id] ){ 
			CKEDITOR.instances[id].updateElement();
		}
	});
	
	$.post('<?=$this->name?>.php?type=save',$('#form_set').serializeArray(),function(rs){
		if(rs.err != undefined){
			alert(rs.err);
			if(rs.id != undefined){
				$('#Set_'+rs.id).focus();	
			}	
		}else{
			alert(rs.msg);
			btn_show_data();
			table();
		}
	},'json');	
	return false;
}

//排序存檔
function btn_table_save(){
	$.post('<?=$this->name?>.php?type=table_save',$('#table_area input'),function(rs){
		alert(rs);
	},'json');	
}

$(function(){
	
	$(document).ajaxStart(function(){
		var html = "<div id=\"ajax_loading\">LOADING…</div>";
		var width = $("#page-wrapper").width();
		var height = $("#page-wrapper").height();
		$("#page-wrapper").prepend(html);
		$("#ajax_loading").width(width);
		$("#ajax_loading").height(height);
		$("#ajax_loading").css("opacity","0.5")
		$("#ajax_loading").css("padding-top",Math.round(height/2));
	}).ajaxStop(function(){
		$("#ajax_loading").remove();
	}).ajaxError(function (event, jqxhr, settings) {
		alert('ajax錯誤!!');
	})

	
})
	