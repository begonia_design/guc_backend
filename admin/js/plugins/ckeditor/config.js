/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'zh';
	// config.uiColor = '#AADC6E';
	config.filebrowserBrowseUrl = 'ckfinder/ckfinder.html';
	config.filebrowserImageBrowseUrl = 'ckfinder/ckfinder.html?Type=Images';
	config.filebrowserFlashBrowseUrl = 'ckfinder/ckfinder.html?Type=Flash';
	config.filebrowserUploadUrl = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
	config.filebrowserImageUploadUrl = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
	config.filebrowserFlashUploadUrl = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
	//config.toolbar = 'Full';
	config.toolbar = 'TadToolbar';
	
    config.toolbar_TadToolbar = 
	[
        //['Source','-','Templates','-','Cut','Copy','Paste'],
        //['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
        //['Link','Unlink','Image'],
        ////['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
		//['Format','Styles'],
		////'/',
		////['TextColor', 'BGColor'],
        //['NumberedList','BulletedList'],
        //['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		////['Maximize','ShowBlocks']
		['Source','-','Templates','-','Cut','Copy','Paste'],
		['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		['Link','Unlink','Anchor'],
		['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
		'/',
		['Styles', 'Format', 'Font', 'FontSize'],
		['TextColor', 'BGColor'],
		['Bold','Italic','Underline','Strike','-','Subscript','Superscript','CreateDiv',],
		['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
		['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    ];
	
	config.toolbarCanCollapse = true;
	config.allowedContent=true;
	//config.contentsCss = ['../tw/css/style.css'];
	config.font_names = "Gotham-Light;Gotham-Book;Gotham-Medium; Arial/Arial, Helvetica, sans-serif;Comic Sans MS/Comic Sans MS, cursive;Courier New/Courier New, Courier, monospace;Georgia/Georgia, serif;Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;Tahoma/Tahoma, Geneva, sans-serif;Times New Roman/Times New Roman, Times, serif;Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;Verdana/Verdana, Geneva, sans-serif";
};

CKEDITOR.stylesSet.add( 'my_styles', [
	// Block-level styles.
	{ name: 'Blue Title', element: 'h2', styles: { color: 'Blue' } },
	{ name: 'Red Title',  element: 'h3', styles: { color: 'Red' } },

	// Inline styles.
	{ name: 'CSS Style', element: 'span', attributes: { 'class': 'my_style' } },
	{ name: 'Marker: Yellow', element: 'span', styles: { 'background-color': 'Yellow' } }
]);