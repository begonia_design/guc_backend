<?php
include "common.php";

use admin\lib\AdminPortal;
use admin\lib\Html;
use enum\BoardTitle;
use model\InvCorBoardDao;

$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

InvCorBoardDao::setBackend();

$page->field->id();
$page->field->txt('大類別標題' ,'title' ,1 ,200);
$page->field->int('收折方式', 'close_type', 1, 1)->setEnum(BoardTitle::get());
$page->field->status();
$page->field->sort('排序', 'sort', ($page->getMaxSort()+1));
$page->field->createTime();
$page->field->updateTime();
$page->field->custom('小類別','board');

//搜尋===========================
$page->setSearchStyle(1);
$page->search->setDefaultSort('sort', 'ASC');
$page->search->text('title');
$page->search->select('status');

//表格===========================
//$page->table->txt('id', 70, 'text-center')->setTdClass('text-center');
$page->table->txt('title');
$page->table->select('close_type','100');
$page->table->select('status');
$page->table->input('sort');
$page->table->linkBtn('管理小類別', '管理小類別','inv_cor_board.php?title_id=');
$page->table->mod();
$page->table->del();

$page->callback->setBeforeSaveTableSelect(function($key, $field, $value){
    if ($field == 'close_type' && $value == BoardTitle::ALL_CLOSE) {
        $data = InvCorBoardDao::getByTitleId($key);
        if ($data) {
            foreach($data as $k => $v) {
                if (trim($v['title']) == "") {
                    exit(json_encode("小分類標題空白不可全部收折"));
                }
            }
            InvCorBoardDao::updateCloseTypeByTitleId($key);
        }
    }
});

//新增刪除===========================
$page->editor->text('title');
$page->editor->select('close_type');
$page->editor->select('status');
$page->editor->text('sort');

if($_POST['id'] > 0) {
    $page->editor->custom('board', function($keyId, $value){
        $table = Html::table('table_editor')->setTh(['類型','內容','收折','排序','管理']);

        $data = InvCorBoardDao::getByTitleId($keyId);
        if ($data){
            foreach ($data as $k => $v) {
                $table->setTd([
                    \enum\BoardType::get($v['type']),
                    $v['title'],
                    \enum\BoardCloseType::get($v['close_type']),
                    $v['sort'],
                    Html::btnClick(Html::icon(Html::ICON_PENCIL),"getEditor('{$k}','inv_cor_board.php');",Html::BTN_WARNING)
                ]);
            }
        }
        $html = $table->tableHtml();
        $html .= Html::btnClick('新增小類別', "getEditor('0','inv_cor_board.php');")." ";
        $html .= Html::btnLink('管理小類別', "inv_cor_board.php?title_id=".$keyId);
        return $html;
    });
}

$page->callback->setBeforeUpdateSave(function($data, $originalData, $customData){
    if ($data['close_type'] == BoardTitle::ALL_CLOSE) {
        $data1 = InvCorBoardDao::getByTitleId($originalData['id']);
        if ($data1) {
            foreach ($data1 as $k => $v) {
                if (trim($v['title']) == "") {
                    exit(json_encode(array('err' => "小分類標題空白不可全部收折")));
                }
            }
        }
    }
    return $data;
});

$page->callback->setAfterUpdateSave(function($data, $originalData, $customData){
    if ($data['close_type'] == BoardTitle::ALL_CLOSE) {
        InvCorBoardDao::updateCloseTypeByTitleId($originalData['id']);
    }
});

