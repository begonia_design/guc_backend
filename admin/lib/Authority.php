<?php
namespace admin\lib;

use Complex\Exception;
use lib\Db;

class Authority{

    private $authority = array();

    function __construct($fileName, $userId){

        if($fileName == 'index' || $fileName == 'admin_password') {
            $sql = "SELECT 1 AS p_read,1 AS p_update,name FROM admin_user WHERE id = :id";
            $bind = array(':id' => $userId);
            $this->authority = Db::getBindRowData($sql,$bind);
        } else {
            $sql = "
			SELECT
				a.p_read,
				a.p_insert,
				a.p_delete,
				a.p_update,
				u.name
			FROM admin_power a
			JOIN admin_user u ON u.id = a.user_id
			WHERE u.status = 1
			  AND a.menu = :file
			  AND u.id = :user_id";
            $bind = array(':file' => $fileName , ':user_id' => $userId);
            $this->authority = Db::getBindRowData($sql,$bind);
        }

    }

    public function isRead(){
        return ($this->authority['p_read'] == 1)?true:false;
    }

    public function isInsert(){
        return ($this->authority['p_insert'] == 1)?true:false;
    }

    public function isUpdate(){
        return ($this->authority['p_update'] == 1)?true:false;
    }

    public function isDelete(){
        return ($this->authority['p_delete'] == 1)?true:false;
    }

    public function getUserName(){
        return $this->authority['name'];
    }

}