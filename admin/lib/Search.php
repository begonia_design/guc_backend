<?php
namespace admin\lib;

class Search
{

    private $field;
    private $sql;
    private $dbKey;
    private $post;
    private $html;
    private $defaultHtml;
    private $js;

    function __construct(Field $field, Sql $sql)
    {
        $this->post = $_POST['search'];
        if ($_GET['type'] == 'export') {
            $this->post = $_GET['search'];
        }
        $this->field = $field;
        $this->sql = $sql;
        $this->dbKey = $field->getDbKey();
    }

    /**
     * 設定預設排序
     * @param $orderBy
     * @param $sortBy
     */
    public function setDefaultSort($orderBy, $sortBy)
    {
        $orderBy = ($this->field->exist($orderBy)) ? $orderBy : $this->dbKey;
        $this->sql->setOrderBy($orderBy, $sortBy);
        return $this;
    }

    public function setSecondSort($secondSort)
    {
        $this->sql->setSecondSort($secondSort);
    }


    //設定搜尋預設值
    public function setDefault($field, $value)
    {
        if (!isset($this->post[$field])) {
            $this->post[$field] = $value;
            $this->defaultHtml .= "<input type='hidden' name='default[{$field}]' value='{$value}'>";
        }
    }

    //生成文字搜尋
    public function text($field, $type = 'LIKE')
    {
        $title = $this->field->getTitle($field);
        if (isset($this->post[$field]) && trim($this->post[$field]) != '') {
            $value = $this->post[$field];
            $value = ($type == 'LIKE') ? "%{$value}%" : $value;
            $this->sql->setSql("{$field} {$type} :{$field}", ":{$field}", $value);
        }

        $html = '<input id="search-' . $field . '" name="search[' . $field . ']" class="form-control" type="text" placeholder="輸入關鍵字.." value="' . $value . '">';

        $this->add($title, $html);
        return $this;
    }

    //生成下拉選單搜尋
    public function select($field, $readonly = false)
    {
        $value = "";
        $title = $this->field->getTitle($field);
        if (isset($this->post[$field]) && $this->post[$field] != "" && $this->field->enumExist($field)) {
            $value = $this->post[$field];
            $this->sql->setSql("{$field} = :{$field}", ":{$field}", $value);
        }

        $option_html = '<option value="">全部</option>';
        foreach ($this->field->getEnum($field) as $k => $v) {
            $selected = ($value != "" && $k == $value) ? 'selected' : '';
            $option_html .= "<option value=\"{$k}\" {$selected}>{$v}</option>";
        }

        $readonlyHtml = ($readonly)?"disabled":"";
        $html = '
			<select id="search-' . $field . '" name="search[' . $field . ']" class="form-control" '.$readonlyHtml.'>
				' . $option_html . '
			</select>';

        $this->add($title, $html, ($field == $this->field->getParentKey()));
        return $this;
    }

    public function dateEqual($field)
    {
        $title = $this->field->getTitle($field);
        if (isset($this->post[$field]) && $this->post[$field] != "") {
            $value = $this->post[$field];
            $this->sql->setSql("{$field} = :{$field}", ":{$field}", $value);
        }
        $html = "<input id=\"search-{$field}\" name=\"search[{$field}]\" class=\"form-control\" type=\"text\" placeholder=\"輸入日期\" value=\"{$value}\">";
        $this->js = "$('#search-{$field}').datetimepicker({pickTime: false});";
        $this->add($title, $html);
        return $this;
    }


    //生成日期範圍搜尋
    public function dateBetween($field, $field2 = null)
    {
        $value = "";
        $title = $this->field->getTitle($field);

        if (isset($this->post[$field . "_B1"]) && $this->post[$field . "_B1"] != "" && isset($this->post[$field . "_B2"]) && $this->post[$field . "_B2"] != "") {
            $value1 = $this->post[$field . "_B1"];
            $value2 = $this->post[$field . "_B2"];
            $sql = "{$field} BETWEEN :{$field}_B1 AND :{$field}_B2";
            if ($field2) {
                $sql = "({$sql} OR {$field2} BETWEEN :{$field}_B3 AND :{$field}_B4)";
                $this->sql->setSearchBind(":{$field}_B3", $value1);
                $this->sql->setSearchBind(":{$field}_B4", $value2);
            }
            $this->sql->setSearchSql($sql);
            $this->sql->setSearchBind(":{$field}_B1", $value1);
            $this->sql->setSearchBind(":{$field}_B2", $value2);
        }

        $html = '<div class="form-group">
			<div class="input-group">
			  <div class="input-group-addon">' . $title . '</div>
			  <input id="S_' . $field . '_B1" name="search[' . $field . '_B1]" class="form-control" data-date-format="YYYY-MM-DD" type="text" placeholder="輸入開始日期" value="' . $value1 . '">
			  <span class="input-group-btn" style="width:0px;"></span>
			  <input id="S_' . $field . '_B2" name="search[' . $field . '_B2]" class="form-control" data-date-format="YYYY-MM-DD" type="text" placeholder="輸入結束日期" value="' . $value2 . '">
			</div>
		  </div>
		  <script>$(function(){$("#S_' . $field . '_B1,#S_' . $field . '_B2").datetimepicker({pickTime: false}); })</script>
		  ';
        $this->html .= $html;
        return $this;
    }


    //生成搜尋區塊
    public function add($title, $input, $isParentKey = false)
    {
        $class = ($isParentKey)?" has-success":"";
        $html = '
		  <div class="form-group">
			<div class="input-group'.$class.'">
			  <div class="input-group-addon">' . $title . '</div>
			  ' . $input . '
			</div>
		  </div>';
        $this->html .= $html;
    }

    //生成文字搜尋
    public function hidden($field)
    {
        $value = '';
        if (isset($this->post[$field]) && trim($this->post[$field]) != '') {
            $value = $this->post[$field];
            $this->sql->setSql("{$field} = :{$field}", ":{$field}", $value);
        }
        $html = '<input type="hidden" id="search-' . $field . '" name="search[' . $field . ']" value="' . $value . '">';

        $this->html .= $html;
        return $this;
    }

    //自訂搜尋區塊HTML
    public function addHtml($html)
    {
        $this->html .= $html;
    }

    //輸出搜尋區塊
    public function output($searchStyle = 1)
    {
        $searchButton = "";
        if ($this->html != '') {
            $searchButton = '<button type="button" class="btn btn-default" onClick="getTable(1)"><span class="glyphicon glyphicon-search"></span> 搜尋</button>';
        }
        $width = ($searchStyle == 1) ? 8 : 12;
        ?>
        <div class="col-lg-<?php echo $width; ?> search_area">
            <div class="form-inline" role="form">
                <?php echo $this->html; ?>
                <?php echo $searchButton; ?>
            </div>
        </div>
        <scirpt><?php echo $this->js; ?></scirpt>
        <?php
    }

    public function outputDefault()
    {
        if ($this->field->getParentKey() && isset($_GET[$this->field->getParentKey()])) {
            $this->setDefault($this->field->getParentKey(), $_GET[$this->field->getParentKey()]);
        }
        ?>
        <form id="form-default">
            <input type="hidden" name="id">
            <?php echo $this->defaultHtml;?>
        </form>
        <?php
    }

}