<?php
namespace admin\lib;

class Field
{

    private $data;
    private $key;
    private $enum;
    private $undefinedEnumSql;
    private $optionGroup;
    private $preg;
    private $dbName;
    private $dbKey;
    private $parentKey;

    function __construct($dbName, $dbKey = 'id')
    {
        $this->dbName = $dbName;
        $this->dbKey = $dbKey;
    }

    public function setDbName($dbName)
    {
        $this->dbName = $dbName;
    }

    public function setDbKey($dbKey)
    {
        $this->dbKey = $dbKey;
    }

    public function getDbName()
    {
        return $this->dbName;
    }

    public function getDbKey()
    {
        return $this->dbKey;
    }


    //設定欄位資訊
    public function set($title, $field, $data_type, $min, $max, $default = null)
    {
        $this->key = $field;
        $this->data[$field] = array($title, $data_type, $min, $max, $default);
        return $this;
    }

    //設定文字欄位
    public function txt($title, $field, $min = 0, $max = 255, $default = null)
    {
        return $this->set($title, $field, 'txt', $min, $max, $default);
    }


    //設定數字欄位
    public function int($title, $field, $min, $max, $default = '')
    {
        return $this->set($title, $field, 'int', $min, $max, $default);
    }

    //設定檔案欄位
    public function file($title, $field, $required = false)
    {
        $min = ($required) ? 1 : 0;
        return $this->set($title, $field, 'txt', $min, 100, null);
    }

    //設定Email欄位
    public function email($title, $field, $min, $max, $default = null)
    {
        return $this->set($title, $field, 'email', $min, $max, $default);
    }

    //設定ID欄位
    public function id($title = '編號', $field = 'id')
    {
        return $this->set($title, $field, 'int', 1, 11, null);
    }

    //設定日期欄位
    public function date($title, $field, $min = 0, $default = null)
    {
        return $this->set($title, $field, 'date', $min, 10, $default);
    }

    //設定日期時間欄位
    public function datetime($title, $field, $min, $max, $default = null)
    {
        return $this->set($title, $field, 'datetime', $min, $max, $default);
    }

    //設定狀態欄位
    public function status($title = '狀態', $field = 'status', $default = 1)
    {
        return $this->set($title, $field, 'txt', 1, 2, $default);
    }

    //設定排序欄位
    public function sort($title = '排序', $field = 'sort', $default = "0")
    {
        return $this->set($title, $field, 'txt', 0, 5, $default);
    }

    public function createTime($title = '建立時間', $field = 'create_time')
    {
        return $this->set($title, $field, 'datetime', 0, 20, date("Y-m-d H:i:s"));
    }

    public function updateTime($title = '更新時間', $field = 'update_time')
    {
        return $this->set($title, $field, 'datetime', 0, 20, date("Y-m-d H:i:s"));
    }

    //自訂非資料庫欄位
    public function custom($title, $field, $min = 0, $max = 999, $default = null)
    {
        return $this->set($title, $field, 'custom', $min, $max, $default);
    }

    //設定欄位關連
    public function setEnum(array $data, $field = null)
    {
        $field = ($field === null) ? $this->key : $field;
        $this->enum[$field] = $data;
        return $this;
    }

    //設定上層key值
    public function setParentKey($field = null)
    {
        $field = ($field === null) ? $this->key : $field;
        $this->parentKey = $field;
        return $this;
    }

    /**
     * @param String $sql = SELECT xxx AS value FROM xxx WHERE xx = :id
     * @param null $field
     */
    public function setUndefinedEnum($sql, $field = null)
    {
        $field = ($field === null) ? $this->key : $field;
        $this->undefinedEnumSql[$field] = $sql;
    }


    //設定二階選單
    public function setOptionGroup(array $data, $field = null)
    {
        $field = ($field === null) ? $this->key : $field;
        $this->optionGroup[$field] = $data;
    }

    //設定檢查規則
    public function setPreg($preg, $error_response, $field = null)
    {
        $field = ($field === null) ? $this->key : $field;
        $this->preg[$field] = array('preg' => $preg, 'error_response' => $error_response);
        return $this;
    }

    //取得欄位名稱
    public function getTitle($field)
    {
        $this->check($field);
        return $this->data[$field][0];
    }

    //取得欄位類型
    public function getType($field)
    {
        $this->check($field);
        return $this->data[$field][1];
    }

    //取得欄位最小長度
    public function getMin($field)
    {
        $this->check($field);
        return $this->data[$field][2];
    }

    //取得欄位最大長度
    public function getMax($field)
    {
        $this->check($field);
        return $this->data[$field][3];
    }

    //取得欄位預設值
    public function getDefault($field)
    {
        $this->check($field);
        return $this->data[$field][4];
    }

    //取得欄位預設值
    public function getPreg($field)
    {
        return $this->preg[$field];
    }

    //取得欄位資料
    public function getAll()
    {
        return $this->data;
    }

    //取得欄位列舉資料
    public function getEnum($field = null, $value = null)
    {
        if ($field == null && $value == null) {
            return $this->enum;
        } elseif ($field != null && $value === null) {
            return $this->enum[$field];
        } else {
            return $this->enum[$field][$value];
        }
    }

    public function getUndefinedEnum($field)
    {
        return $this->undefinedEnumSql[$field];
    }

    //判斷欄位是否存在
    public function exist($field)
    {
        return isset($this->data[$field]);
    }

    //判斷欄位列舉是否存在
    public function enumExist($field, $value = null)
    {
        if (!isset($this->data[$field])) {
            return false;
        }

        if ($value === null) {
            if (!isset($this->enum[$field]) OR count($this->enum[$field]) == 0) {
                return false;
            }
            return true;
        } else {
            return isset($this->enum[$field][$value]);
        }
    }

    public function getOptionGroup($field)
    {
        return $this->optionGroup[$field];
    }

    private function check($field)
    {
        if (!isset($this->data[$field])) {
            die("field[{$field}] does not exist");
        }
    }

    public function get()
    {
        $fieldArray = array_keys($this->data);
        return $fieldArray;
    }

    public function getParentKey()
    {
        return $this->parentKey;
    }

}
