<?php
namespace admin\lib;

class Paging{
    private $onePageCount = 10;
    private $count = 0;
    private $pages = 0;
    private $index = 0;
    private $first = 0;
    private $last = 0;

    public function setIndex($index)
    {
        $this->index = $index;
    }

    public function getIndex()
    {
        return $this->index;
    }

    public function sqlLimit($count){
        $this->count = $count;
        //計算總頁數
        $this->pages = ceil($this->count / $this->onePageCount);
        //目前頁數判斷
        $this->index = ($this->index < 1)?1:$this->index;
        $this->index = ($this->index > $this->pages)?$this->pages:$this->index;
        //開始資料筆數
        $this->first = ($this->index - 1) * $this->onePageCount;
        //結束資料筆數
        $this->last = $this->first + $this->onePageCount;
        $this->last = ($this->last > $this->count)?$this->count:$this->last;
    }

    public function getFirst()
    {
        return $this->first;
    }

    public function getLast()
    {
        return $this->last;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function getPages()
    {
        return $this->pages;
    }

    public function getOnePageCount()
    {
        return $this->onePageCount;
    }

    public function output(){
        return array(
            'count' => $this->count,
            'index' => $this->index,
            'first' => $this->first,
            'last' => $this->last,
            'pages' => $this->pages
        );
    }
}