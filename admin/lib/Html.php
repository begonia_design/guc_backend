<?php


namespace admin\lib;


class Html
{
    const BTN_DEFAULT = 'btn-default';
    const BTN_PRIMARY = 'btn-primary';
    const BTN_SUCCESS = 'btn-success';
    const BTN_INFO = 'btn-info';
    const BTN_WARNING = 'btn-warning';
    const BTN_DANGER = 'btn-danger';

    const BTN_SIZE_LG = 'btn-lg';
    const BTN_SIZE_SM = 'btn-sm';
    const BTN_SIZE_XS = 'btn-xs';

    //https://v3.bootcss.com/components/
    const ICON_PLUS = 'plus';
    const ICON_REMOVE = 'remove';
    const ICON_PENCIL = 'pencil';

    private $tableId;
    private $thArr = [];
    private $tdArr = [];
    private $tableClass = [];
    private $tableHtml;

    public static function btnClick($txt, $action, $class = self::BTN_DEFAULT, $size = ''): string
    {
        return '<button class="btn '.$class.' '.$size.'" type="button" onclick="'.$action.'">'.$txt.'</button>';
    }

    public static function btnLink($txt, $link, $class = self::BTN_DEFAULT, $size = ''): string
    {
        return '<a class="btn '.$class.' '.$size.'" href="'.$link.'" role="button">'.$txt.'</a>';
    }

    public static function icon(string $type): string
    {
        return '<span class="glyphicon glyphicon-'.$type.'"></span>';
    }

    public static function table($id, $class = 'table'): Html
    {
        $table = new Html();
        return $table->createTable($id, $class);
    }

    public function createTable($id, $class): Html
    {
        $this->tableId = $id;
        return $this;
    }

    public function setTh(array $th): Html
    {
        $this->thArr = $th;
        return $this;
    }

    public function setTd(array $td): Html
    {
        $this->tdArr[] = $td;
        return $this;
    }

    public function tableHtml(): string
    {
        $html = '<table class="table" id="'.$this->tableId.'">';

        $th = '<thead><tr>';
        foreach ($this->thArr as $v) {
            $th .= '<th>'.$v.'</th>';
        }
        $th .= '</tr></thead>';

        $td = '<tbody>';
        foreach ($this->tdArr as $tr) {
            $td .= '<tr>';
            foreach ($tr as $v) {
                $td .= '<td>'.$v.'</td>';
            }
            $td .= '</tr>';
        }
        $td .= '</tbody>';

        return $html.$th.$td.'</table>';
    }

}