<?php

use lib\Config;

?>
<!DOCTYPE html>
<html lang="zh-TW">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo Config::ADMIN_TITLE; ?></title>

    <?php if(Config::ADMIN_FAVICON != ""): ?>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo Config::ADMIN_FAVICON; ?>">
    <?php endif ?>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/admin.css?202109" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <link href="css/plugins/select2/select2.css" rel="stylesheet" type="text/css">
    
    <link href="css/plugins/datetimepicker.css" rel="stylesheet" type="text/css">
    
    <link href="css/plugins/lightbox.css" rel="stylesheet" type="text/css">
    
    <link href="css/plugins/jquery.Jcrop.min.css" rel="stylesheet" type="text/css">

	<link href="css/plugins/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
    <!-- jQuery Version 1.11.0 -->
    <script src="js/jquery-3.3.1.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <script src="js/plugins/select2/select2.min.js"></script>

    <script src="js/plugins/datetimepicker.js"></script>
    
    <script src="js/plugins/fileuploader.js"></script>
	
    <script src="js/plugins/lightbox.min.js"></script>
    
<!--    <script src="js/plugins/ckeditor/ckeditor.js?9"></script>-->
    <script src="ckeditor4/ckeditor.js?1"></script>

    <script src="ckfinder35/ckfinder.js"></script>
    <script src="ckeditor5/ckeditor.js?1"></script>
    
    <script src="js/plugins/jquery.Jcrop.min.js"></script>

	<script src="js/plugins/jquery-ui.min.js"></script>

	<script src="js/plugins/bootstrap-colorpicker.min.js"></script>


    <script>

	function getTable(num){
        if (num != null) {
            $('#page-index').val(num);
        }
		$.post('?type=getTable',$('#form-table').serializeArray(),function(rs){
		    if (rs.showMsg != undefined) {
                $('#table-list,#table-paging').hide();
                $('#table-show-msg').text(rs.showMsg);
                $('#table-show-msg').show();
		        return false;
            }
            $('#table-show-msg').text('');
            $('#table-show-msg').hide();
            $('#table-list,#table-paging').show();
			$('#table-list tbody').html(rs.html);
			var first = rs.pageBar.first + 1;
			var last = rs.pageBar.last;
			var count = rs.pageBar.count;
			var index = rs.pageBar.index;
			var pages = rs.pageBar.pages;

			$('#table-page-info').text('第 ' + first + ' 到 ' + last + ' 筆，共 ' + count + ' 筆');
			$('#page-index').val(index);
			$('#order-by').val(rs.orderBy);
			$('#sort-br').val(rs.sortBy);
			
			var css = (rs.sortBy == 'ASC')?'sorting-asc':'sorting-desc';
			var thClass = rs.orderBy;
			$(".order-by").removeClass("sorting-asc sorting-desc").addClass("sorting");
			$(".order-by-" + thClass).removeClass("sorting").addClass(css);

			createPageBar(pages);
			return false;
		},'json');
	}

    function saveTableInput(){
        $.post('?type=saveTableInput',$('#table-list input'),function(rs){
            alert(rs);
        },'json');
    }

    function saveTableSelect(select){
        $.post('?type=saveTableSelect',select,function(rs){
            alert(rs);
            getTable();
        },'json');
    }

    function deleteTable(id){
        if(confirm("確定刪除？")){
            $.post('?type=deleteData',{id:id},function(rs){
                if(rs != undefined){
                    alert(rs);
                }
                getTable();
            },'json');
        }
    }
	
	function createPageBar(pages){
		var index = $('#page-index').val();
		var first = parseInt(index) - 3;
		var last = parseInt(index) + 3;
		var prev = parseInt(index) - 1;
		var next = parseInt(index) + 1
		first = (first < 1)?1:first;
		last  = (last > pages)?pages:last;
		var css = (index == 1)?'class="disabled"':'';
		var js = (index == 1)?'':'onclick="getTable('+prev+')"';
		var html = '<li '+css+'><a href="javascript:void(0);" '+js+'>&laquo;</a></li>';
		for (var i = first; i <= last; i++) {
			css = (i == index)?'class="active"':'';
			js = (i == index)?'':'onclick="getTable('+i+')"';
			html += '<li '+css+'><a href="javascript:void(0);" '+js+'>'+i+'</a></li>';
		}
		css = (last == index)?'class="disabled"':'';
		js = (last == index)?'':'onclick="getTable('+next+')"';
		html += '<li '+css+'><a href="javascript:void(0);" '+js+'>&raquo;</a></li>';
		$('#table-page-bar').html(html);
	}

	function setSort(id){
		var sortBy = 'ASC';
		if($('#order-by').val() == id){
			sortBy = ($('#sort-by').val() == sortBy)?'DESC':'ASC';
		}
		$('#order-by').val(id);
		$('#sort-by').val(sortBy);
        getTable(1);
	}``

    //新增或修改頁面
    function getEditor(id, url) {
	    let urlParam = '';
	    if (url == null) {
	        url = '';
	        $('#parentKeyId').val(id);
            $('#editor-back-btn').hide();
        } else {
            urlParam = '&parentKeyId=' + $('#parentKeyId').val();
            $('#editor-back-btn').show();
        }
        $('#saveUrl').val(url);
        $('#form-table').hide();
        $('#editor').show();
        $('#default-id').val(id);
        const title = (id == 0) ? '新增資料' : '修改資料';
        $('#editor-title').text(title);
        $('#form-default input[name=id]').val(id);
        $.post(url + '?type=getEditor' + urlParam, $('#form-default').serializeArray(), function (html) {
            $('#editor-content').html(html);
        });
    }

	//返回資料頁面
	function hideEditor(){
        $('#form-table').show();
        $('#editor').hide();
	}

    //修改表單傳送
    function saveEditor() {
        let url = $('#saveUrl').val();;

        $('#form-editor textarea').each(function () {
            var id = $(this).attr('id');
            if (CKEDITOR.instances[id]) {
                CKEDITOR.instances[id].updateElement();
            }
        });

        $.post(url+'?type=saveEditor', $('#form-editor').serializeArray(), function (rs) {
            if (rs.err != undefined) {
                alert(rs.err);
                if (rs.id != undefined) {
                    $('#set-' + rs.id).focus();
                }
            } else {
				if (rs.msg != undefined) {
					alert(rs.msg);
				}
				if (url == '') {
                    if ($('#table-list').length > 0) {
                        hideEditor();
                        getTable();
                    }
                } else {
				    getEditor($('#parentKeyId').val());
                }
            }
        }, 'json');
        return false;
    }

	//建立ckeditor編輯器
	function ckeditor(id){
		if( CKEDITOR.instances[id] ){ 
			CKEDITOR.remove(CKEDITOR.instances[id]); 
		}
        CKEDITOR.on( 'dialogDefinition', function( ev ) {
            var dialogName = ev.data.name;
            var dialogDefinition = ev.data.definition;

            if ( dialogName == 'table' ) {
                var info = dialogDefinition.getContents( 'info' );
                info.get( 'txtWidth' )[ 'default' ] = '';       // Set default width to 100%
                info.get( 'txtBorder' )[ 'default' ] = '';
                info.get( 'txtCellSpace' )[ 'default' ] = '';
                info.get( 'txtCellPad' )[ 'default' ] = '';
                // var addCssClass = dialogDefinition.getContents('advanced').get('advCSSClasses');
                // addCssClass['default'] = 'table';
            }
        });
		CKEDITOR.replace(id);
	}

	function ckeditor5(id, config){
        ClassicEditor
            .create( document.querySelector( id ), {
                ckfinder: {
                    uploadUrl: './ckfinder35/core/connector/php/connector.php?command=QuickUpload&type=Images&responseType=json'
                },
                toolbar: {
                    items: [
                        'undo',
                        'redo',
                        '|',
                        'heading',
                        '|',
                        'fontFamily',
                        'fontColor',
                        'fontSize',
                        'fontBackgroundColor',
                        '|',
                        'bold',
                        'italic',
                        'underline',
                        'removeFormat',
                        'link',
                        '|',
                        'blockQuote',
                        'horizontalLine',
                        '|',
                        'bulletedList',
                        'numberedList',
                        '|',
                        'outdent',
                        'indent',
                        '|',
                        'insertTable',
                        'imageUpload',
                        'mediaEmbed',
                        'ckfinder',
                        '|',
                        'sourceEditing'
                    ],
                    shouldNotGroupWhenFull: true
                },
                language: 'zh',
                image: {
                    toolbar: [
                        'imageTextAlternative',
                        'imageStyle:inline',
                        'imageStyle:block',
                        'imageStyle:side'
                    ]
                },
                table: {
                    contentToolbar: [
                        'tableColumn',
                        'tableRow',
                        'mergeTableCells',
                        'tableCellProperties',
                        'tableProperties'
                    ]
                },
                autosave: {
                    save( editor ) {
                        return $(id).val( editor.getData() );
                    }
                }
            } )
            .catch( function( error ) {
                console.error( error );
            } );
    }
	
	//建立上傳功能
	function fileUpload(id, fileExt, del, onCompleteFunction){
		var fileUploadId = "file-upload-" + id;

		var deleteButton = (del==0)?'':'<button class="file-upload-btn btn btn-warning" type="button" onclick="fileUploadDel(\''+id+'\')">刪除</button>';

        var fileExtArr = (fileExt == '')?'':fileExt.split(',');
			
		var uploader = new qq.FileUploader({
			element: document.getElementById(fileUploadId),
			action: 'upload.php',
			allowedExtensions: fileExtArr,
			debug: true,
			onSubmit: function(uploadid, fileName){
				$('#'+fileUploadId+' span.qq-upload-list>div').remove();
				$('#'+fileUploadId+' button.file-upload-btn').remove();
			},
			onProgress: function(uploadid, fileName, loaded, total){
				
			},
			onComplete: function(uploadid, fileName, responseJSON){
			    let originFileName = fileName;
				fileName = responseJSON.filename;
				$('#'+ fileUploadId +' .qq-upload-file').text(responseJSON.orig);
				$('#'+ id).val(responseJSON.orig);
                $('#upload-'+ id).val(fileName);
				$('#'+ fileUploadId +' .qq-upload-list').after(deleteButton);
				if(responseJSON.type == 'img'){
                    //var resizeButton = '<button class="file-upload-btn btn btn-warning" type="button" onclick="imageResize(\''+fileName+'\')">縮圖</button> ';
					//var cutButton = '<button class="file-upload-btn btn btn-warning" type="button" onclick="imageCut(\''+fileName+'\')">剪裁</button> ';
					//$('#'+fileUploadId+' .qq-upload-list').after(cutButton);
				}

				if (onCompleteFunction != undefined) {
                    onCompleteFunction(responseJSON.orig, fileName);
                }

			},
			onCancel: function(uploadid, fileName){},
			showMessage: function(message){
				alert(message);
			}
		});
		
		//如果有資料
		if($('#'+id).val() != undefined && $('#'+id).val() != ''){
		    console.log($('#'+id));
		    var ext = $('#'+id).val().split('.').pop();
		    if (ext == 'jpg' || ext=='png' || ext=='gif') {
		        var picUrl = '../<?php echo $this->fileDir?>' + $('#' + id).val();
                var html = '<div><a data-lightbox="image-'+id+'" href="'+picUrl+'"><img style="max-height:100px;max-width:100px" class="img-thumbnail" src="'+picUrl+'"></div>';
            } else {
                var html = '<div class="btn btn-info qq-upload-success">' +
                    '<span class="qq-upload-file">' + $('#' + id).val() + '</span></div>';
            }
			$('#'+ fileUploadId +' span.qq-upload-list').html(html);
			$('#'+ fileUploadId +' .qq-upload-list').after(deleteButton);
		}
	}
	
	//刪除檔案功能
	function fileUploadDel(id){
        var fileUploadId = "file-upload-" + id;
		$('#'+fileUploadId+' span.qq-upload-list>div.btn').remove();
		$('#'+fileUploadId+' button.file-upload-btn').remove();
		$('#'+id).val('');
	}

	//縮圖功能
	function imageResize(tempDir, img){
		$('#img-resize-img').attr('src','../'+ tempDir + img + '?'+ Math.random());
		$('#img-resize').modal({ backdrop:false });
	}

	//確定縮圖
	function imageResizeSubmit(){

	}

	//圖片範圍
	function imageCut(img){
		$('#div-image-cut').html('<img id="img-image-cut" class="img-responsive" src="../upload/temp/'+img+'?'+ Math.random()+'">');
		//$('#img-image-cut').attr('src' ,'../upload/temp/'+img+'?'+ Math.random());
		$('#input-image-cut-name').val(img);
		
		$('#img-image-cut').Jcrop({
			onSelect:function(c){
				$('#input-image-cut-x').val(c.x);
				$('#input-image-cut-y').val(c.y);
				$('#input-image-cut-w').val(c.w);
				$('#input-image-cut-h').val(c.h);
				$('#btn-image-cut-submit').show();
			}
		});
		$('#btn-image-cut-submit').hide();
		$('#dialog-image-cut').modal({ backdrop:false });
	}
	
	//確定裁圖
	function imageCutSubmit(){
		$('#input-image-cut-width').val($('#img-image-cut').width());
		$.post('?type=imageCut',$('#form-image-cut').serializeArray(),function(rs){
			$('#dialog-image-cut').modal('hide');
            $('#div-image-cut').html('<img id="img-image-cut" class="img-responsive" src="">');
		},'json');
	}
	
	//設定高度
	function setHeight(){
		$('#page-wrapper').css("min-height",$('body').height() + "px");
		$('.side-nav').css("height",( $('body').height() - 80) + "px");	
	}

	//ajax處理
	function getAjax(type, id, returnObj){
		$.get("ajax.php?type="+type+"&id="+id,function(rs){
			returnObj.html(rs);
		},"html");
	}

	function exportExcel(){
	    location.href='?type=export&'+$('#form-table').serialize()
    }
	
	$(function(){
		$.fn.extend({
			select2Sortable: function(){
				var select = $(this);
				$(select).select2({
					width: '100%',
					createTag: function(params) {
						return undefined;
					}
				});
				var ul = $(select).next('.select2-container').first('ul.select2-selection__rendered');
				ul.sortable({
					placeholder : 'ui-state-highlight',
					forcePlaceholderSize: true,
					items       : 'li:not(.select2-search__field)',
					tolerance   : 'pointer',
					stop: function() {
						$($(ul).find('.select2-selection__choice').get().reverse()).each(function() {
							var id = $(this).data('data').id;
							var option = select.find('option[value="' + id + '"]')[0];
							$(select).prepend(option);
						});
					}
				});
			}
		});

        setHeight();
		$(window).resize(setHeight);
		$(document).ajaxStart(function(){
			var html = "<div id=\"ajax-loading\">LOADING…</div>";
			var width = $("#page-wrapper").width();
			var height = $("#page-wrapper").height();
			$("#page-wrapper").prepend(html);
			$("#ajax-loading").width(width);
			$("#ajax-loading").height(height);
			$("#ajax-loading").css("opacity","0.5")
			$("#ajax-loading").css("padding-top",Math.round(height/2));
		}).ajaxStop(function(){
			$("#ajax-loading").remove();
		}).ajaxError(function (event, jqxhr, settings) {
			alert('ajax錯誤!!');
		});

		$('#image-cut').on('hidden.bs.modal', function (e) {
		  $('div.imgareaselect-selection').remove();
		  $('div.imgareaselect-border1').remove();
		  $('div.imgareaselect-border2').remove();
		  $('div.imgareaselect-border3').remove();
		  $('div.imgareaselect-border4').remove();
		  $('div.imgareaselect-outer').hide();
		});

        if ($('#table-list').length > 0) {
            getTable(1);
        }
	})
    </script>

</head>

<body>
    
    <div id="wrapper" class="wrapper-containers">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top wrapper-containers" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="index.php"><?php echo (Config::ADMIN_LOGO == "")?'<span class="glyphicon glyphicon-cog"></span>':Config::ADMIN_LOGO ?> <?php echo Config::ADMIN_TITLE; ?></a>
                
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav pull-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $this->authority->getUserName(); ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="admin_password.php"><i class="fa fa-fw fa-gear"></i> 變更密碼 </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="?type=logout"><i class="fa fa-fw fa-power-off"></i> 登出 </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">

					<?php echo $this->outputLanguageSelect(); ?>

                    <!-- 選單開始 -->
					<?php $this->menu->outputMenuHtml(); ?>
            		<!-- 選單  -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Nav -->
                <div class="row">
                    <div class="col-lg-12">
                        <ol id="page-nav" class="breadcrumb">
							<li><a href="index.php"><span class="glyphicon glyphicon-home"></span> 首頁</a></li>
							<?php $this->menu->outputNavHtml(); ?>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <?php $this->outputContent(); ?>
                
            </div>   
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
        
    </div>
    <!-- /#wrapper --> 
 
    <div id="footer" class="navbar-fixed-bottom wrapper-containers">
    	<p class="text-center"><?php echo Config::ADMIN_COPYRIGHT; ?></p>
    </div>


	<!-- 縮圖 -->
	<div id="dialog-image-resize" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="dialog-image-resize" aria-hidden="true">
		<div class="modal-dialog modal-md">
			<div class="modal-content">

				<div class="modal-body">
					<div class="row text-center">
						<img id="img-resize-img" class="img-responsive img-thumbnail" src="">
						<form id="img-resize-form">
							<input type="hidden" id="img-r-name" name="img-resize[name]" />
							<input type="text" id="img-r-w" name="img-resize[w]" />
							<input type="text" id="img-r-h" name="img-resize[h]" />
						</form>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
					<button id="img-resize-btn-submit" type="button" class="btn btn-primary" onClick="imgResizeSubmit()">確定縮圖</button>
				</div>

			</div>
		</div>
	</div>


	<!-- 裁圖 -->
    <div id="dialog-image-cut" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="dialog-image-cut" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">

				<div class="modal-body">
					<div class="row text-center" id="div-image-cut">
						<img id="img-image-cut" class="img-responsive" src="">
					</div>
				</div>

				<div class="modal-footer">
					<form id="form-image-cut">
						<input type="hidden" id="input-image-cut-name" name="imageCut[name]" />
						<input type="hidden" id="input-image-cut-x" name="imageCut[x]" />
						<input type="hidden" id="input-image-cut-y" name="imageCut[y]" />
						<input type="hidden" id="input-image-cut-w" name="imageCut[w]" />
						<input type="hidden" id="input-image-cut-h" name="imageCut[h]" />
						<input type="hidden" id="input-image-cut-width" name="imageCut[width]" />
					</form>
					<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
					<button id="btn-image-cut-submit" type="button" class="btn btn-primary" onClick="imageCutSubmit()">確定剪裁</button>
				</div>

			</div>
		</div>
    </div>
       
</body>

</html>
