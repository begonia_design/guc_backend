<?php
namespace admin\lib;

use lib\Db;
use PDO;

class Editor
{
    private $field;
    private $dbName;
    private $dbKey;

    private $key;
    private $data;
    private $default = array();

    private $editorHtml;
    private $editorJs;
    private $dbData;
    private $callback;

    function __construct(Field $field, Callback $callback)
    {
        $this->field = $field;
        $this->dbName = $field->getDbName();
        $this->dbKey = $field->getDbKey();
        $this->callback = $callback;
    }

    /*
     * 新增預設值
     */
    public function setDefault($name, $value)
    {
        $this->default[$name] = $value;
    }

    /*
     * 讀取預設值
     */
    public function getDefault()
    {
        return $this->default;
    }


    public function text($field)
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'text'
        );
        return $this;
    }

    /*
     * 建立日期欄位輸入
     */
    public function date($field)
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'date',
        );
        return $this;
    }

    /*
     * 建立日期時間欄位輸入
     */
    public function datetime($field)
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'datetime',
        );
        return $this;
    }

    /*
     * 建立檔案上傳欄位
     */
    public function file($field, $file_type = 'jpg,gif,png')
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'file',
            'file_type' => $file_type,
        );
        return $this;
    }

    /*
     * 建立多行文字欄位
     */
    public function textarea($field)
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'textarea',
        );
        return $this;
    }

    /*
     * 建立編輯器欄位
     */
    public function ckeditor($field)
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'ckeditor',
        );
        return $this;
    }

    /*
     *
     */
    public function ckeditor5($field, $configArray = null)
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'ckeditor5',
            'config' => $configArray
        );
        return $this;
    }

    /*
     * 建立下拉選單
     */
    public function select($field, $multiple = false)
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'select',
            'multiple' => $multiple,
        );
        return $this;
    }

    /*
     * 建立可搜尋下拉選單
     */
    public function select2($field, $multiple = false, $create = false)
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'select2',
            'multiple' => $multiple,
            'create' => $create
        );
        return $this;
    }

    /*
     * 建立可搜尋下拉選單
     */
    public function sortSelect2($field)
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'sortSelect2',
            'multiple' => true,
        );
        return $this;
    }

    /*
     * 建立純文字輸出
     */
    public function txt($field)
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'txt',
        );
        return $this;
    }

    public function txtHtml($field)
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'txtHtml',
        );
        return $this;
    }

    /*
     * 建立顏色選擇器
     */
    public function color($field)
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'color',
        );
        return $this;
    }

    /**
     * 自訂編輯欄位 function($keyId, $value) return html
     * @param $field
     * @param $callback
     * @return $this
     */
    public function custom($field, $callback)
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'custom',
            'callback' => $callback
        );
        return $this;
    }


    /*
     * 建立分線
     */
    public function html($html)
    {
        $this->key++;
        $this->data[$this->key] = array(
            'type' => 'html',
            'html' => $html
        );
        return $this;
    }


    /*
     * 自訂修改表格欄位
     */
    public function setTitle($title)
    {
        $this->data[$this->key]['title'] = $title;
        return $this;
    }

    /*
     * 備註欄位
     */
    public function setMemo($memo)
    {
        $this->data[$this->key]['memo'] = $memo;
        return $this;
    }

    /*
     * 建立onchange事件
     */
    public function setOnchange($onchange)
    {
        $this->data[$this->key]['onchange'] = $onchange;
        return $this;
    }


    /**
     * @param $type
     * @param $changeField
     */
    public function setAjax($type, $changeField)
    {
        $this->setOnchange("getAjax('{$type}', $(this).val(), $('#set-{$changeField}'))");
    }


    /*
     * 輸出編輯單行HTML
     */
    public function rowHtml($title, $id, $input, $memo = '')
    {
        $memo_html = ($memo == '') ? '' : '<span class="badge">' . $memo . '</span>';
        $html = '
			<div class="form-group class-' . $id . '">
				<label for="' . $id . '" class="col-sm-2 control-label">' . $title . '</label>
				<div class="col-sm-10">
				  ' . $input . $memo_html . '
				</div>
			</div>';
        return $html;
    }


    /*
     * 取得資料庫資料
     */
    public function getDbData($keyId)
    {
        $sql = sprintf("SELECT * FROM %s WHERE %s = :keyId", $this->dbName, $this->dbKey);
        $rs = Db::connect()->prepare($sql);
        if (!$rs) die(json_encode(Db::connect()->errorInfo()));
        $rs->bindValue(':keyId', $keyId);
        $rs->execute();
        return $rs->fetch(PDO::FETCH_ASSOC);
    }

    /*
     * 輸出新增刪除表單內容
     */
    public function output($keyId)
    {
        //如果為修改，先讀取原始資料
        $data = array();
        $setJs = '';

        if ($keyId > 0) { //修改資料
            $data = $this->getDbData($keyId);
        }

        $data = $this->callback->executeBeforeEditor($data);

        $enum = $this->callback->executeBeforeEditorEnum($this->field->getEnum(), $data);


        $setHtml = "<input type=\"hidden\" id=\"set-{$this->dbKey}\" name=\"set[{$this->dbKey}]\" value=\"{$keyId}\" >";

        if($this->field->getParentKey() && isset($_POST['default'][$this->field->getParentKey()])){

        }

        foreach ($this->data as $v) {

            if ($v['type'] == "html") {
                $setHtml .= $v["html"];
                continue;
            }

            if ($keyId == 0 && $this->field->getParentKey() == $v['field']) {
                if (isset($_GET['parentKeyId'])) {
                    $value = $_GET['parentKeyId'];
                } else if (isset($_POST['default'][$this->field->getParentKey()])) {
                    $value = $_POST['default'][$this->field->getParentKey()];
                }
            } else {
                $value = ($keyId == 0 && !isset($data[$v['field']])) ? $this->field->getDefault($v['field']) : $data[$v['field']];
                $value = ($v['type'] == 'txtHtml') ? $value : htmlspecialchars($value);
            }
            $name = "set[{$v['field']}]";
            $id = "set-{$v['field']}";
            $memo = $v['memo'];
            $html = "";
            $onchange = "";

            if (isset($v['onchange'])) {
                $onchange = "onchange=\"{$v['onchange']}\"";
            }

            //生成HTML
            switch ($v['type']) {
                case 'text':
                case 'color':
                    $html = "<input type=\"text\" class=\"form-control\" id=\"{$id}\" name=\"{$name}\" placeholder=\"請輸入{$v['title']}\" value=\"{$value}\" {$onchange}>";
                    break;
                case 'date':
                    $html = "<input type=\"text\" class=\"form-control\" data-date-format=\"YYYY-MM-DD\" id=\"{$id}\" name=\"{$name}\" placeholder=\"請輸入{$v['title']}\" value=\"{$value}\" {$onchange}>";
                    break;
                case 'datetime':
                    $html = "<input type=\"text\" class=\"form-control\" data-date-format=\"YYYY-MM-DD HH:mm:ss\" id=\"{$id}\" name=\"{$name}\" placeholder=\"請輸入{$v['title']}\" value=\"{$value}\" {$onchange}>";
                    break;
                case 'file':
                    $html = "<div id=\"file-upload-{$id}\"></div><input type=\"hidden\" id=\"{$id}\" name=\"{$name}\" value=\"{$value}\" {$onchange}>";
                    $html .= "<input type=\"hidden\" id=\"upload-{$id}\" name=\"upload-{$name}\" value=\"\" >";
                    break;
                case 'select':
                case 'select2':
                    $multiple = ($v['multiple']) ? 'multiple="multiple"' : '';
                    $name = ($v['multiple']) ? $name . '[]' : $name;
                    $option = ($v['multiple']) ? "" : "<option value=\"\">請選擇</option>";
                    //二階選單
                    if ($this->field->getOptionGroup($v['field'])) {
                        foreach ($this->field->getOptionGroup($v['field']) as $groupTitle => $optionArray) {
                            $option .= "<optgroup label=\"{$groupTitle}\">";
                            foreach ($optionArray as $optionKey) {
                                if (is_array($value)) {
                                    $selected = (in_array($optionKey, $value)) ? 'selected="selected"' : '';
                                } else {
                                    $selected = ($value !== "" && $optionKey == $value) ? 'selected="selected"' : '';
                                }
                                $option .= "<option value=\"{$optionKey}\" {$selected}>{$enum[$v['field']][$optionKey]}</option>";
                            }
                            $option .= "</optgroup>";
                        }
                    } else {
                        foreach ($enum[$v['field']] as $k1 => $v1) {
                            if (is_array($value)) {
                                $selected = (in_array($k1, $value)) ? 'selected' : '';
                            } else {
                                $selected = ($value !== "" && $k1 == $value) ? 'selected' : '';
                            }
                            $option .= "<option value=\"{$k1}\" {$selected}>{$v1}</option>";
                        }
                    }
                    $html = "<select class=\"form-control\" name=\"{$name}\" id=\"{$id}\" {$multiple} {$onchange}>{$option}</select>";
                    break;
                case 'sortSelect2':
                    $option = "";
                    $optionArray = $enum[$v['field']];
                    if (is_array($value)) {
                        foreach ($value as $key) {
                            if (isset($optionArray[$key])) {
                                $option .= "<option value=\"{$key}\" selected >{$optionArray[$key]}</option>";
                                unset($optionArray[$key]);
                            }
                        }
                    }
                    if ($optionArray) {
                        foreach ($optionArray as $key => $optionValue) {
                            $option .= "<option value=\"{$key}\" >{$optionValue}</option>";
                        }
                    }
                    $html = "<select class=\"form-control\" name=\"{$name}[]\" id=\"{$id}\" multiple=\"multiple\" {$onchange}>{$option}</select>";
                    break;

                case 'ckeditor':
                case 'ckeditor5':
                case 'textarea':
                    $html = "<textarea class=\"form-control\" id=\"{$id}\" name=\"{$name}\" rows=\"3\" {$onchange}>{$value}</textarea>";
                    break;

                case 'txt':
                case 'txtHtml':
                    $txt = (isset($enum[$v['field']][$value])) ? $enum[$v['field']][$value] : $value;
                    $html = "<p class=\"form-control-static\">{$txt}</p>";
                    break;

                case 'custom':
                    if ($v['callback'] == null) {
                        exit("error => editor custom callback not exist");
                    }
                    $html = $v['callback']($keyId, $value);
                    break;

            }//END 生成HTML

            $setHtml .= $this->rowHtml($v['title'], $id, $html, $memo);

            //生成JS
            switch ($v['type']) {
                case 'date':
                    $setJs .= "$('#{$id}').datetimepicker({pickTime: false});";
                    break;
                case 'datetime':
                    $setJs .= "$('#{$id}').datetimepicker();";
                    break;
                case 'select2':
                    //$set_js .= "$('#{$id}').select2_sortable();";
                    if ($v['create']) {
                        $setJs .= "$('#{$id}').select2({tags: true});";
                    } else {
                        $setJs .= "$('#{$id}').select2();";
                    }
                    break;
                case 'sortSelect2':
                    $setJs .= "$('#{$id}').select2Sortable();";
                    break;
                case 'file':
                    $del = ($this->field->getMin($v['field']) == 0) ? 1 : 0;
                    $setJs .= "fileUpload('{$id}','{$v['file_type']}',{$del}, function(fileName) {
                        ".$this->callback->executeFileUploadComplete($id)."
                    });";
                    break;
                case 'ckeditor':
                    $setJs .= "ckeditor('{$id}');";
                    break;
                case 'ckeditor5':
                    $setJs .= "ckeditor5('#{$id}');";
                    break;
                case 'color':
                    $setJs .= "$('#{$id}').colorpicker();";
                    break;
            }//end 生成JS

        }//END　foreach

        echo $setHtml . '<script>' . $setJs . '</script>';

        $this->callback->executeAfterEditorHtml($keyId);
    }

    public static function outputEditorHtml()
    {
        ?>
        <div id="editor" class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-edit"></span>
                        <span id="editor-title">新增</span>
                        <button class="btn btn-default pull-right btn-xs" type="button" onclick="hideEditor()">
                            回資料列表
                        </button>
                        <span class="pull-right">　</span>
                        <button id="editor-back-btn" class="btn btn-default pull-right btn-xs" type="button" onclick="getEditor($('#parentKeyId').val())">
                            回上頁
                        </button>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="hidden" id="saveUrl" value="">
                                <input type="hidden" id="parentKeyId" value="">
                                <form id="form-editor" class="form-horizontal" role="form" onsubmit="saveEditor();return false;">

                                    <div id="editor-content"></div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-primary">確定</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- end panel-body -->
                </div>
                <!-- end panel -->
            </div>
        </div>
        <?php
    }

    public function getEditorData()
    {
        return $this->data;
    }

}