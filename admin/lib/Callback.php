<?php
namespace admin\lib;

use League\Flysystem\Exception;
use Logger;
use PHPExcel;
use PHPExcel_IOFactory;

class Callback
{
    private $export;

    private $import;

    private $content;

    private $beforeTableData;

    private $beforeEditorEnum;

    private $beforeEditor;

    private $afterEditorHtml;

    private $beforeInsertSave;

    private $afterInsertSave;

    private $beforeUpdateSave;

    private $afterUpdateSave;

    private $beforeDelete;

    private $afterDelete;

    private $fileUploadComplete;

    private $custom;

    private $topButton;

    private $afterTableInput;

    private $beforeTableSelect;

    private $afterTableSelect;

    private $fileDir;

    private $otherHtml;

    /**
     * @param callable $export
     * function(Spreadsheet $ss) return $ss
     *   $ex->setActiveSheetIndex(0)->setCellValueByColumnAndRow($col, $row, $value);
     */
    public function setExport(callable $export)
    {
        $this->export = $export;
    }

    public function isExportExist(): bool
    {
        return ($this->export != null);
    }

    public function executeExport(){
        if ($this->export != null) {
            $callBackFunction = $this->export;
            $ex = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
            $ex = $callBackFunction($ex);
            $fileName = basename($_SERVER['PHP_SELF'],".php").date("YmdHis");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$fileName.'.xls"'); //檔案名稱
            header('Cache-Control: max-age=0');

            try {
                $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($ex, 'Xls');
                $writer->save('php://output');
            } catch (Exception $e){
                Logger::getLogger(__METHOD__)->error($e->getMessage());
            }
            exit;
        }
    }

    /**
     * @param callable $import
     * 匯入 function($filePath)
     */
    public function setImport(callable $import)
    {
        $this->import = $import;
    }

    public function isImportExist(): bool
    {
        return ($this->import != null);
    }

    public function executeImport(){
        $file = $_POST['fileName'];
        $filePath = SITE_DIR ."/". \lib\Config::UPLOAD_TEMP. $file;
        $callBackFunction = $this->import;
        $callBackFunction($filePath);
    }

    /**
     * @param callable $content
     */
    public function setContent(callable $content)
    {
        $this->content = $content;
    }

    public function executeContent(): bool
    {
        if ($this->content != null) {
            $callbackFunction = $this->content;
            $callbackFunction();
            return true;
        }
        return false;
    }

    /**
     * 列表資料輸出前處理 function($row) return $row
     *
     * $row : key => [field1 => value1, field2 => value2, ...]
     * @param callable A non-null {@see barCallable()}.
     * @see barCallable()
     */
    public function setBeforeTableData(callable $beforeTableData)
    {
        $this->beforeTableData = $beforeTableData;
    }


    public function executeBeforeTableData($row)
    {
        if ($this->beforeTableData != null) {
            $callbackFunction = $this->beforeTableData;
            return $callbackFunction($row);
        }
        return $row;
    }


    /**
     * 編輯輸出前關連編輯 function($enum, $data) return $enum
     * $enum : field => [key1 => value1, key2 => value2, ...]
     * $data : [field1 => value1, field2 => value2, ...]
     * @param $beforeEditorEnum
     */
    public function setBeforeEditorEnum(callable $beforeEditorEnum)
    {
        $this->beforeEditorEnum = $beforeEditorEnum;
    }

    public function executeBeforeEditorEnum($enum, $data)
    {
        if ($this->beforeEditorEnum != null) {
            $callbackFunction = $this->beforeEditorEnum;
            return $callbackFunction($enum, $data);
        }
        return $enum;
    }


    /**
     * 編輯資料顯示前處理 function($data) return $data
     * $data : [field1 => value1, field2 => value2, ...]
     * @param $beforeEditor
     */
    public function setBeforeEditor(callable $beforeEditor)
    {
        $this->beforeEditor = $beforeEditor;
    }

    public function executeBeforeEditor($data)
    {
        if ($this->beforeEditor != null) {
            $callbackFunction = $this->beforeEditor;
            return $callbackFunction($data);
        }
        return $data;
    }


    /**
     * 編輯區塊HTML部份輸出 function($id) return html
     * $id : key
     * @param $afterEditorHtml
     */
    public function setAfterEditorHtml(callable $afterEditorHtml)
    {
        $this->afterEditorHtml = $afterEditorHtml;
    }

    public function executeAfterEditorHtml($id)
    {
        if ($this->afterEditorHtml != null) {
            $callbackFunction = $this->afterEditorHtml;
            return $callbackFunction($id);
        }
        return '';
    }


    /**
     * 新增前資料處理 function($data, $customData) return $data
     * $data : [field1 => value1, field2 => value2]
     * @param $beforeInsertSave
     */
    public function setBeforeInsertSave(callable $beforeInsertSave)
    {
        $this->beforeInsertSave = $beforeInsertSave;
    }

    public function executeBeforeInsertSave($data, $customData)
    {
        if ($this->beforeInsertSave != null) {
            $callbackFunction = $this->beforeInsertSave;
            return $callbackFunction($data, $customData);
        }
        return $data;
    }


    /**
     * 新增後資料處理 function($data, $customData)
     * $data : [field1 => value1, field2 => value2]
     * @param $afterInsertSave
     */
    public function setAfterInsertSave(callable $afterInsertSave)
    {
        $this->afterInsertSave = $afterInsertSave;
    }

    public function executeAfterInsertSave($data, $customData)
    {
        if ($this->afterInsertSave != null) {
            $callbackFunction = $this->afterInsertSave;
            $callbackFunction($data, $customData);
        }
    }


    /**
     * 修改前資料處理 function($data, $originalData, $customData) { return $data; }
     * $data : [field1 => value1, field2 => value2]
     * $originalData : [field1 => value1, field2 => value2]
     * @param $beforeUpdateSave
     */
    public function setBeforeUpdateSave(callable $beforeUpdateSave)
    {
        $this->beforeUpdateSave = $beforeUpdateSave;
    }

    public function executeBeforeUpdateSave($data, $originalData, $customData)
    {
        if ($this->beforeUpdateSave != null) {
            $callbackFunction = $this->beforeUpdateSave;
            return $callbackFunction($data, $originalData, $customData);
        }
        return $data;
    }


    /**
     * 修改後資料處理 function($data, $originalData, $customData)
     * $data : [field1 => value1, field2 => value2]
     * @param $afterUpdateSave
     */
    public function setAfterUpdateSave(callable $afterUpdateSave)
    {
        $this->afterUpdateSave = $afterUpdateSave;
    }

    public function executeAfterUpdateSave($data, $originalData, $customData)
    {
        if ($this->afterUpdateSave != null) {
            $callbackFunction = $this->afterUpdateSave;
            $callbackFunction($data, $originalData, $customData);
        }
    }


    /**
     * 刪除前處理 function($keyId)
     * @param $beforeDelete
     */
    public function setBeforeDelete(callable $beforeDelete)
    {
        $this->beforeDelete = $beforeDelete;
    }

    public function executeBeforeDelete($keyId)
    {
        if ($this->beforeDelete != null) {
            $callbackFunction = $this->beforeDelete;
            $callbackFunction($keyId);
        }
    }


    /**
     * 刪除後處理 function($keyId)
     * @param $afterDelete
     */
    public function setAfterDelete(callable $afterDelete)
    {
        $this->afterDelete = $afterDelete;
    }

    public function executeAfterDelete($keyId)
    {
        if ($this->afterDelete != null) {
            $callbackFunction = $this->afterDelete;
            $callbackFunction($keyId);
        }
    }


    /**
     * 檔案上傳後處理 內容為JS code
     * @param $id
     * @param callable $fileUploadComplete
     */
    public function setFileUploadComplete($id, callable  $fileUploadComplete)
    {
        $this->fileUploadComplete[$id] = $fileUploadComplete;
    }

    public function executeFileUploadComplete($id)
    {
        $js = '';
        if (isset($this->fileUploadComplete[$id])) {
            $callbackFunction = $this->fileUploadComplete[$id];
            $js = $callbackFunction();
        }
        return $js;
    }

    public function setCustomFunction(callable $customFunction)
    {
        $this->custom = $customFunction;
    }

    public function executeCustomFunction()
    {
        if ($this->custom != null) {
            $callbackFunction = $this->custom;
            $callbackFunction();
        }
    }

    public function setTopButton(callable $topButton)
    {
        $this->topButton = $topButton;
    }

    public function executeTopButton()
    {
        if ($this->topButton != null) {
            $callbackFunction = $this->topButton;
            $callbackFunction();
        }
    }

    /**
     * 修改後資料處理 function($key, $field, $value)
     * @param $afterTableInput
     */
    public function setAfterTableInput(callable $afterTableInput)
    {
        $this->afterTableInput = $afterTableInput;
    }

    public function executeAfterTableInput($key, $field, $value)
    {
        if ($this->afterTableInput != null) {
            $callbackFunction = $this->afterTableInput;
            $callbackFunction($key, $field, $value);
        }
    }

    /**
     * 修改後資料處理 function($key, $field, $value)
     * @param $beforeTableSelect
     */
    public function setBeforeSaveTableSelect(callable $beforeTableSelect)
    {
        $this->beforeTableSelect = $beforeTableSelect;
    }

    public function executeBeforeSaveTableSelect($key, $field, $value)
    {
        if ($this->beforeTableSelect != null) {
            $callbackFunction = $this->beforeTableSelect;
            $callbackFunction($key, $field, $value);
        }
    }

    /**
     * 修改後資料處理 function($key, $field, $value)
     * @param $afterTableSelect
     */
    public function setAfterSaveTableSelect(callable $afterTableSelect)
    {
        $this->afterTableSelect = $afterTableSelect;
    }

    public function executeAfterSaveTableSelect($key, $field, $value)
    {
        if ($this->afterTableSelect != null) {
            $callbackFunction = $this->afterTableSelect;
            $callbackFunction($key, $field, $value);
        }
    }

    public function setFileDir(callable $fileDir)
    {
        $this->fileDir = $fileDir;
    }

    public function executeSetFileDir($fileDir, $data)
    {
        if ($this->fileDir != null) {
            $callbackFunction = $this->fileDir;
            return $callbackFunction($fileDir, $data);
        }
        return $fileDir;
    }

    public function setOtherHtml(callable $function)
    {
        $this->otherHtml = $function;
    }

    public function executeOtherHtml()
    {
        if ($this->otherHtml != null) {
            $callbackFunction = $this->otherHtml;
            $callbackFunction();
        }
    }
}
