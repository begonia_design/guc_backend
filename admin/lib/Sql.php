<?php
namespace admin\lib;

class Sql{
    private $search;
    private $orderBy;
    private $sortBy;
    private $bindValue;
    private $secondSort;

    function __construct(){

    }

    public function setOrderBy($orderBy, $sort){
        $this->orderBy = $orderBy;
        $this->sortBy = ($sort == 'ASC')?'ASC':'DESC';
    }

    public function setSecondSort($secondSort)
    {
        $this->secondSort = $secondSort;
    }

    public function setSql($sql, $bind, $value){
        $this->search[] = $sql;
        $this->bindValue[$bind] = $value;
    }

    public function setSearchSql($sql){
        $this->search[] = $sql;
    }

    public function setSearchBind($bind, $value){
        $this->bindValue[$bind] = $value;
    }

    public function getWhereSql(){
        if(is_array($this->search)){
            return ' AND '. join(' AND ',$this->search);
        }
        return null;
    }

    public function getBindValue(){
        return $this->bindValue;
    }

    public function getSortSql(){
        if ($this->orderBy == 'sort') {
            $idSort = ($this->sortBy == 'ASC')?'DESC':'ASC';
            //return " ORDER BY sort=0 {$this->sortBy}, sort {$this->sortBy}, id {$idSort}";
            return " ORDER BY sort {$this->sortBy}, id {$idSort}";
        }
        if ($this->secondSort) {
            return " ORDER BY " . $this->orderBy . " " . $this->sortBy . ", ". $this->secondSort . " ". $this->sortBy;
        } else {
            return " ORDER BY " . $this->orderBy . " " . $this->sortBy;
        }
    }

    public function getOrderBy(){
        return $this->orderBy;
    }

    public function getSortBy(){
        return $this->sortBy;
    }

}
