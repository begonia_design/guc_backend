<?php
namespace admin\lib;

use lib\Db;
use PDO;

class Menu{

    private $sub;
    private $menu;
    private $nav;

    function __construct($pageName, $userId){

        $auth = array();
        if ($userId != 1) {
            $sql = "SELECT p.* FROM admin_user u 
                JOIN admin_power p ON p.user_id = u.id 
                WHERE u.status = 1 AND u.id = :id";
            $auth = Db::getBindRowArray($sql, [':id' => $userId], 'menu');
        }
        $menu = \Common::MENU;

        $i = 1;
        $j = 1;
        foreach ($menu as $k => $v) {

            //page_name => [title, read, insert, update, delete]
            foreach ($v as $k1 => $v1) {
                if ($userId == 1 || isset($auth[$k1])) {
                    $this->sub[$i]['title'] = $k;
                    if ($k1 == $pageName) {
                        $this->sub[$i]['class'] = "active1";
                        $this->sub[$i]['class1'] = "in";
                    }

                    $this->menu[$i][$j] = array(
                        'class' => ($k1 == $pageName) ? 'active' : '',
                        'title' => $v1[0],
                        'href' => $k1.".php",
                        'target' => ''
                    );

                    if ($k1 == $pageName) {
                        $this->nav = array(
                            'sub' => $k,
                            'title' => $v1[0]
                        );
                    }
                }
                $j++;
            }
            $i++;
        }


//        $sql = "
//			SELECT
//				s.sub_id,
//				s.title AS sub_title,
//				m.menu_id,
//				m.title AS menu_title,
//				m.file,
//				IF(m.file='',m.url,CONCAT(m.file,'.php')) AS href
//			FROM admin_menu_sub s
//			JOIN admin_menu m ON s.sub_id = m.sub_id
//			JOIN admin_power p ON m.menu_id = p.menu_id
//			JOIN admin_user u ON p.user_id = u.id
//			WHERE u.status = 1
//			  AND u.id = :user_id
//			  AND m.status = 1
//			ORDER BY s.sort,m.sort";
//        $rs = Db::connect()->prepare($sql);
//        $rs->execute(array(':user_id'=>$userId));
//        while($row = $rs->fetch(PDO::FETCH_ASSOC)) {
//            //建立主目錄相關資料
//            $this->sub[$row['sub_id']]['title'] = $row['sub_title'];
//            if ($row['file'] == $pageName) {
//                $this->sub[$row['sub_id']]['class'] = "active1";
//                $this->sub[$row['sub_id']]['class1'] = "in";
//            }
//
//            //建立次目錄資料
//            $this->menu[$row['sub_id']][$row['menu_id']] = array(
//                'class' => ($row['file'] == $pageName) ? 'active' : '',
//                'title' => $row['menu_title'],
//                'href' => $row['href'],
//                'target' => ($row['file'] == '') ? 'target="new"' : ''
//            );
//
//            //建立麵包屑
//            if ($row['file'] == $pageName) {
//                $this->nav = array(
//                    'sub' => $row['sub_title'],
//                    'title' => $row['menu_title']
//                );
//            }
//        }
    }

    /*
     * 自訂麵包屑
     */
    public function setNav($subTitle, $title = null){
        $this->nav['sub'] = $subTitle;
        if($title != null){
            $this->nav['title'] = $title;
        }
    }

    /*
     * 輸出目錄HTML
     */
    public function outputMenuHtml(){
        foreach($this->sub as $subId => $sub){
            $menuArr = $this->menu[$subId];
            if(count($menuArr) == 1){
                foreach($menuArr as $menu) {
                    $this->outputSubMenuHtml($menu['class'], $menu['href'], $menu['title']);
                }
            }else{
                ?>
                <li class="<?php echo $sub['class']; ?>">
                    <a href="javascript:;" data-toggle="collapse" data-target="#menu_<?php echo $subId; ?>">
                    <span class="glyphicon glyphicon-folder-open"></span> <?php echo $sub['title']; ?> <i class="fa fa-fw fa-caret-down"></i></a>
                    <ul id="menu_<?php echo $subId; ?>" class="collapse <?php echo $sub['class1']?>">
                        <?php
                        foreach($menuArr as $menu) {
                            $this->outputSubMenuHtml($menu['class'], $menu['href'], $menu['title'], $menu['target'], 'glyphicon-list-alt');
                        }
                        ?>
                    </ul>
                </li>
                <?php
            }
        }
    }

    private function outputSubMenuHtml($class, $href, $title, $target = '', $icon = 'glyphicon-file')
    {
        ?>
        <li class="<?php echo $class; ?>">
            <a <?php echo $target; ?> href="<?php echo $href; ?>"><span class="glyphicon <?php echo $icon; ?>"></span> <?php echo $title; ?></a>
        </li>
        <?php
    }

    /*
     * 麵包屑 HTML 輸出
     */
    public function outputNavHtml(){
        $html = '';
        if(isset($this->nav['sub'])){
            $html .= '<li><a href="#">'.strip_tags($this->nav['sub']).'</a></li>';
        }

        if(isset($this->nav['title'])){
            $html .= '<li class="active">'.strip_tags($this->nav['title']).'</li>';
        }

        echo $html;
    }

}