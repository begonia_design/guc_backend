<?php
namespace admin\lib;

use lib\Db;

class Table
{

    private $field;
    private $key;
    private $data = array();
    private $fileDir;
    private $saveButton = false;
    private $authority;

    function __construct(Field $field, Authority $authority, $fileDir)
    {
        $this->field = $field;
        $this->authority = $authority;
        $this->fileDir = $fileDir;
    }


    public function txt($field, $width = '', $class = '')
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'type' => 'txt',
            'width' => $width,
            'class' => $class,
            'field' => $field,
            'sort' => 1
        );

        return $this;
    }


    public function input($field, $width = '70', $class = '')
    {
        $this->saveButton = true;
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'input',
            'width' => $width,
            'class' => $class,
            'sort' => 1
        );

        return $this;
    }


    public function img($field, $width = '70', $class = 'text-center')
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'img',
            'width' => $width,
            'class' => $class,
            'tdClass' => $class,
            'sort' => 0
        );

        return $this;
    }

    public function pic($field, $width = '70', $class = 'text-center')
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'pic',
            'width' => $width,
            'class' => $class,
            'tdClass' => $class,
            'sort' => 0
        );

        return $this;
    }


    public function select($field, $width = '80', $class = 'text-center')
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $this->field->getTitle($field),
            'field' => $field,
            'type' => 'select',
            'width' => $width,
            'class' => $class,
            'tdClass' => $class,
            'sort' => 0
        );

        return $this;
    }

    //修改
    public function mod($title = '修改', $width = '70', $class = 'text-center')
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $title,
            'type' => 'mod',
            'width' => $width,
            'class' => $class,
            'tdClass' => $class,
            'sort' => 0
        );

        return $this;
    }

    //刪除按鈕
    public function del($title = '刪除', $width = '70', $class = 'text-center', $checkFunction = null)
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $title,
            'type' => 'del',
            'width' => $width,
            'class' => $class,
            'tdClass' => $class,
            'sort' => 0,
            'callback' => $checkFunction
        );

        return $this;
    }

    //連結按鈕
    public function linkBtn($title, $btnTxt, $link, $width = '70', $class = 'text-center')
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $title,
            'type' => 'linkBtn',
            'width' => $width,
            'class' => $class,
            'tdClass' => $class,
            'btnTxt' => $btnTxt,
            'link' => $link,
            'sort' => 0
        );

        return $this;
    }

    /**
     * 自訂表格欄位 function($dataKey, $rowData) return tdHtml
     * @param $title
     * @param $callback
     * @return $this
     */
    public function custom($title, $callback)
    {
        $this->key++;
        $this->data[$this->key] = array(
            'title' => $title,
            'type' => 'custom',
            'callback' => $callback,
            'sort' => 0
        );

        return $this;
    }


    public function setTdClass($class)
    {
        $this->data[$this->key]['tdClass'] = $class;
        return $this;
    }

    public function setTitle($title)
    {
        $this->data[$this->key]['title'] = $title;
        return $this;
    }

    public function setWidth($width)
    {
        $this->data[$this->key]['width'] = $width;
        return $this;
    }

    //顯示表格內容
    public function output($dbData)
    {
        if (!$this->data) {
            exit("error => table not exist;");
        }

        $html = "";
        foreach ($dbData as $dataKey => $rowData) {
            $html .= "<tr>";

            foreach ($this->data as $arr) {
                $type = $arr['type'];
                $field = $arr['field'];
                $value = $rowData[$field];
                $tdClass = $arr['tdClass'];
                $tdHtml = "";

                //如果沒有修改權限，input與select改為txt
                if (!$this->authority->isUpdate() && in_array($type, array('input', 'select'))) {
                    $type = 'txt';
                }

                switch ($type) {
                    case 'txt':
                        if ($this->field->enumExist($field)) {
                            $value = $this->field->getEnum($field, $value);
                            if ($value == null && $this->field->getUndefinedEnum($field) != null) {
                                $value = Db::getBindRowData($this->field->getUndefinedEnum($field), [':id' => $value], 'value');
                            }
                        }
                        $tdHtml = "<span class=\"td-text\">" . $value . "</span>";
                        break;

                    case 'input':
                        $tdHtml = "<input type=\"text\" class=\"form-control input-sm\" name=\"input[{$field}][{$dataKey}]\" value=\"{$value}\">";
                        break;

                    case 'img':
                        $tdHtml = ($value == '') ? '' : "<a class=\"btn btn-primary btn-sm\" data-lightbox=\"image-{$dataKey}\" href=\"../{$this->fileDir}{$value}\"><span class=\"glyphicon glyphicon-picture\"></span></a>";
                        break;

                    case 'pic':
                        $tdHtml = ($value == '') ? '' : "<a class=\"img-thumbnail\" data-lightbox=\"image-{$dataKey}\" href=\"../{$this->fileDir}{$value}\"><img src=\"../{$this->fileDir}{$value}\" style=\"max-height: 100px;max-width: 100px;\"></a>";
                        break;

                    case 'select':
                        $option = '';
                        if (!$this->field->getEnum($field)) {
                            exit("error => {$field} Enum not exist;");
                        }
                        foreach ($this->field->getEnum($field) as $optionValue => $optionTxt) {
                            $selected = ($value == $optionValue) ? 'selected="selected"' : '';
                            $option .= "<option {$selected} value=\"{$optionValue}\">{$optionTxt}</option>";
                        }
                        $tdHtml = "<select class=\"form-control input-sm\" name=\"input[{$field}][{$dataKey}]\" onchange=\"saveTableSelect($(this))\">{$option}</select>";
                        break;

                    case 'mod':
                        if ($this->authority->isUpdate()) {
                            $tdHtml = "<a class=\"btn btn-warning btn-sm\" href=\"javascript:getEditor('{$dataKey}')\"><span class=\"glyphicon glyphicon-pencil\"></span></a>";
                        }
                        break;

                    case 'del':
                        if ($this->authority->isDelete()) {
                            $tdHtml = "<a class=\"btn btn-danger btn-sm\" href=\"javascript:deleteTable('{$dataKey}')\"><span class=\"glyphicon glyphicon-remove\"></span></a>";
                            if ($arr['callback'] != null) {
                                $callbackFunction = $arr['callback'];
                                if (!$callbackFunction($rowData)) {
                                    $tdHtml = "";
                                }
                            }
                        }
                        break;

                    case 'linkBtn':
                        $tdHtml = "<a class=\"btn btn-default btn-sm\" href=\"{$arr['link']}{$dataKey}\">{$arr['btnTxt']}</a>";
                        break;

                    case 'custom':
                        if ($arr['callback'] == null) {
                            exit("error => custom callback not exist");
                        }
                        $callbackFunction = $arr['callback'];
                        $tdHtml = $callbackFunction($dataKey, $rowData);
                        break;
                }
                $html .= "<td class=\"{$tdClass}\">{$tdHtml}</td>";
            }

            $html .= "</tr>";
        }
        return $html;
    }

    public function getSetData()
    {
        return $this->data;
    }

    public function showSaveBtn()
    {
        return $this->saveButton;
    }

}