<?php
namespace admin\lib;

use lib\Db;
use lib\Config;
use PDO;
use PHPMailer\PHPMailer\Exception;

class AdminPortal
{

    public $fileName = "";        //檔案名稱
    public $dbName = "";    //資料表
    public $dbKey = "";    //主鍵
    public $fileDir = ""; //檔案上傳存放
    public $fileTemp = ""; //檔案上傳暫存目錄

    public $authority; //權限
    public $menu; //目錄
    public $field; //欄位
    public $search; //搜尋
    public $sql; //sql語法
    public $paging; //分頁
    public $table; //資料列表
    public $editor; //編輯列表
    public $callback; //callback

    public $language = array(); //語系

    public $table_input_save_btn = false;

    private $searchStyle = 1; //搜尋區塊是否換行

    private $log;

    private $isSortRepeat = false;
    private $sortField = 'sort';
    private $sortTypeField = array();

    function __construct($fileName, $dbName = null, $dbKey = 'id')
    {
        \Logger::configure(Config::LOG);
        $this->log = \Logger::getLogger(__CLASS__);

        $this->fileName = $fileName; //設定檔案名稱
        $this->dbName = ($dbName == null) ? $fileName : $dbName; //設定資料表
        $this->dbKey = $dbKey; //設定主鍵

        $this->language = Config::SITE_LANG;
        $this->fileTemp = Config::UPLOAD_TEMP;
        $this->fileDir = Config::UPLOAD_DIR . $fileName . '/';

        $this->menu = new Menu($this->fileName, $_SESSION['userId']);
        $this->authority = new Authority($this->fileName, $_SESSION['userId']);
        $this->callback = new Callback();

        if ($fileName != 'index') {
            $this->field = new Field($this->dbName, $this->dbKey);
            $this->sql = new Sql();
            $this->paging = new Paging();
            $this->search = new Search($this->field, $this->sql);
            $this->table = new Table($this->field, $this->authority, $this->fileDir);
            $this->editor = new Editor($this->field, $this->callback);
            $this->field->setEnum($this->getStatus(), 'status');
        }
    }


    public function setSearchStyle($styleType)
    {
        $this->searchStyle = $styleType;
    }


    //取得狀態欄位
    private function getStatus()
    {
        $status = Db::getBindRowArray("SELECT id,title FROM admin_status WHERE source=:source", array(':source' => $this->fileName), 'id', 'title');
        if (!$status) {
            $status = Db::getRowArray("SELECT id,title FROM admin_status WHERE source='default'", 'id', 'title');
        }
        return $status;
    }


    /**
     * AJAX回傳表格列表
     * @param $orderBy
     * @param $sortBy
     * @param $index
     */
    private function ajaxGetTable($orderBy, $sortBy, $index)
    {
        if ($this->field->getParentKey()) {
            $bindKay = ":{$this->field->getParentKey()}";
            $bindValueArr = $this->sql->getBindValue();
            if ($bindValueArr[$bindKay] == "") {
                exit(json_encode(array(
                    'showMsg' => "請先搜尋".$this->field->getTitle($this->field->getParentKey())
                )));
            }
        }

        $html = "";
        $data = array();

        $this->search->setDefaultSort($orderBy, $sortBy);

        $sql = sprintf("SELECT COUNT(*) AS c FROM %s WHERE 1 %s ", $this->dbName, $this->sql->getWhereSql());
        $rs = Db::connect()->prepare($sql);
        $rs->execute($this->sql->getBindValue());
        $dataCount = $rs->fetchColumn();//總筆數

        $index = (is_numeric($index)) ? $index : 1; //目前頁數
        $this->paging->setIndex($index);

        if ($dataCount > 0) {
            $this->paging->sqlLimit($dataCount);
            $sql = sprintf("SELECT * FROM %s WHERE 1 %s %s LIMIT %s,%s",
                $this->dbName,
                $this->sql->getWhereSql(),
                $this->sql->getSortSql(),
                $this->paging->getFirst(),
                $this->paging->getOnePageCount()
            );
            $rs = Db::connect()->prepare($sql);
            $rs->execute($this->sql->getBindValue());
            while ($row = $rs->fetch(PDO::FETCH_ASSOC)) {
                //表格處理
                $row = $this->callback->executeBeforeTableData($row);
                //處理表格資料
                $data[$row[$this->dbKey]] = $row;
            }

            //顯示資料
            $html = $this->table->output($data);
        }


        exit(json_encode(array(
            'html' => $html,
            'pageBar' => $this->paging->output(),
            'orderBy' => $this->sql->getOrderBy(),
            'sortBy' => $this->sql->getSortBy(),
        )));
    }

    /*
     * 存檔
     */
    private function ajaxSaveEditor($post)
    {
        $originalData = array();
        $data = array();
        $otherData = array();
        $file = array();

        //讀取原資料
        $keyId = $post[$this->dbKey];

        if ($keyId > 0) {
            if (!$this->authority->isUpdate()) {
                exit(json_encode(array('err' => '您無修改權限!')));
            }
            $originalData = $this->editor->getDbData($keyId);
            if (!$originalData) {
                exit(json_encode(array('err' => '查無修改資料!')));
            }
        } else {
            if (!$this->authority->isInsert()) {
                exit(json_encode(array('err' => '您無新增權限!')));
            }
        }//END 讀取原資料


        //判斷其它欄位資料
        foreach ($this->editor->getEditorData() as $v) {

            if (!in_array($v['type'], array('txt', 'html'))) {
                $type = $this->field->getType($v['field']); //欄位類型

                if ($type == 'custom') {
                    $otherData[$v['field']] = $post[$v['field']];
                } else {
                    $data[$v['field']] = $post[$v['field']];

                    $min = $this->field->getMin($v['field']);    //最小長度
                    $max = $this->field->getMax($v['field']);    //最大長度
                    $title = $this->field->getTitle($v['field']);
                    $preg = $this->field->getPreg($v['field']);

                    self::checkSaveData($data[$v['field']], $title, $v['field'], $type, $min, $max, $preg);

                    //記錄檔案
                    if ($v['type'] == 'file') {
                        $file[$v['field']] = array(
                            'title' => $title,
                            'originalFile' => $originalData[$v['field']],
                            'newFile' => $data[$v['field']],
                            'fileType' => ($v['file_type'] == '') ? array() : explode(',', $v['file_type']),
                            'min' => $min
                        );
                    }
                }
            }
        }//END 資料

        //處理檔案
        //$data = $this->ajaxSaveFile($file, $data);

        $msg = "";
        if ($keyId > 0) {
            $data = $this->callback->executeBeforeUpdateSave($data, $originalData, $otherData);
            $data = $this->ajaxSaveFile($file, $data);
            $this->log->info("Data:".json_encode($data));
            if ($data) {
                if ($this->field->exist('update_time')) {
                    $data['update_time'] = date("Y-m-d H:i:s");
                }
                Db::update($this->dbName, $data, "{$this->dbKey} = :dbKey", array(':dbKey' => $keyId));
                $this->log->info("UserId:{$_SESSION['userId']} UPDATE:{$this->dbName} KeyId:{$keyId} Data:".json_encode($data));
                $msg = "修改成功!";
                if ($this->field->exist('sort')) {
                    $this->orderSort($keyId, $data['sort']);
                }
            }
            $this->callback->executeAfterUpdateSave($data, $originalData, $otherData);
        } else {
            $data = $this->callback->executeBeforeInsertSave($data, $otherData);
            $data = $this->ajaxSaveFile($file, $data);
            if ($data) {
                if ($this->field->exist('create_time')) {
                    $data['create_time'] = date("Y-m-d H:i:s");
                }
                if ($this->field->exist('update_time')) {
                    $data['update_time'] = date("Y-m-d H:i:s");
                }
                $keyId = Db::insert($this->dbName, $data);
                $data[$this->dbKey] = $keyId;
                $this->log->info("UserId:{$_SESSION['userId']} INSERT:{$this->dbName} KeyId:{$keyId} Data:".json_encode($data));
                $msg = "新增成功!";
                if (!$this->isSortRepeat && $this->field->exist($this->sortField)) {
                    $this->orderSort($keyId, $data['sort']);
                }
            }
            $this->callback->executeAfterInsertSave($data, $otherData);
        }

        exit(json_encode(array('msg' => $msg)));
    }


    public function orderSort($key, $sort, $parentKeyValue = null)
    {

        $sql = "SELECT * FROM {$this->dbName} WHERE {$this->dbKey} != :key AND sort = :sort";
        $bind = [':key' => $key, ':sort' => $sort];

        if ($this->field->getParentKey()) {
            $sql .= " AND {$this->field->getParentKey()} = :parentKey";

            if ($parentKeyValue == null) {
                $sql1 = "SELECT * FROM {$this->dbName} WHERE {$this->dbKey} = :key";
                $data = Db::getBindRowData($sql1,[':key' => $key]);
                $parentKeyValue = $data[$this->field->getParentKey()];
            }
            $bind[':parentKey'] = $parentKeyValue;
        }

        $this->log->info($sql);
        $data = Db::getBindRowData($sql, $bind);
        if ($data) {
            Db::update($this->dbName, ['sort' => $sort + 1], "{$this->dbKey} = :key", [":key" => $data[$this->dbKey]]);
            $this->orderSort($data[$this->dbKey], $sort + 1, $parentKeyValue);
        }
    }



    /**
     * 存檔前欄位判斷
     */
    private static function checkSaveData($value, $title, $field, $type, $min, $max, $preg)
    {
        if (is_array($value)) {
            return;
        }
        $len = mb_strlen($value, "UTF8"); //字串長度
        //判斷字串長度
        if ($min > 0 && $len < $min) {
            $txt = ($min == 1) ? "{$title} 為必填" : "{$title}需大於{$min}個字";
            exit(json_encode(array('err' => $txt, 'id' => $field)));
        }

        if ($max > 0 && $len > $max) {
            exit(json_encode(array('err' => "{$title} 不可大於 {$max} 個字", 'id' => $field)));
        }

        //判斷類型
        if ($len > 0) {
            if ($preg) {
                if (!preg_match($preg['preg'], $value)) {
                    exit(json_encode(array('err' => "{$title} 格式錯誤，{$preg['error_response']}", 'id' => $field)));
                }
            } else {
                switch ($type) {
                    case 'int': //數字
                        if (!is_numeric($value)) {
                            exit(json_encode(array('err' => "{$title} 需為數字", 'id' => $field)));
                        }
                        break;

                    case 'date': //日期
                        if ($value != date("Y-m-d", strtotime($value))) {
                            exit(json_encode(array('err' => "{$title} 日期格式錯誤：yyyy-mm-dd", 'id' => $field)));
                        }
                        break;

                    case 'datetime': //日期時間
                        if ($value != date("Y-m-d H:i:s", strtotime($value))) {
                            exit(json_encode(array('err' => "{$title} 格式錯誤：yyyy-mm-dd HH:ii:ss", 'id' => $field)));
                        }
                        break;

                    case 'email': //Email
                        if (!preg_match('/^[_.0-9a-z-]+@([0-9a-z-]+.)+[a-z]{2,3}$/', $value)) {
                            exit(json_encode(array('err' => "{$title} 格式錯誤，請輸入正確的email格式！", 'id' => $field)));
                        }
                        break;

                }
            }
        }//END　判斷類型
    }


    /**
     *  檔案處理
     */
    private function ajaxSaveFile($file, $data)
    {


        $fileDir = SITE_DIR . '/' . $this->fileDir;
        $fileTempDir = SITE_DIR . '/' . $this->fileTemp;

        $fileDir = $this->callback->executeSetFileDir($fileDir, $data);

        if (count($file) > 0) {
            //判斷上傳目錄是否存在
            if (!is_dir($fileDir)) {
                mkdir($fileDir);
            }

            foreach ($file as $field => $v) {
                $newFileName = "";

                //實際上傳檔案
                if (isset($_POST['upload-set'][$field])
                    && $_POST['upload-set'][$field] != "" && $v['newFile'] != "") {

                    $newTempFile = $_POST['upload-set'][$field];
                    $newFileInfo = pathinfo($fileTempDir . $newTempFile);
                    $ext = $newFileInfo['extension'];

//						if($ext == 'jpg'){
//						@imagejpeg(@imagecreatefromjpeg($originalPath), $newPath, 80);
////					}elseif($ext == 'png') {
////					@imagepng(@imagecreatefrompng($original_path), $new_path, 8);
///
                    //如果有限制副檔名
                    if (count($v['fileType']) > 0) {
                        if (!in_array($ext, $v['fileType'])) {
                            exit(json_encode(array('err' => "{$v['title']} 檔案類型錯誤，只能上傳" . join(',', $v['fileType']))));
                        }
                    }

                    $fileName = pathinfo($v['newFile'],PATHINFO_FILENAME);

                    $newFileName = $fileName.".".$ext;

                    if ($newFileName != $v['originalFile']) {
                        $i = 1;
                        while (is_file($fileDir . $newFileName)) {
                            $newFileName = $fileName . "_" . $i++ . "." . $ext;
                            $data[$field] = $newFileName;
                        }
                    }

                    copy($fileTempDir.$newTempFile, $fileDir.$newFileName);
                }

                //有舊資料移除
                if ($v['originalFile'] != "" && $v['originalFile'] != $v['newFile']
                    && is_file($fileDir . $v['originalFile'])) {
                    @unlink($fileDir . $v['originalFile']);
                }

            }//END foreach
        }

        return $data;
    }


    private function ajaxSaveTableSelect($post)
    {
        $field = key($post);
        $keyId = key($post[$field]);
        $value = $post[$field][$keyId];
        $title = $this->field->getTitle($field);
        $valueTxt = ($this->field->enumExist($field, $value)) ? $this->field->getEnum($field, $value) : $value;

        if ($this->authority->isUpdate()) {
            $this->callback->executeBeforeSaveTableSelect($keyId, $field, $value);
            $data = array($field => $value);
            Db::update($this->dbName, $data, "{$this->dbKey} = :keyId", array(':keyId' => $keyId));
            $msg = "{$title} 修改為 {$valueTxt}";
            $this->log->info("UserId:{$_SESSION['userId']} UPDATE:{$this->dbName} Data:{$field}:{$value}");
            $this->callback->executeAfterSaveTableSelect($keyId, $field, $value);
        } else {
            $msg = "您無權限";
        }
        exit(json_encode($msg));
    }

    /*
     *  儲存表格直接輸入
     */
    private function ajaxSaveTableInput($post)
    {
        $data = array();
        if ($this->authority->isUpdate()) {
            foreach ($post as $field => $arr) {
                if ($this->field->exist($field)) {

                    if ($field == 'sort') {
                        $unique = array_unique($arr);
                        if (count($arr) != count($unique)) {
                            exit(json_encode("排序不能重覆"));
                        }
                        asort($arr);
                    }

                    foreach ($arr as $keyId => $value) {
                        Db::update($this->dbName, array($field => $value), "{$this->dbKey} = :keyId",
                            array(':keyId' => $keyId));
                        if ($field == 'sort') {
                            $this->orderSort($keyId, $value);
                        }
                        $this->callback->executeAfterTableInput($keyId, $field, $value);
                        $data[$field][$keyId] = $value;
                    }
                }
            }
            $this->log->info("UserId:{$_SESSION['userId']} UPDATE:{$this->dbName} Data:".json_encode($data));
            $msg = "修改成功";
        } else {
            $msg = "您無權限";
        }

        exit(json_encode($msg));
    }

    /**
     * ajax刪除資料
     * @param $id
     */
    private function ajaxDeleteData($id)
    {
        $msg = null;
        if ($this->authority->isDelete()) {
            //刪除前處理
            $this->callback->executeBeforeDelete($id);
            $sql = sprintf("DELETE FROM %s WHERE %s = :keyId", $this->dbName, $this->dbKey);
            try {
                $rs = Db::connect()->prepare($sql);
                if ($rs->execute(array(':keyId' => $id))) {
                    $this->log->info("UserId:{$_SESSION['userId']} Delete:{$this->dbName} KeyId:{$id}");
                }
            } catch(Exception $e) {
                $this->log->error($e->errorMessage());
            }
            //刪除後處理
            $this->callback->executeAfterDelete($id);
        } else {
            $msg = "您無權限";
        }
        exit(json_encode($msg));
    }



    /**
     * 語系選單輸出
     */
    private function outputLanguageSelect()
    {
        if (count($this->language) > 1) {
            ?>
        <li class="change-lan-form">
            <div>
                <select class="form-control" onChange="location.href='?type=changeLanguage&lang='+$(this).val()">
                    <?php foreach ($this->language as $k => $v){ ?>
                        <option <?php echo ($_SESSION['lang'] == $k)?'selected':''; ?> value="<?php echo $k; ?>"><?php echo $v; ?></option>
                    <?php } ?>
                </select>
            </div>
        </li>
            <?php
        }
    }

    /**
     * 按鈕區塊輸出
     */
    private function outputButton()
    {
        $modBtn = ($this->authority->isUpdate() && $this->table->showSaveBtn()) ? '<button type="button" onclick="saveTableInput();" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> 儲存</button>　' : '';
        $addBtn = ($this->authority->isInsert()) ? '<button type="button" onclick="getEditor(0);" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> 新增</button>' : '';
        $expBtn = ($this->callback->isExportExist()) ? " <button type=\"button\" onclick=\"exportExcel();\" href=\"?type=export\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-cloud-download\"></span> 匯出</button>" : "";

        $width = ($this->searchStyle == 1) ? 4 : 12;
        ?>
        <div class="col-lg-<?php echo $width; ?> text-right btn_area">
            <?php echo $modBtn . $addBtn . $expBtn; ?>
            <?php $this->callback->executeTopButton(); ?>
        </div>
        <?php
    }


    /*
     *  表格輸出
     */
    protected function outputTable()
    {

        $th = "";
        foreach ($this->table->getSetData() as $k => $v) {
            $style = (isset($v['width'])) ? "width:{$v['width']}px;" : '';
            $class = (isset($v['class'])) ? $v['class'] : '';
            $js = ($v['sort']) ? " onclick=\"setSort('{$v['field']}');\"" : '';
            $sortCss = ($v['sort']) ? 'order-by sorting' : '';
            $th .= "<th style=\"{$style}\" class=\"{$class}\"><div {$js} id=\"table-th-{$k}\" class=\"order-by-{$v['field']} {$sortCss}\">{$v['title']}</div></th>";
        }

        $this->search->outputDefault();
        ?>
        <form id="form-table" onsubmit="return false;">
            <input type="hidden" id="order-by" name="sort[orderBy]" value="<?php echo $this->sql->getOrderBy(); ?>">
            <input type="hidden" id="sort-by" name="sort[sortBy]" value="<?php echo $this->sql->getSortBy(); ?>">

            <div class="row">
                <?php $this->search->output($this->searchStyle); ?>
                <?php $this->outputButton(); ?>
            </div>

            <?php if ($this->callback->isImportExist()){ ?>
            <div class="row form-horizontal">
                <div class="form-group">
                    <label class="col-sm-1 control-label">匯入</label>
                    <div class="col-sm-11">
                        <div id="file-upload-import"></div>
                    </div>
                </div>
            </div>
            <script>
                $(function(){
                    fileUpload('import','',false, function(fileName, tempFileName) {
                        $.post("?type=import",{fileName:tempFileName},function(rs){
                            if (rs.status == 'err') {
                                alert(rs.msg);
                            }
                            if (rs.status == 'success') {
                                alert(rs.msg);
                                location.reload();
                            }
                        },'json');
                    });
                })
            </script>
            <?php } ?>

            <div id="table-list" class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-condensed table-hover">
                            <thead>
                            <tr><?php echo $th; ?></tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div id="table-paging" class="row">
                <div class="col-md-3">
                    <div class="input-group">
                        <input type="text" id="page-index" name="pageIndex" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" onClick="getTable()">快速跳頁</button>
                        </span>
                    </div>
                </div>

                <div class="col-md-3 text-center">
                    <p id="table-page-info"></p>
                </div>

                <div class="col-md-6 text-right">
                    <ul id="table-page-bar" class="pagination">
                    </ul>
                </div>
            </div>

            <div class="alert alert-info" role="alert" id="table-show-msg">
            </div>
        </form>
        <?php
    }


    /*
     * 輸出主要內容區HTML
     */
    private function outputContent()
    {
        if (!$this->callback->executeContent()) {
            $this->outputTable();
            $this->editor->outputEditorHtml();
        }
        $this->callback->executeOtherHtml();
    }


    //頁面輸出
    private function output()
    {
        switch ($_GET['type']) {
            case 'getTable': //顯示表格資料
                $this->ajaxGetTable($_POST['sort']['orderBy'], $_POST['sort']['sortBy'], $_POST['pageIndex']);
                exit;

            case 'getEditor': //顯示修改資料表單
                $this->editor->output($_POST['id']);
                exit;

            case 'saveEditor': //表單存檔
                $this->ajaxSaveEditor($_POST['set']);
                exit;

            case 'saveTableSelect': //快速修改表格資料
                $this->ajaxSaveTableSelect($_POST['input']);
                exit;

            case 'saveTableInput': //快速修改表格資料
                $this->ajaxSaveTableInput($_POST['input']);
                exit;

            case 'deleteData': //刪除資料
                $this->ajaxDeleteData($_POST['id']);
                exit;

            case 'export': //匯出
                $this->callback->executeExport();
                exit;

            case 'changeLanguage': //更換語系
                $siteLang = Config::SITE_LANG;
                $_SESSION['lang'] = (isset($siteLang[$_GET['lang']])) ? $_GET['lang'] : key($siteLang);
                header("location:{$this->fileName}.php");
                exit;

            case 'imageCut': //裁圖
                $name = $_POST['imageCut']['name'];
                $w = $_POST['imageCut']['w'];
                $h = $_POST['imageCut']['h'];
                $x = $_POST['imageCut']['x'];
                $y = $_POST['imageCut']['y'];
                $width = $_POST['imageCut']['width'];
                $dir = $this->fileTemp;
                exit(json_encode(self::imageCut($name, $w, $h, $x, $y, $width, $dir)));

            case 'logout': //登出
                unset($_SESSION['userId']);
                header("location:login.php");
                exit;

            case 'custom':
                $this->callback->executeCustomFunction();
                exit;

            case 'import':
                $this->callback->executeImport();
                exit;

            default:
                include 'layout_html.php';
        }
    }


    //LOG
    private function addLog($type, $data)
    {
        $this->log->info("UserId:{$_SESSION['user_id']} DATA:".json_encode($data));
        $log = array(
            'stamp' => date("Y-m-d H:i:s"),
            'user_id' => $_SESSION['user_id'],
            'db_name' => $_SESSION['user_id'],
            'type' => $type,
            'json_data' => json_encode($data)
        );
        Db::insert('admin_log', $log);
    }

    public function getMaxSort($sortField = 'sort')
    {
        $maxSort = Db::getRowData("SELECT * FROM {$this->dbName} ORDER BY {$sortField} DESC LIMIT 1", $sortField);
        return ($maxSort)?:0;
    }

    //裁圖
    public static function imageCut($name, $w, $h, $x, $y, $width, $dir)
    {
        $imageFile = SITE_DIR ."/". $dir . $name;
        if (is_file($imageFile)) {
            $imageInfo = getimagesize($imageFile);
            $imageWidth = $imageInfo[0]; //取得原圖寬度
            //計算比例
            if ($imageWidth != $width) {
                $w = round($w * $imageWidth / $width);
                $h = round($h * $imageWidth / $width);
                $x = round($x * $imageWidth / $width);
                $y = round($y * $imageWidth / $width);
            }

            $img = imagecreatefromstring(file_get_contents($imageFile));
            $newImage = imagecreatetruecolor($w, $h);
            imagecopyresampled($newImage, $img, 0, 0, $x, $y, $w, $h, $w, $h);
            imagejpeg($newImage, $imageFile);
            imagedestroy($newImage);
            return array('rs' => 'ok');
        } else {
            return array('rs' => 'err');
        }
    }//END 裁圖




    function __destruct()
    {
        $this->output();
    }


}
