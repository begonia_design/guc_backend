<?php


namespace admin\lib;


use model\InvFinTabDao;

class Tab
{
    public static function getTabAdmin($fileName, InvFinTabDao $daoClass)
    {
        $page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

        $page->field->id();
        $page->field->txt('URL_ID', 'url_id', 1, 30);
        $page->field->txt('標題' ,'title' ,1 ,200);
        $page->field->txt('內容', 'html', 1, 51200, \Common::getDefaultHtml());
        $page->field->status();
        $page->field->sort();
        $page->field->createTime();
        $page->field->updateTime();

        //搜尋===========================
        $page->setSearchStyle(1);
        $page->search->setDefaultSort('sort', 'ASC');
        $page->search->text('title');
        $page->search->select('status');

        //表格===========================
        //$page->table->txt('id', 70, 'text-center')->setTdClass('text-center');
        $page->table->txt('title');
        $page->table->select('status');
        $page->table->input('sort');
        $page->table->mod();
        $page->table->del();

        //新增刪除===========================
        $page->editor->text('url_id');
        $page->editor->text('title');
        $page->editor->ckeditor('html');
        $page->editor->select('status');
        $page->editor->text('sort');

        $daoClass::setBackend();

        $page->callback->setBeforeInsertSave(function($data, $customData)use($daoClass){
            if ($daoClass::getAllByUrlId($data['url_id'])) {
                exit(json_encode(array('err' => 'URL_ID重覆，請重新輸入')));
            }
            return $data;
        });

        $page->callback->setBeforeUpdateSave(function($data, $originalData, $customData)use($daoClass){
            if ($daoClass::getAllByUrlId($data['url_id'], $originalData['id'])) {
                exit(json_encode(array('err' => 'URL_ID重覆，請重新輸入')));
            }
            return $data;
        });
    }
}