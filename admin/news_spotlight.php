<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->int('類型', 'type', 1, 1)->setEnum(\enum\NewsType::get());
$page->field->txt('URL ID', 'url_id', 1, 60)->setPreg("/^([0-9A-Za-z_-]+)$/","URL_ID只能英文數字或底線");
$page->field->txt('標題' ,'title' ,1 ,200);
$page->field->txt('內容', 'html', 1, 51200, Common::getDefaultHtml());
$page->field->date('上架日' ,'start_date' ,10 , date("Y-m-d"));
$page->field->date('下架日' ,'end_date' ,0 );
$page->field->status();
$page->field->int('首頁排序', 'index_sort', 1, 1)->setEnum(Common::INDEX_SORT);
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(0);
$page->search->setDefaultSort('start_date', 'DESC');
$page->search->select('type');
$page->search->text('title');
$page->search->select('status');

//表格===========================
//$page->table->txt('id', 70, 'text-center')->setTdClass('text-center');
$page->table->txt('url_id');
$page->table->txt('type');
$page->table->txt('title');
$page->table->txt('start_date');
$page->table->txt('end_date');
$page->table->select('index_sort');
$page->table->select('status');
$page->table->mod();
$page->table->del();

//新增刪除===========================
$page->editor->select('type');
$page->editor->text('url_id');
$page->editor->text('title');
$page->editor->ckeditor('html');
$page->editor->date('start_date');
$page->editor->date('end_date');
$page->editor->select('index_sort');
$page->editor->select('status');

\model\NewsSpotlightDao::setBackend();

$page->callback->setAfterSaveTableSelect(function($key, $field, $value){
    if ($field == 'index_sort' && $value > 0) {
        \model\IndexSort::updateSort($value, $key,1);
    }
});

$page->callback->setBeforeInsertSave(function($data, $customData){
    if (\model\NewsSpotlightDao::getAllByUrlId($data['url_id'])) {
        exit(json_encode(array('err' => 'URL_ID重覆，請重新輸入')));
    }
    Common::checkDateAndReturnErrMsg($data);
    return $data;
});

$page->callback->setAfterInsertSave(function($data, $customData){
    if ($data['index_sort'] > 0) {
        \model\IndexSort::updateSort($data['index_sort'], $data['id'],1);
    }
});

$page->callback->setBeforeUpdateSave(function($data, $originalData, $customData){
    if (\model\NewsSpotlightDao::getAllByUrlId($data['url_id'], $originalData['id'])) {
        exit(json_encode(array('err' => 'URL_ID重覆，請重新輸入')));
    }
    Common::checkDateAndReturnErrMsg($data);
    return $data;
});

$page->callback->setAfterUpdateSave(function($data, $originalData, $customData){
    if ($data['index_sort'] > 0 && $data['index_sort'] != $originalData['index_sort']) {
        \model\IndexSort::updateSort($data['index_sort'], $data['id'],1);
    }
});

$page->callback->setTopButton(function(){
    Common::newsTopButton();
});

$page->callback->setOtherHtml(function(){
    Common::newsQuickSort();
});