/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
    config.language = 'zh';
	// config.uiColor = '#AADC6E';

    config.filebrowserBrowseUrl= 'ckfinder35/ckfinder.html';
    config.filebrowserImageBrowseUrl= 'ckfinder35/ckfinder.html?type=Images';
    config.filebrowserUploadUrl= 'ckfinder35/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl= 'ckfinder35/core/connector/php/connector.php?command=QuickUpload&type=Images';

    //http://cdn.ckeditor.com/4.5.5/full-all/samples/toolbarconfigurator/index.html#advanced
    config.toolbar = 'Custom1';
    config.toolbar_Custom1 = [
        { name: 'document', items: [ 'Source'] },
        { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
        { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'] },
        { name: 'links', items: [ 'Link', 'Unlink' ] },
        { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule'] },
        '/',
        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
        { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike','RemoveFormat' ] },
        { name: 'colors', items: [ 'TextColor', 'BGColor' ] }
    ]


    config.toolbar_TadToolbar =
        [
            //['Source','-','Templates','-','Cut','Copy','Paste'],
            //['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
            //['Link','Unlink','Image'],
            ////['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
            //['Format','Styles'],
            ////'/',
            ////['TextColor', 'BGColor'],
            //['NumberedList','BulletedList'],
            //['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
            ////['Maximize','ShowBlocks']
            ['Source','-','Templates','-','Cut','Copy','Paste'],
            ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
            ['Link','Unlink','Anchor'],
            ['Image','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
            '/',
            ['Styles', 'Format', 'Font', 'FontSize'],
            ['TextColor', 'BGColor'],
            ['Bold','Italic','Underline','Strike','-','Subscript','Superscript','CreateDiv',],
            ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
            ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
        ];
    config.stylesSet = [{ name: 'h4_green', element: 'h4', styles: { 'color': '#096' }}];
    config.format_tags = 'p;h3;h4;h5;h6;pre;address;div';
    config.toolbarCanCollapse = true;
    config.allowedContent = true;
    config.contentsCss = ['../en/css/style.min.css', 'ckeditor4/custom.css'];
    config.bodyClass = 'article-block__cont article-cont';

};
