<?php
include "common.php";

use admin\lib\AdminPortal;

$page = new AdminPortal(basename(__FILE__, '.php'));

$page->field
    ->id()
    ->int('發信方法', 'type', 1, 1)
        ->setEnum([ 0 => 'SERVER直接發送', 1 => '自訂SMTP', 2 => 'Gmail SMTP'])
    ->txt('HOST', 'host', 0, 50)
    ->txt('PORT', 'port', 0, 50)
    ->txt('使用者帳號', 'username', 0, 50)
    ->txt('使用者密碼', 'password', 0, 50)
    ->int('AUTO TLS', 'auto_tls', 1 , 1, 1)
        ->setEnum([0 => '否', 1 => '是'])
    ->txt('SMTP secure', 'smtp_secure', 0 , 20)
        ->setEnum(['' => '無', 'tls' => 'TLS', 'ssl' => 'SSL'])
    ->email('發信Email', 'from_email', 1, 50)
    ->txt('發信者名稱', 'from_name', 1, 50)
    ->custom('測試發信', 'test');

//搜尋===========================
//$page->setSearchStyle(0);
//$page->search->setDefaultSort('id', 'DESC')
//    ->text('title')
//    ->select('status');

//表格===========================
$page->table
    ->txt('type')
    ->txt('from_email')
    ->txt('from_name')
    ->mod();


//新增刪除===========================
$page->editor
    ->select('type')->setOnchange("changeType(this);")
    ->text('host')
    ->text('port')
    ->text('username')
    ->text('password')
    ->select('auto_tls')
    ->select('smtp_secure')
    ->text('from_email')
    ->text('from_name')
    ->custom('test', function ($keyId, $value) {
        $html = <<<HTML
<div class='input-group'>
    <input type="text" class="form-control" id="set-test" name="set[test]" placeholder="請輸入EMAIL">
    <div class="input-group-btn">
        <button type='button' class='btn btn-default' onclick='sendTestMail()'>發送測試信</button>
    </div>
</div>
<div id="gmailInfo" class="alert alert-info" style="margin:20px 0 0 0">使用Gmail發信請到 <b>「設定」</b> > <b>「轉寄和 POP/IMAP」</b> > 選擇<b>「對所有郵件啟用 POP 功能」</b>與<b>「啟用 IMAP」</b>，並<a target="_blank" href="https://myaccount.google.com/u/0/lesssecureapps">設定開啟低安全性應用程式存取權</a></div>
<script>
function sendTestMail(){
    $.post("ajax.php?type=sendTestMail", $('#form-editor').serializeArray(), function(rs){
        if (rs.msg != undefined) {
            alert(rs.msg); 
        }
    },'json');
}

function changeType(t){
    var smtpField = $('.class-set-host,.class-set-port,.class-set-username,.class-set-password,.class-set-auto_tls,.class-set-smtp_secure');
    var type = $(t).val();
    $('#gmailInfo').hide();
    if (type == 0) {
        smtpField.hide();
    } else {
        smtpField.show();
        if (type == 2) {
            $('#set-host').val("smtp.gmail.com");
            $('#set-port').val("465");
            $('#set-smtp_secure').val("ssl");
            $('#gmailInfo').show();
        }
    }
    
}

$(function(){
    changeType('#set-type');
})
</script>
HTML;
        return $html;
    });


