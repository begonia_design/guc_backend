<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->txt('類型' ,'type' ,1 ,50);
$page->field->txt('標題' ,'title' ,1 ,200);
$page->field->file('檔案' ,'file' ,true);
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(0);
$page->search->setDefaultSort('id', 'ASC');

//表格===========================
$page->table->txt('type');
$page->table->txt('title');
$page->table->txt('file');
$page->table->txt('update_time');
$page->table->mod();

//新增刪除===========================
$page->editor->txt('type');
$page->editor->text('title');
$page->editor->file('file','pdf,xls,xlsx');
