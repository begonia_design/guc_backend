<?php
include "lib/upload.php";
include "../php/lib/Config.php";

use \lib\Config;

session_start();
if(!isset($_SESSION['userId'])) {
    exit;
}

// list of valid extensions, ex. array("jpeg", "xml", "bmp")
$allowedExtensions =($_GET['ftype']=='')?array():explode(',',$_GET['ftype']);
// max file size in bytes
$sizeLimit = Config::UPLOAD_SIZE * 1024 * 1024;

$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
$result = $uploader->handleUpload('../'. Config::UPLOAD_TEMP);

// to pass data through iframe you will need to encode all html tags
echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
