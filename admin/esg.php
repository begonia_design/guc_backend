<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->txt('標題' ,'title' ,1 ,200);
$page->field->int('類型', 'type', 1, 1, 1)->setEnum([1 => '檔案', 2=>'連結']);
$page->field->txt('連結' ,'url' ,0 ,200);
$page->field->file('檔案' ,'file' );
$page->field->date('上架日' ,'start_date' ,10 , date("Y-m-d"));
$page->field->date('下架日' ,'end_date' ,0 );
$page->field->createTime();
$page->field->updateTime();
$page->field->sort('排序', 'sort', ($page->getMaxSort()+1));

//搜尋===========================
$page->setSearchStyle(0);
$page->search->setDefaultSort('sort', 'ASC');
$page->search->text('title');

//表格===========================
$page->table->txt('title');
$page->table->txt('type');
$page->table->txt('start_date');
$page->table->txt('end_date');
$page->table->txt('update_time');
$page->table->input('sort');
$page->table->mod();
$page->table->del();

//新增刪除===========================
$page->editor->text('title');
$page->editor->select('type')->setOnchange("changeType(this);");
$page->editor->text('url');
$page->editor->file('file','xls,xlsx,pdf');
$page->editor->date('start_date');
$page->editor->date('end_date');
$page->editor->text('sort');

$page->callback->setAfterEditorHtml(function($id){
    ?>
    <script>
        function changeType(t){
            if ($(t).val() == 1) {
                $('.class-set-file').show();
                $('.class-set-url').hide();
            } else {
                $('.class-set-file').hide();
                $('.class-set-url').show();
            }
        }

        $(function(){
            changeType('#set-type');
        })
    </script>
    <?php
});

$page->callback->setBeforeInsertSave(function($data, $customData){
    if ($data['type'] == 1 && $data['file'] == "") {
        exit(json_encode(array('err' => "請上傳檔案")));
    }
    if ($data['type'] == 2 && trim($data['url']) == "") {
        exit(json_encode(array('err' => "請輸入連結網址")));
    }
    Common::checkDateAndReturnErrMsg($data);
    return $data;
});

$page->callback->setBeforeUpdateSave(function($data, $originalData, $customData){
    if ($data['type'] == 1 && $data['file'] == "") {
        exit(json_encode(array('err' => "請上傳檔案")));
    }
    if ($data['type'] == 2 && trim($data['url']) == "") {
        exit(json_encode(array('err' => "請輸入連結網址")));
    }
    Common::checkDateAndReturnErrMsg($data);
    return $data;
});