<?php
include "common.php";

use admin\lib\AdminPortal;
use lib\Config;
use lib\Db;

$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->txt('Question' ,'question' ,1 ,10000);
$page->field->txt('Answer', 'answer', 1, 10000, '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>');
$page->field->sort('排序', 'sort', ($page->getMaxSort()+1));
$page->field->status();
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(1);
$page->search->setDefaultSort('sort', 'ASC');

//表格===========================
$page->table->txt('question');
$page->table->input('sort');
$page->table->select('status');
$page->table->mod();
$page->table->del();

//新增刪除===========================
$page->editor->textarea('question');
$page->editor->ckeditor('answer');
$page->editor->text('sort');
$page->editor->select('status');
