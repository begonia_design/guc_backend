<?php
ini_set("display_errors", "On"); // 顯示錯誤是否打開( On=開, Off=關 )
error_reporting(E_ALL & ~E_NOTICE);

header("content-type:text/html; charset=utf-8");
//header("Content-Security-Policy: default-src *; script-src 'unsafe-inline'");
mb_internal_encoding("UTF-8");
date_default_timezone_set("Asia/Taipei");

define("SITE_DIR", dirname(dirname(__FILE__))); //定義網站根目錄
include SITE_DIR . '/php/vendor/autoload.php';

session_start();

if (basename($_SERVER['PHP_SELF']) === 'login.php') {
    if (isset($_SESSION['userId'])) {
        header("location:index.php");
        exit;
    }
} else {
    if (!isset($_SESSION['userId'])) {
        header("location:login.php");
        exit;
    }
}

use enum\DepositType;
use lib\Config;
use lib\Db;

Logger::configure(Config::LOG);

class Common
{
    const HIDDEN = true;

    const LANG = Config::SITE_LANG;

    // page_name => [title, read, insert, update, delete]
    const MENU = [
        '系統管理' => [
            'admin_user' => ['帳號權限管理', 1, 1, 1, 1],
            'lang_json' => ['語系管理', 1, 0, 1, 0],
                //'admin_mail'    => ['發信設定', 1, 0, 1, 1],
            //'admin_config'  => ['網站設定', 1, 0, 1, 0]
        ],
        '首頁管理' => [
            'index_banner' => ['Banner 管理', 1, 1 , 1, 1],
            'index_content' => ['內容管理', 1, 0, 1, 0]
        ],
        'IP Portfolio' => [
            'sol_ip' => ['IP Portfolio', 1, 1 , 1, 1],
        ],
        'Financials' => [
            'inv_financials' => ['Overview', 1, 0, 1, 0],
            'inv_fin_revenues' => ['Revenues 圖表', 1, 1, 1, 1],
            'inv_fin_net' => ['Net Income 圖表', 1, 1, 1, 1],
            'inv_fin_monthly' => ['Monthly Revenues 表格', 1, 1, 1, 1],
            'inv_fin_mon_value' => ['Monthly Revenues 其他', 1, 0, 1, 0],
            'inv_fin_quarterly' => ['Quarterly Result', 1, 1, 1, 1],
            'inv_fin_annual' => ['Annual Report', 1, 1, 1, 1],
            'inv_fin_historical' => ['Historical Information', 1, 0, 1, 0],
            'inv_fin_report' => ['Financials Report', 1, 1, 1, 1],
            'inv_fin_calendar' => ['Financials Calendar', 1, 1, 1, 1],
            'inv_fin_tab' => ['頁籤－Financials', 1, 1, 1, 1],
//            'investor_board' => ['Board of Directors', 1, 1, 1, 1],
//            'investor_board_sub' => ['Board of Directors sub', 1, 1, 1, 1]
        ],
        'Corporate Governance' => [
            'inv_cor_board_title' => ['Board of Directors 大類別', 1, 1, 1, 1],
            'inv_cor_board' => ['Board of Directors 小類別', 1, 1, 1, 1],
            'inv_cor_committees' => ['Committees', 1, 1, 1, 1],
            'inv_cor_cgo' => ['Corporate Governance Officer', 1, 0, 1, 0],
            'inv_cor_policy' => ['Major Internal Policies', 1, 1, 1, 1],
            'inv_cor_tab' => ['頁籤－Corporate Governance', 1, 1, 1, 1],
        ],
        'Shareholders Services' => [
            'inv_sha_meeting' => ['Shareholders\' Meeting', 1, 1, 1, 1],
            'inv_sha_history' => ['Dividend History', 1, 1, 1, 1],
            'inv_sha_major' => ['Major Shareholders', 1, 1, 1, 1],
            'inv_sha_tab' => ['頁籤－Shareholders Services', 1, 1, 1, 1],
        ],
        'FAQ' => [
            'inv_faq' => ['FAQ', 1, 1, 1, 1],
        ],
        'Press Center' => [
            'news' => ['Press Release', 1, 1, 1, 1],
            'news_spotlight' => ['GUC Spotlight', 1, 1, 1, 1],
            'event' => ['Events', 1, 1, 1, 1],
        ],
        'ESG' => [
            'esg' => ['ESG at GUC', 1, 1, 1, 1],
            'esg_report' => ['ESG Report', 1, 1, 1, 1]
        ],
        'Policy' => [
            'policy_risk' => ['Risk Management', 1, 1, 1, 1]
        ],
        'Privacy Policy' => [
            'privacy' => ['Privacy Policy', 1, 0, 1, 0]
        ],
        'Contact Us' => [
            'contact' => ['Contact Us', 1, 0, 1, 1]
        ],
        'Media Library' => [
            'media' => ['Media Library', 1, 1, 1, 1]
        ]

    ];

    public static function checkHidden($lang) {
        return !(Common::HIDDEN && $lang != 'tw');
    }

    const INDEX_SORT = [
        0 => '不顯示於首頁',
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6'
    ];

    public static function checkEndDate($startDate, $endDate): bool
    {
        if ($endDate != "") {
            if (strtotime($endDate) < strtotime($startDate)) {
                return false;
            }
        }
        return true;
    }

    public static function checkDateAndReturnErrMsg($data)
    {
        if (!Common::checkEndDate($data['start_date'], $data['end_date'])) {
            exit(json_encode(array('err' => "下架日不可早於上架日", 'id' => 'set-end_date')));
        }
    }

    public static function getDefaultHtml()
    {
        return '<h3>h3: GUC in-house Hyperscale Timing Model improve timing analysis efficiency for billions gate Huge Design</h3>
              <h4>h4: Sub Title</h4>
              <h5>h5: Sub Sub Title</h5>
              <h6>h6: Sub Sub Sub Title</h6>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
              <ul>
                <li>Challenge: Huge design STA long run time &amp; machine memory usage Challenge: Huge design STA long run time &amp; machine memory usage Challenge: Huge design STA long run time &amp; machine memory usage</li>
                <li>Solution: GUC hybrid STA methodology for huge design</li>
                <li>GUC hybrid model: optimized model + feedthrougth model</li>
                <li>Real case example : improve block model reduction rate to over 90% and shorten timing closure TAT to 10~30%
                  <ul>
                    <li>item 1</li>
                    <li>item 2</li>
                    <li>item 3</li>
                    <li>item 4
                      <ul>
                        <li>item 1</li>
                        <li>item 2</li>
                        <li>item 3</li>
                        <li>item 4</li>
                      </ul>
                    </li>
                  </ul>
                </li>
              </ul><br>
              <h4>Sub Title</h4>
              <ol>
                <li>Challenge: Huge design STA long run time &amp; machine memory usage Challenge: Huge design STA long run time &amp; machine memory usage Challenge: Huge design STA long run time &amp; machine memory usage</li>
                <li>Solution: GUC hybrid STA methodology for huge design</li>
                <li>GUC hybrid model: optimized model + feedthrougth model</li>
                <li>Real case example : improve block model reduction rate to over 90% and shorten timing closure TAT to 10~30%
                  <ol>
                    <li>item 1</li>
                    <li>item 2</li>
                    <li>item 3</li>
                    <li>item 4</li>
                  </ol>
                </li>
              </ol>
              <p><a href="news-cont.html">Link1: The link has already visited. 已去過的連結</a></p>
              <p><a href="news-cont1.html">Link2: Normal. 未去過的連結</a></p><br><br>
              <p style="text-align: center"><img src="../../en/images/news/news-img-01.jpg" alt="圖片說明"></p>
              <table>
                <tbody>
                  <tr>
                    <td>title</td>
                    <td>title</td>
                    <td>title</td>
                    <td>title</td>
                  </tr>
                  <tr>
                    <td>cont</td>
                    <td>cont</td>
                    <td>cont</td>
                    <td>cont</td>
                  </tr>
                  <tr>
                    <td>cont</td>
                    <td>cont</td>
                    <td>cont</td>
                    <td>cont</td>
                  </tr>
                </tbody>
              </table>';
    }

    public static function newsTopButton(){
        ?>
        <button type="button" class="btn btn-primary" onclick="clickSortableBtn()">
            首頁排序
        </button>
        <?php
    }


    public static function newsQuickSort(){
        ?>
        <div class="modal fade text-left" id="sortableModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel">首頁排序管理</h4>
                    </div>
                    <div class="modal-body">
                        <form id="indexSortableForm">
                            <ul id="indexSortable" class="list-group">

                            </ul>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                        <button type="button" class="btn btn-primary" onclick="saveSortable()">更新排序</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <script>
            // $(function(){
            //     $('#indexSortable').sortable();
            // })

            function clickSortableBtn() {
                $.post('ajax.php?type=getIndexNews',function(rs){
                    liHtml = "";
                    $.each( rs, function( key, value ) {
                        labelText = (value.page ==='news')?'Press Release':'GUC Spotlight';
                        iconHtml = '<span class="glyphicon glyphicon-resize-vertical"></span>';
                        labelHtml = iconHtml + '<span class="badge">'+labelText+'</span> ';
                        liHtml += '<li value="'+value.page+','+value.id+'" class="list-group-item">'+ labelHtml + value.title+'</li>';
                    });
                    $('#indexSortable').html(liHtml);
                    $('#indexSortable').sortable();
                },'json')
                $('#sortableModal').modal('show');
            }

            function saveSortable() {
                var sortedIDs = $( "#indexSortable" ).sortable( "toArray" , {attribute:'value'});
                console.log(sortedIDs);
                $.post('ajax.php?type=saveIndexNewsSort', {'sort':sortedIDs},function(rs){
                    alert(rs.msg);
                    getTable();
                    $('#sortableModal').modal('hide');
                },'json')
            }
        </script>
        <?php
    }
}

