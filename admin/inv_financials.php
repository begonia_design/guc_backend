<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->txt('內文' ,'html' ,1 ,51200);
$page->field->txt('Market Capitalization' ,'mc_count' ,1 ,20);
$page->field->date('Market Capitalization Date' ,'mc_date' ,1 ,20);
$page->field->int('Shares Outstanding' ,'so_count' ,1 ,20);
$page->field->date('Shares Outstanding Date' ,'so_date' ,1 ,20);
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(0);
$page->search->setDefaultSort('id', 'DESC');

//表格===========================
$page->table->txt('html');
$page->table->mod();

//新增刪除===========================
$page->editor->ckeditor('html');
$page->editor->text('mc_count');
$page->editor->date('mc_date');
$page->editor->text('so_count');
$page->editor->date('so_date');
