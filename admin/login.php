<?php
include 'common.php';

use lib\Db;

if ($_GET['type'] == 'login') {

    $login_check = $_SESSION['login'];
    unset($_SESSION['login']);

    if ($login_check == '' OR $login_check != $_POST['check']) {
        exit(json_encode(array('err' => '驗証碼錯誤！')));
    }

    /*
    if(!tools::check_recaptche()){
        exit(json_encode(array('err'=>'驗証碼錯誤！')));
    }
    */
    $sql = "
		SELECT id 
		FROM admin_user 
		WHERE account = :account
		  AND password = :password
		  AND status = 1 ";

    $userId = Db::getBindRowData($sql, array(':account' => $_POST['account'], ':password' => $_POST['password']), 'id');

    if (!$userId) {
        exit(json_encode(array('err' => '帳號密碼錯誤！')));
    } else {
        $_SESSION['userId'] = $userId;
        $_SESSION['lang'] = 'en';
        exit(json_encode(array('url' => 'index.php')));
    }
}
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/owl.carousel.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min43.css">
    
    <!-- Style -->
    <link rel="stylesheet" href="css/style.login.css">

    <title>GUC Admin</title>
  </head>
  <body>
  

  <div class="d-md-flex half">
    <div class="bg" style="background-image: url('images/bg_1.jpg');"></div>
    <div class="contents">

      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-12">
            <div class="form-block mx-auto">
              <div class="text-center mb-5">
              <h3><strong>GUC</strong> 後端管理系統</h3>
              <!-- <p class="mb-4">Lorem ipsum dolor sit amet elit. Sapiente sit aut eos consectetur adipisicing.</p> -->
              </div>
              <form action="#" id="form-login" onSubmit="return false;">
                <div class="form-group first">
                  <label for="username">Account</label>
                  <input type="text" name="account" class="form-control" placeholder="Account" id="username">
                </div>
                <div class="form-group last mb-4">
                  <label for="password">Password</label>
                  <input type="password" name="password" class="form-control" placeholder="Password" id="password">
                </div>

                <div class="form-row last mb-4">
                  <div class="col "><input type="text" class="form-control" name="check" placeholder="CAPTCHA" id="check"></div>
                  <div class="col"><a href="javascript:changeCheckImg();" class="thumbnail"><img class="img-fluid img-thumbnail" src="../checkimg.php?n=login"
                                                                                                 style="width: 100%;height:auto;" alt=""></a></div>
                </div>

                <input type="submit" value="Log In" class="btn btn-block btn-primary">

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    
  </div>
    
    

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script>
      $(function () {
        $('#form-login').submit(function () {
          $.post('login.php?type=login', $('#form-login').serializeArray(), function (rs) {
            if (rs.err != undefined) {
              alert(rs.err);
              changeCheckImg();
            } else {
              location.href = rs.url;
            }
          }, 'json');
          return false;
        });
      })

      function changeCheckImg() {
        $('#checkimg').attr('src', '../checkimg.php?n=login&' + Math.random());
      }
    </script>
  </body>
</html>