<?php
include "common.php";

use admin\lib\AdminPortal;
use lib\Config;
use lib\Db;

$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}");

$page->field->id();
$page->field->txt('資料夾' ,'folder' ,0 ,30);
$page->field->int('類別', 'type', 1, 0)->setEnum([0 => '媒體庫', 1 => '編輯器']);
$page->field->txt('檔案' ,'file' ,1 ,255);
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(1);
$page->search->setDefaultSort('id', 'DESC');
$page->search->select('type');
$page->search->text('file');

//表格===========================
//$page->table->txt('id', 70, 'text-center')->setTdClass('text-center');
$page->table->txt('create_time');
$page->table->txt('type');
$page->table->txt('folder');
$page->table->txt('file');
$page->table->custom('連結', function($key, $rowData){
    if ($rowData['type'] == 1) {
        $folder = $rowData['folder']."/";
    } else {
        $folder = "media/";
        $folder .= ($rowData['folder'] == "") ? "" : "{$rowData['folder']}/";
    }
    $linkTxt = Config::SITE_URL."/".Config::UPLOAD_DIR.$folder.$rowData['file'];
    $imageSrc = "../".Config::UPLOAD_DIR.$folder.$rowData['file'];
    $js = "<script>$('#tooltipImage{$key}').tooltip({html: true});</script>";
    return "<a id=\"tooltipImage{$key}\" href=\"#\" data-toggle=\"tooltip\" title=\"<img class='img-thumbnail' src='{$imageSrc}'/>\">{$linkTxt}</a>{$js}";
});
$page->table->del();

//新增刪除===========================
$page->editor->select2('folder', false, true);
$page->editor->file('file','');


$page->callback->setBeforeEditorEnum(function($enum, $data){
    $sql = "SELECT folder FROM media WHERE type = 0 AND folder != '' GROUP BY folder ";
    $data = Db::getRowArray($sql,'folder', 'folder');
    $enum['folder'] = $data;
    return $enum;
});

$page->callback->setBeforeInsertSave(function($data, $customData){
    $data['type'] = 0;
    return $data;
});

$page->callback->setFileDir(function($fileDir, $data){
    if ($data['folder']!="") {
        $fileDir .= "{$data['folder']}/";
        Logger::getLogger("upload Folder")->debug($fileDir);
    }
    return $fileDir;
});

$page->callback->setBeforeDelete(function($id){
    $sql = "SELECT * FROM media WHERE id = :id";
    $data = Db::getBindRowData($sql, [':id' => $id]);
    if ($data['type'] == 1) {
        $folderPath = $data['folder']."/";
    } else {
        $folderPath = ($data['folder'] == "") ? "" : "{$data['folder']}/";
        $folderPath = "media/{$folderPath}";
    }
    $fileFolder = SITE_DIR . "/" . Config::UPLOAD_DIR . $folderPath;
    $filePath = $fileFolder. $data['file'];
    if (is_file($filePath)) {
        if(unlink($filePath)){
            // 刪除空目錄
            if($data['folder'] != "" && count(scandir($fileFolder)) == 2){
                rmdir($fileFolder);
            }
        } else {
            Logger::getLogger('delete file error')->error($filePath);
        }
    }
});




syncCkfinderFile();
function syncCkfinderFile() {
//    $extTxt = 'bmp,gif,jpeg,jpg,png,7z,aiff,asf,avi,bmp,csv,doc,docx,fla,flv,gif,gz,gzip,jpeg,jpg,mid,mov,mp3,mp4,mpc,mpeg,mpg,ods,odt,pdf,png,ppt,pptx,qt,ram,rar,rm,rmi,rmvb,rtf,sdc,swf,sxc,sxw,tar,tgz,tif,tiff,txt,vsd,wav,wma,wmv,xls,xlsx,zip';
//    $ext = explode($extTxt, ',');
    $dbFile = Db::getRowArray("SELECT * FROM media WHERE type = 1",'file','file');
    if (!$dbFile) {
        $dbFile = [];
    }

    $folder = SITE_DIR . "/" . Config::UPLOAD_DIR . "images";

    $now = date("Y-m-d H:i:s");
    $fileList = array_filter(scandir($folder), function($item) use ($folder) {
        return !is_dir($folder . $item);
    });
    $existFile = [];
    if ($fileList) {
        foreach ($fileList as $file) {
            if (is_file($folder."/".$file)) {
                $existFile[$file] = $file;
                if (!isset($dbFile[$file])) {
                    $data = [
                        'type' => 1,
                        'folder' => 'images',
                        'file' => $file,
                        'create_time' => $now,
                        'update_time' => $now
                    ];
                    Db::insert('media', $data);
                }
            }
        }
    }

    if ($dbFile) {
        foreach ($dbFile as $file) {
            if (!isset($existFile[$file])) {
                Db::bindDelete('media',"type = 1 AND file = :file",[':file' => $file]);
            }
        }
    }
}