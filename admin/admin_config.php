<?php
include "common.php";

use admin\lib\AdminPortal;

$page = new AdminPortal(basename(__FILE__, '.php'));

$page->field->id();
$page->field->txt('說明' ,'title' ,1 ,100);
$page->field->txt('內容' ,'content' ,1 ,2000);

//搜尋===========================

$page->search->setDefaultSort('id', 'ASC');
$page->search->text('title');

//表格===========================
$page->table->txt('id', 70, 'text-center')->setTdClass('text-center');
$page->table->txt('title');
$page->table->txt('content');
$page->table->mod();

//新增刪除===========================
$page->editor->txt('title');
$page->editor->textarea('content');



