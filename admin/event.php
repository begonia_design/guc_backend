<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->date('活動開始日' ,'event_start_date' ,1);
$page->field->date('活動結束日' ,'event_end_date' ,1);
$page->field->txt('標題' ,'title' ,1 ,100);
$page->field->txt('連結', 'url', 0, 200);
$page->field->date('上架日' ,'start_date' ,10 , date("Y-m-d"));
$page->field->date('下架日' ,'end_date' ,0 );
$page->field->status();
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(0);
$page->search->setDefaultSort('event_start_date', 'DESC');
$page->search->select('status');

//表格===========================
//$page->table->txt('id', 70, 'text-center')->setTdClass('text-center');
$page->table->txt('event_start_date');
$page->table->txt('event_end_date');
$page->table->txt('title');
$page->table->txt('start_date');
$page->table->txt('end_date');
$page->table->select('status');
$page->table->mod();
$page->table->del();

//新增刪除===========================
$page->editor->date('event_start_date');
$page->editor->date('event_end_date');
$page->editor->text('title');
$page->editor->text('url');
$page->editor->date('start_date');
$page->editor->date('end_date');
$page->editor->select('status');
