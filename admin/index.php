<?php
include "common.php";

use admin\lib\AdminPortal;
use lib\Db;

$page = new AdminPortal(basename(__FILE__, '.php'));

$page->callback->setContent(function(){
    $sql = "SELECT COUNT(1) AS c FROM contact WHERE status = 0";
    $count = Db::getRowData($sql,'c');
    if ($count > 0) {
        echo '<div class="alert alert-info" role="alert">尚有'.$count.'則<a class="alert-link" href="contact.php">聯絡我們</a>待處理</div>';

    }
});
