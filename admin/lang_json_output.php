<?php
include "common.php";


$lang = file_get_contents(SITE_DIR."/php/template/layout/lang.json");
$langJson = json_decode($lang, true);

$spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
$i = 0;
foreach($langJson as $page => $langArr) {
    if ($i > 0) {
        $spreadsheet->createSheet();
    }
    $spreadsheet->setActiveSheetIndex($i++);
    $worksheet = $spreadsheet->getActiveSheet();
    //工作表標題
    $worksheet->setTitle($page);
    $worksheet
        ->setCellValue('A1', 'Key')
        ->setCellValue('B1', 'En')
        ->setCellValue('C1', 'Tw')
        ->setCellValue('D1', 'Cn');
    $j = 1;
    foreach ($langArr as $k => $v) {
        $str = substr($k, -3);
        if ($str != "_tw" && $str != "_cn") {
            $j++;
            $spreadsheet->getActiveSheet()
                ->setCellValue('A' . $j, $k)
                ->setCellValue('B' . $j, $v)
                ->setCellValue('C' . $j, $langArr["{$k}_tw"])
                ->setCellValue('D' . $j, $langArr["{$k}_cn"]);
        }
    }
    $spreadsheet->getActiveSheet()->getProtection()->setSheet(true);
    //$spreadsheet->getActiveSheet()->protectCells('A2:A'.$j, "php");
    $spreadsheet->getActiveSheet()->getStyle('B2:D'.$j)->getProtection()
        ->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
}
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="GUC_lang.xls"'); //檔案名稱
header('Cache-Control: max-age=0');

$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
$writer->save('php://output');