<?php
include "common.php";

use admin\lib\AdminPortal;


$_POST['id'] = $_SESSION['userId'];

$page = new AdminPortal(basename(__FILE__, '.php'), 'admin_user');
$page->field->set('編號', 'id', 'int', 1, 11, $_SESSION['userId']);
$page->field->set('帳號', 'account', 'txt', 1, 20);
$page->field->set('密碼', 'password', 'txt', 1, 20);
$page->field->set('名稱', 'name', 'txt', 1, 20);
$page->field->set('狀態', 'status', 'int', 1, 1);

//新增刪除===========================
$page->editor->txt('account');
$page->editor->text('password');
$page->editor->text('name');

$page->callback->setContent(function () use ($page) {
    $page->editor->outputEditorHtml();
    ?>
    <script>
        $(function () {
            $('#editor').show();
            $('#editor-title').text('修改密碼');
            $('#editor-title').next().remove();
            $.post('?type=getEditor', function (html) {
                $('#editor-content').html(html);
            });
        })
    </script>
    <?php
    exit;
});

$page->callback->setBeforeUpdateSave(function ($data, $originalData, $otherData) {
    if ($originalData['id'] != $_SESSION['userId']) {
        exit(json_encode(array('err' => '權限錯誤')));
    }
    return $data;
});
