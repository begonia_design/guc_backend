<?php
include "common.php";

use admin\lib\AdminPortal;
use lib\Db;
use model\AdminUserDao AS d;

$page = new AdminPortal(basename(__FILE__, '.php'));

//欄位設定=======================
$page->field->id()
            ->txt('帳號', d::ACCOUNT, 4, 12)
                ->setPreg('/^([0-9A-Za-z]+)$/', "只能為英文或數字")
            ->txt('密碼' , d::PASSWORD, 4, 16)
                ->setPreg('/^([0-9A-Za-z]+)$/', "只能為英文或數字")
            ->txt('名稱', d::NAME, 1, 20)
            ->email('Email', d::EMAIL, 0 ,50)
            ->status();

//搜尋===========================
$page->search->setDefaultSort('id', 'ASC');
$page->search->text('name');
$page->search->select('status');

//表格===========================
//$page->table->txt('id', 70, 'text-center')->setTdClass('text-center');
$page->table->txt('account');
$page->table->txt('name');
$page->table->txt('email');
$page->table->select('status');
$page->table->mod();
$page->table->del('刪除','70','text-center',function($rowData){
    return ($rowData['id'] > 1);
});

//表單===========================
$page->editor->text('name');
$page->editor->text('account');
$page->editor->text('password');
$page->editor->text('email');
$page->editor->select('status');


$page->callback->setAfterEditorHtml(function ($id) {

    //$menu = AdminUser::getAdminMenu();
    $menu = Common::MENU;

    $auth = ($id > 0) ? AdminUser::getAdminAuthority($id) : array();

    $authArr = array(1 => 'p_read', 2 => 'p_insert', 3 => 'p_update', 4 => 'p_delete');
    $authTxt = array(1 => '讀取', 2 => '新增', 3 => '修改', 4 => '刪除');

    $html = "";

    $customTitle = array(

    );

    if ($menu) {
        $html = "
				<div class=\"form-group\">
					<label class=\"col-sm-2 control-label\" for=\"auth-0\">全選</label>
					<div class=\"col-sm-10\">
						<label class=\"checkbox-inline\">
						  <input type=\"checkbox\" value=\"1\" class=\"check-all\">全選
						</label>
					</div>
				</div>";

        $i = 1;
        foreach ($menu as $k => $v) {
            $html .= "<div class=\"alert alert-info\" role=\"alert\">{$k}</div>";

            foreach ($v as $k1 => $v1) {
                $authCheckbox = "
					<label class=\"checkbox-inline\">
					  <input type=\"checkbox\" class=\"auth check-row\" value=\"{$k1}\">全選
					</label>";

                foreach ($authArr as $k2 => $v2) {
                    if ($v1[$k2] == 1) {
                        $checked = ($auth[$k1][$v2] == 1 || $id == 1) ? 'checked' : "";
                        $readonly = ($id == 1) ? 'readonly' : "";
                        $authCheckbox .= "
						<label class=\"checkbox-inline\">
						  <input type=\"checkbox\" class=\"auth auth-{$k1}\" id=\"auth-{$k1}-{$v2}\" name=\"set[auth][{$k1}][{$v2}]\" {$checked} value=\"1\" {$readonly}> {$authTxt[$k2]}
						</label>";
                    }
                }

                $title = (isset($customTitle[$k]))?$customTitle[$k]:$v1[0];
                $html .= AdminUser::rowHtml($title, "auth-{$i}", $authCheckbox);
            }
            $i++;
        }







//        $subMenuId = 0;
//        foreach ($menu as $k => $v) {
//            $authCheckbox = "
//					<label class=\"checkbox-inline\">
//					  <input type=\"checkbox\" class=\"auth check-row\" value=\"{$k}\">全選
//					</label>";
//            foreach ($authArr as $k1 => $v1) {
//                if ($v[$k1] == 1) {
//                    $checked = ($auth[$k][$k1] == 1 || $id == 1) ? 'checked' : "";
//                    $readonly = ($id == 1) ? 'readonly' : "";
//                    $authCheckbox .= "
//						<label class=\"checkbox-inline\">
//						  <input type=\"checkbox\" class=\"auth auth-{$k}\" id=\"auth-{$k}-{$k1}\" name=\"set[auth][{$k}][{$k1}]\" {$checked} value=\"1\" {$readonly}> {$v1}
//						</label>";
//                }
//            }
//            if ($subMenuId != $v['sub_id']) {
//                $html .= "<div class=\"alert alert-info\" role=\"alert\">{$v['sub_title']}</div>";
//                $subMenuId = $v['sub_id'];
//            }
//            $title = (isset($customTitle[$k]))?$customTitle[$k]:$v['title'];
//            $html .= AdminUser::rowHtml($title, "auth-{$k}", $authCheckbox);
//        }
        $html = AdminUser::rowHtml("權限設定", "power", $html);
        $html .= "
				<script>
					$(function(){
						$('input.check-row').change(function(){
							var id = $(this).val();
							$('input.auth-'+id).prop('checked',$(this).prop('checked'));
						});
						$('input.check-all').change(function(){
							$('input.auth').prop('checked',$(this).prop('checked'));
						});
					})
				</script>";
    }
    echo $html;
});


$page->callback->setAfterInsertSave(function ($data, $otherData) {
    AdminUser::saveAdmin($data['id']);
});

$page->callback->setAfterUpdateSave(function ($data, $originalData, $otherData) {
    AdminUser::saveAdmin($originalData['id']);
});

$page->callback->setbeforeDelete(function ($id) {
    if ($id == 1) {
        exit(json_encode('不可刪除管理者'));
    }
});

$page->callback->setAfterDelete(function ($id) {
    AdminUser::delAdmin($id);
});


class AdminUser
{

    //刪除使用者權限
    public static function delAdmin($id)
    {
        Db::bindDelete("admin_power", "user_id = :userId", array(':userId' => $id));
    }

    //新增使用者權限
    public static function saveAdmin($keyId)
    {
        self::delAdmin($keyId);
        $power = $_POST['set']['auth'];
        if ($power) {
            foreach ($power as $menuId => $data) {
                if (!isset($data['p_read'])) {
                    $data['p_read'] = 0;
                }
                if (!isset($data['p_insert'])) {
                    $data['p_insert'] = 0;
                }
                if (!isset($data['p_update'])) {
                    $data['p_update'] = 0;
                }
                if (!isset($data['p_delete'])) {
                    $data['p_delete'] = 0;
                }
                $data['user_id'] = $keyId;
                $data['menu'] = $menuId;
                Db::insert('admin_power', $data);
            }
        }
    }

    //取得權限目錄
    public static function getAdminMenu()
    {
        $sql = "
			SELECT m.*,s.title AS sub_title 
			FROM admin_menu m
			JOIN admin_menu_sub s ON m.sub_id = s.sub_id
			WHERE m.status = 1 ORDER BY s.sort,m.sort";
        return Db::getRowArray($sql, 'menu_id');
    }

    //取得使用者權限
    public static function getAdminAuthority($id)
    {
        $sql = "SELECT * FROM admin_power WHERE user_id = :id";
        return Db::getBindRowArray($sql, array(':id' => $id), 'menu');
    }


    //輸出編輯頁HTML
    public static function rowHtml($title, $id, $input, $memo = '')
    {
        $memo_html = ($memo == '') ? '' : '<span class="badge">' . $memo . '</span>';
        $html = '
			<div class="form-group class-' . $id . '">
				<label for="' . $id . '" class="col-sm-2 control-label">' . $title . '</label>
				<div class="col-sm-10">
				  ' . $input . $memo_html . '
				</div>
			</div>';
        return $html;
    }

}