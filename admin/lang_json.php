<?php
include "common.php";

use admin\lib\AdminPortal;
use lib\Db;

$langPath = SITE_DIR . "/php/template/layout/";
$langBakPath = $langPath . "bak/";

if ($_GET['type'] == "uploadLang") {
    $file = $_POST['fileName'];
    $filePath = SITE_DIR ."/". \lib\Config::UPLOAD_TEMP. $file;
    if (is_file($filePath)) {
        $dataArr = array();
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        try {
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($filePath);

            //讀取目前語系檔
            $origFile = file_get_contents($langPath . "lang.json");
            $langJson = json_decode($origFile, true);

            //excel檔轉換格式
            foreach ( $spreadsheet->getWorksheetIterator() as $data ) {
                $arr = $data->toArray();
                $newJsonArr = [];
                foreach ($arr as $k => $v) {
                    if ($k > 0) {
                        $newJsonArr[$v[0]] = $v[1];
                        if (trim($v[2]) != "") {
                            $newJsonArr[$v[0]."_tw"] = $v[2];
                        }
                        if (trim($v[3]) != "") {
                            $newJsonArr[$v[0]."_cn"] = $v[3];
                        }
                    }
                }
                $dataArr[$data->getTitle()] = $newJsonArr;
            }

            //Logger::getLogger(__METHOD__)->info(json_encode($dataArr));

            //以原檔案格式為主更新
            foreach($langJson as $page => $langArr) {
                if (isset($dataArr[$page])) {
                    $newArr = $dataArr[$page];
                    //Logger::getLogger($page)->info(json_encode($newArr));
                    foreach ($langArr as $k => $v) {
                        $str = substr($k, -3);
                        if ($str != "_tw" && $str != "_cn") {
                            if (isset($newArr[$k]) && trim($newArr[$k]) != "") {
                                $langJson[$page][$k] = $newArr[$k];
                            }
                            if (isset($newArr[$k."_tw"]) && trim($newArr[$k."_tw"]) != "") {
                                //Logger::getLogger($k)->info($newArr[$k."_tw"]);
                                $langJson[$page][$k."_tw"] = $newArr[$k."_tw"];
                            }
                            if (isset($newArr[$k."_cn"]) && trim($newArr[$k."_cn"]) != "") {
                                $langJson[$page][$k."_cn"] = $newArr[$k."_cn"];
                            }
                        }
                    }
                }
            }

            if (!copy($langPath."lang.json", $langBakPath."lang".date("YmdHis").".json")) {
                throw new Exception("copy error");
            }

            $jsonFileName = $langPath."lang.json";
            if (($jsonRes = fopen($jsonFileName, "w")) === FALSE) {
                throw new Exception("fopen error");
            }

            if (!fwrite($jsonRes, json_encode($langJson, JSON_PRETTY_PRINT))) { //將資訊寫入檔案
                fclose($jsonRes);
                throw new Exception("fwrite error");
            }
            fclose($jsonRes); //關閉指標

        } catch (Exception $e) {
            Logger::getLogger(__METHOD__)->error($e->getMessage());
            echo json_encode(['status' => 'err', 'msg' => "只能上傳 xls"]);
            exit;
        }
        echo json_encode(['status' => 'success', 'msg' => '更新成功']);
        exit;
    }
    echo json_encode(['status' => 'err', 'msg' => '失敗']);
    exit;
}

if ($_GET['type'] == "download") {
    $lang = file_get_contents(SITE_DIR."/php/template/layout/lang.json");
    $langJson = json_decode($lang, true);

    $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
    $i = 0;
    foreach($langJson as $page => $langArr) {
        if ($i > 0) {
            $spreadsheet->createSheet();
        }
        $spreadsheet->setActiveSheetIndex($i++);
        $worksheet = $spreadsheet->getActiveSheet();
        //工作表標題
        $worksheet->setTitle($page);
        $worksheet
            ->setCellValue('A1', 'Key')
            ->setCellValue('B1', 'En')
            ->setCellValue('C1', 'Tw')
            ->setCellValue('D1', 'Cn');
        $j = 1;
        foreach ($langArr as $k => $v) {
            $str = substr($k, -3);
            if ($str != "_tw" && $str != "_cn") {
                $j++;
                $spreadsheet->getActiveSheet()
                    ->setCellValue('A' . $j, $k)
                    ->setCellValue('B' . $j, $v)
                    ->setCellValue('C' . $j, $langArr["{$k}_tw"])
                    ->setCellValue('D' . $j, $langArr["{$k}_cn"]);
            }
        }
        $spreadsheet->getActiveSheet()->getProtection()->setSheet(true);
        //$spreadsheet->getActiveSheet()->protectCells('A2:A'.$j, "php");
        $spreadsheet->getActiveSheet()->getStyle('B2:D'.$j)->getProtection()
            ->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);
    }
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="GUC_lang.xls"'); //檔案名稱
    header('Cache-Control: max-age=0');

    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
    $writer->save('php://output');
    exit;
}


$page = new AdminPortal(basename(__FILE__, '.php'));
$page->callback->setContent(function() use ($page) {
    ?>
    <form class="form-horizontal">
    <div class="form-group">
        <label class="col-sm-1 control-label">匯出</label>
        <div class="col-sm-11">
          <a href="lang_json.php?type=download" class="btn btn-default">匯出</a>
        </div>
    </div>
    <?php if ($page->authority->isUpdate()){ ?>
    <div class="form-group">
        <label class="col-sm-1 control-label">匯入</label>
        <div class="col-sm-11">
            <div id="file-upload-lang"></div>
        </div>
    </div>
    <?php } ?>
    </form>
    <script>
        $(function(){
            fileUpload('lang','',false, function(fileName, tempFileName) {
                $.post("?type=uploadLang",{fileName:tempFileName},function(rs){
                    if (rs.status == 'err') {
                        alert(rs.msg);
                    }
                    if (rs.status == 'success') {
                        alert(rs.msg);
                    }
                },'json');
            });
        })
    </script>
    <?php
//    echo '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
//    foreach($langJson as $page => $langArr) {
//        echo '<div class="panel panel-default">
//    <div class="panel-heading" role="tab" id="'.$page.'">
//      <h4 class="panel-title">
//        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#C_'.$page.'" aria-expanded="false" aria-controls="C_'.$page.'">
//          '.$page.'
//        </a>
//      </h4>
//    </div>
//    <div id="C_'.$page.'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="'.$page.'">
//      <div class="panel-body">';
//
//        echo "<form class=\"form-horizontal\">";
//        foreach ($langArr as $k => $v) {
//            $str = substr($k, -3);
//            if ($str != "_tw" && $str != "_cn") {
//                echo textArea($k, $v, $langArr["{$k}_tw"],$langArr["{$k}_cn"]);
//            }
//        }
//        echo "</form>";
//        echo '</div>
//    </div>
//  </div>';
//    }
});

function textArea($key, $value, $valueTw, $valueCn){
   return '
  <div class="form-group">
    <label for="'. $key .'" class="col-sm-1 control-label">英</label>
    <div class="col-sm-11">
      <p class="form-control-static">'.$value.'</p>
    </div>
  </div>
  <div class="form-group">
    <label for="'. $key .'_tw" class="col-sm-1 control-label">繁中</label>
    <div class="col-sm-11">
      <textarea class="form-control" name="'. $key .'_tw" id="'. $key .'_tw" rows="1">'.$valueTw.'</textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="'. $key .'_cn" class="col-sm-1 control-label">簡中</label>
    <div class="col-sm-11">
      <textarea class="form-control" name="'. $key .'_cn" id="'. $key .'_cn" rows="1">'.$valueCn.'</textarea>
    </div>
  </div><hr>';
}