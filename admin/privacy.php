<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->txt('內容', 'html', 1, 51200);
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(1);


//表格===========================
//$page->table->txt('id', 70, 'text-center')->setTdClass('text-center');
$page->table->txt('html');
$page->table->mod();

//新增刪除===========================
$page->editor->ckeditor('html');

