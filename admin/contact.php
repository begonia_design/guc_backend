<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}");

$page->field->id();
$page->field->txt('名' ,'first_name' ,1 ,50);
$page->field->txt('姓' ,'last_name' ,1 ,50);
$page->field->txt('公司' ,'company' ,1 ,50);
$page->field->txt('Email' ,'email' ,1 ,50);
$page->field->txt('國家' ,'country' ,1 ,50);
$page->field->txt('電話' ,'phone' ,1 ,50);
$page->field->txt('內容' ,'description' ,1 ,10240);
$page->field->status();
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(0);
$page->search->setDefaultSort('id', 'DESC');
$page->search->select('status');

//表格===========================
//$page->table->txt('id', 70, 'text-center')->setTdClass('text-center');
$page->table->txt('create_time');
$page->table->txt('first_name');
$page->table->txt('last_name');
$page->table->txt('company');
$page->table->txt('country');
$page->table->select('status');
$page->table->mod();
$page->table->del();

//新增刪除===========================
$page->editor->txt('create_time');
$page->editor->txt('first_name');
$page->editor->txt('last_name');
$page->editor->txt('company');
$page->editor->txt('email');
$page->editor->txt('country');
$page->editor->txt('phone');
$page->editor->txt('description');
$page->editor->select('status');
