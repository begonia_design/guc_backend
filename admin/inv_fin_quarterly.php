<?php
include "common.php";

use admin\lib\AdminPortal;
use lib\Config;
use lib\Db;

$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->int('年份' ,'year' ,4 ,4);
$page->field->int('季度' ,'quarterly' ,1 ,1)->setEnum([1=>'Q1', 2=>'Q2', 3=>'Q3', 4=>'Q4']);
$page->field->txt('標題', 'title', 1, 50);
$page->field->file('檔案', 'file', true);
$page->field->date('上架日' ,'start_date' ,10 , date("Y-m-d"));
$page->field->date('下架日' ,'end_date' ,0 );
$page->field->sort('排序', 'sort', ($page->getMaxSort()+1));
$page->field->createTime();
$page->field->updateTime();
if ($_POST['id'] == 0 || $_POST['set']['id'] == 0) {
    $page->field->custom('同步新增', 'sync', 1, 1, 1)->setEnum([1 => '同步新增到Financials Report', 2 => '不同步']);
}
//搜尋===========================
$page->setSearchStyle(1);
$page->search->setDefaultSort('year', 'DESC');
$page->search->setSecondSort('quarterly');
$page->search->text('year');
$page->search->select('quarterly');

//表格===========================
$page->table->txt('year');
$page->table->txt('quarterly');
$page->table->txt('title');
$page->table->txt('start_date');
$page->table->txt('end_date');
$page->table->input('sort');
$page->table->mod();
$page->table->del();

//新增刪除===========================
$page->editor->text('year');
$page->editor->select('quarterly');
$page->editor->text('title');
$page->editor->file('file','pdf,xls,xlsx,wmv,mov,mp4');
$page->editor->date('start_date');
$page->editor->date('end_date');
$page->editor->text('sort');
if ($_POST['id'] == 0) {
    $page->editor->select('sync');
}

$page->callback->setBeforeInsertSave(function($data, $customData){
    Common::checkDateAndReturnErrMsg($data);
    return $data;
});

$page->callback->setBeforeUpdateSave(function($data, $originalData, $customData){
    Common::checkDateAndReturnErrMsg($data);
    return $data;
});

$page->callback->setAfterInsertSave(function($data, $customData){
    Logger::getLogger(__METHOD__)->info(json_encode($customData));
    if ($customData['sync'] == 1) {
        $fromDir = SITE_DIR . "/" . Config::UPLOAD_DIR. "inv_fin_quarterly/";
        $toDir = SITE_DIR . "/" . Config::UPLOAD_DIR. "inv_fin_report/";
        if (is_file($fromDir.$data['file'])) {
            $fileName = pathinfo($data['file'],PATHINFO_FILENAME);
            $ext = pathinfo($data['file'],PATHINFO_EXTENSION);
            $newFileName = $data['file'];
            $i = 1;
            while(is_file($toDir . $newFileName)) {
                $newFileName = $fileName . "_" . $i++ . "." . $ext;
            }
            if (copy($fromDir.$data['file'], $toDir. $newFileName)) {
                $data['file'] = $newFileName;
                Db::insert('inv_fin_report_'.$_SESSION["lang"], $data);
            }else {
                Logger::getLogger(__METHOD__)->error('sync copy file error');
            }
        } else {
            Logger::getLogger(__METHOD__)->error('sync copy file error');
        }
    }
});

