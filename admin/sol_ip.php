<?php
include "common.php";

use admin\lib\AdminPortal;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

\model\SolIpDao::setBackend();

$page->field->id();
$page->field->txt('Part Number' ,'part_number' ,1 ,50);
$page->field->txt('Geometry', 'geometry', 1, 50);
$page->field->txt('網頁分類', 'page_name', 1, 50)->setEnum(\enum\IpPage::get());
$page->field->txt('標題', 'title', 1, 500);
$page->field->txt('說明', 'description', 0, 10000);
$page->field->txt('連結', 'download_url', 0, 255);
$page->field->sort('排序', 'sort', ($page->getMaxSort()+1));
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(1);
$page->search->setDefaultSort('sort', 'ASC');
$page->search->select('page_name');

//表格===========================
//$page->table->txt('id', 70, 'text-center')->setTdClass('text-center');
$page->table->txt('page_name');
$page->table->txt('part_number');
$page->table->txt('geometry');
$page->table->txt('title');
$page->table->txt('description');
$page->table->txt('download_url');
$page->table->input('sort');
$page->table->mod();
$page->table->del();

//新增刪除===========================
$page->editor->text('part_number');
$page->editor->text('geometry');
$page->editor->select('page_name');
$page->editor->text('title');
$page->editor->text('description');
$page->editor->textarea('download_url');
$page->editor->text('sort');


$page->callback->setImport(function($filePath) {
    $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
    try {
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($filePath);

        //讀取第一個sheet
        foreach ( $spreadsheet->getWorksheetIterator() as $data ) {
            $arr = $data->toArray();
            if ($arr) {
                \model\SolIpDao::deleteAll();
                unset($arr[0]);
                $i = 1;
                foreach ($arr as $k => $v) {
                    if (\enum\IpPage::isKey($v[2])) {
                        \model\SolIpDao::insert($v, $i++);
                    }
                }
            }
            break;
        }
    } catch (Exception $e) {
        Logger::getLogger(__METHOD__)->error($e->getMessage());
        echo json_encode(['status' => 'err', 'msg' => '只能上傳 xls']);
        exit;
    }
    echo json_encode(['status' => 'success', 'msg' => '匯入成功']);
    exit;
});

$page->callback->setExport(function(Spreadsheet $ss) {
    $ss->setActiveSheetIndex(0);
    $ss->getActiveSheet()
        ->setCellValue('A1', 'Part_Number')
        ->setCellValue('B1', 'Geometry_nm --> "Process"')
        ->setCellValue('C1', '網頁分類')
        ->setCellValue('D1', '內頁表格標題')
        ->setCellValue('E1', 'Description')
        ->setCellValue('F1', 'Datasheet --> Download');

    $data = \model\SolIpDao::getAllList();
    if ($data) {
        $i = 2;
        foreach ($data as $k => $v) {
            $ss->getActiveSheet()
                ->setCellValue('A' . $i, $v['part_number'])
                ->setCellValue('B' . $i, $v['geometry'])
                ->setCellValue('C' . $i, $v['page_name'])
                ->setCellValue('D' . $i, $v['title'])
                ->setCellValue('E' . $i, $v['description'])
                ->setCellValue('F' . $i, $v['download_url']);
            $i++;
        }
    }
    return $ss;
});