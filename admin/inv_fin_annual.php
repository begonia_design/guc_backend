<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->int('年份' ,'year' ,4 ,4);
$page->field->txt('圖片' ,'img' ,1 ,30);
$page->field->file('檔案' ,'file' ,true);
$page->field->date('上架日' ,'start_date' ,10 , date("Y-m-d"));
$page->field->date('下架日' ,'end_date' ,0 );
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(0);
$page->search->setDefaultSort('year', 'DESC');
$page->search->text('year');

//表格===========================
$page->table->txt('year');
$page->table->img('img');
$page->table->txt('start_date');
$page->table->txt('end_date');
$page->table->txt('update_time');
$page->table->mod();
$page->table->del();

//新增刪除===========================
$page->editor->text('year');
$page->editor->file('img')->setMemo("圖片建議尺寸 360*450");
$page->editor->file('file','xls,xlsx,pdf');
$page->editor->date('start_date');
$page->editor->date('end_date');

$page->callback->setBeforeInsertSave(function($data, $customData){
    Common::checkDateAndReturnErrMsg($data);
    return $data;
});

$page->callback->setBeforeUpdateSave(function($data, $originalData, $customData){
    Common::checkDateAndReturnErrMsg($data);
    return $data;
});