<?php
include "common.php";

use admin\lib\AdminPortal;
$fileName = basename(__FILE__, '.php');
$page = new AdminPortal($fileName, "{$fileName}_{$_SESSION["lang"]}");

$page->field->id();
$page->field->txt('URL ID', 'url_id', 1, 30)->setPreg("/^([0-9A-Za-z_]+)$/","URL_ID只能英文數字或底線");
$page->field->txt('標題' ,'title' ,1 ,200);
$page->field->txt('內容', 'html', 1, 51200, Common::getDefaultHtml());
$page->field->date('上架日' ,'start_date' ,10 , date("Y-m-d"));
$page->field->date('下架日' ,'end_date' ,0 );
$page->field->status();
$page->field->sort('排序', 'sort', ($page->getMaxSort()+1));
$page->field->createTime();
$page->field->updateTime();

//搜尋===========================
$page->setSearchStyle(0);
$page->search->setDefaultSort('sort', 'ASC');
$page->search->text('title');
$page->search->select('status');

//表格===========================
//$page->table->txt('id', 70, 'text-center')->setTdClass('text-center');
$page->table->txt('url_id');
$page->table->txt('title');
$page->table->txt('start_date');
$page->table->txt('end_date');
$page->table->select('status');
$page->table->input('sort');
$page->table->mod();
$page->table->del('刪除','70','text-center',function($rowData){
    return ($rowData['id'] > 3);
});

//新增刪除===========================
$page->editor->text('url_id');
$page->editor->text('title');
$page->editor->ckeditor('html');
$page->editor->date('start_date');
$page->editor->date('end_date');
$page->editor->select('status');
$page->editor->text('sort');

\model\PolicyRiskDao::setBackend();

$page->callback->setBeforeInsertSave(function($data, $customData){
    if (\model\PolicyRiskDao::getAllByUrlId($data['url_id'])) {
        exit(json_encode(array('err' => 'URL_ID重覆，請重新輸入')));
    }
    Common::checkDateAndReturnErrMsg($data);
    return $data;
});

$page->callback->setBeforeUpdateSave(function($data, $originalData, $customData){
    if (\model\PolicyRiskDao::getAllByUrlId($data['url_id'], $originalData['id'])) {
        exit(json_encode(array('err' => 'URL_ID重覆，請重新輸入')));
    }
    Common::checkDateAndReturnErrMsg($data);
    return $data;
});

