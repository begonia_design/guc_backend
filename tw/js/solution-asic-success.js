(function (window, document) {
  /* ---------------------------------------- [START] 整頁 Scroll 監測 (Before) */
  // 為了要 Scroll 到該物件才生成，一方面為了初始效能，另一方面為了呈現動畫
  var pageScrollCheckList = []; // 紀錄物件
  var pageScrollAdd = function pageScrollAdd(selector, buildFunction) {
    var el = typeof selector === 'string' ? document.querySelector(selector) : selector;
    if (el === null) {
      return false;
    }
    pageScrollCheckList.push({
      build: false,
      el: el,
      fun: buildFunction });

  };
  var pageScrollClean = function pageScrollClean() {
    pageScrollCheckList = Array.prototype.filter.call(pageScrollCheckList, function (item) {
      return item.build === false;
    });
  };
  /* ---------------------------------------- [END] 整頁 Scroll 監測 (Before) */

  /* ---------------------------------------- [START] Chart Module */
  var chart = []; // 紀錄生成後的 Chart 物件(Google Chart)
  var chartIndex = 0; // 對 Chart 計數

  // 為了符合設計稿、手機板樣式優化，要客製化生成legend
  // 客戶提供的範例 3nm 顯示要在最上面（最後生成的資料），所以順序要顛倒：28nm above → 3nm
  // 28nm above, 16/12nm, 7/6nm, 5nm, 3nm
  var colors = ['#009966', '#46cd59', '#9eeb67', '#4a96c0', '#6fccff']; // 顏色使用變數
  var legends = ['28nm above', '16/12nm', '7/6nm', '5nm', '3nm'];

  // 設定圖表選項
  var chartOptionGet = function chartOptionGet(data) {
    var isSmall = window.innerWidth < 640;
    var isMedium = window.innerWidth < 1024;

    return {
      width: '100%',
      height: window.innerWidth < 1024 ? 250 : 530,
      backgroundColor: { fill: 'transparent' },
      tooltip: {
        trigger: 'none' // Hover 不顯示資料
      },
      // legend: {
      // 	position: isSmall ? 'top' : 'right', // 圖例要顯示在圖表的位置哪
      // 	alignment: isSmall ? 'start' : 'end', // 圖例要對齊區域的[開始|中間|結束]
      // },
      legend: 'none',
      // 客戶提供的範例 3nm 顯示要在最上面（最後生成的資料），所以順序要顛倒：28nm above → 3nm
      // 28nm above, 16/12nm, 7/6nm, 5nm, 3nm
      colors: colors,
      // bar: { groupWidth: '80%' },
      isStacked: true, // 將Bar疊層
      animation: {
        // 載入動畫
        startup: true,
        duration: 1000,
        easing: 'out' },

      chartArea: {
        top: 10,
        left: 30,
        bottom: 40,
        right: isMedium ? isSmall ? 0 : 100 : 180 },

      vAxis: {
        // Y軸（垂直）
        format: 'decimal',
        textStyle: {
          fontSize: 13 // 設定字級大小
        } },

      hAxis: {
        // x軸（水平）
        // title: '', // 標題移除
        textStyle: {
          fontSize: isSmall ? 14 : 16 // 設定字級大小
        } } };


  };

  function drawChartInit(chartEl) {
    // 預防剛進入頁面時 google chart 尚未讀取完 => 延遲生成
    // 避免未讀取到： google.visualization
    if (!google.visualization || google.visualization && !google.visualization.DataTable) {
      setTimeout(function () {
        drawChartInit(chartEl);
      }, 1000);
      return false;
    }

    // 取得 DataTable HTML 物件（此頁用來擋下，二次讀取作用）
    var chartDataEl = chartEl.querySelector('.data-table');

    // 因為有時候 setOnLoadCallback 會 Call 兩次，以防萬一擋下來
    if (chartDataEl === null) {
      return false;
    }

    // Add Index Data
    chartEl.setAttribute('data-index', chartIndex);

    // [START] 建立 Data table
    var data = new google.visualization.DataTable();

    // 建立直欄資料
    data.addColumn('string', 'Year');
    legends.forEach(function (val) {
      data.addColumn('number', val);
    });

    // 建立橫列資料
    // 輸入照表格順序輸入：年份西元20XX、3nm → 28nm above <====== ★很重要
    // 如果資料是0，請填入「 data0 」 變數
    // 因客戶要求，盡量隱藏數字，年份作簡化
    var data0 = null; // 取代資料為0的項目（如果設為0，Hover該項目圖例還是會反映在圖表上）
    var dataTable = [
    // 年份西元20XX, 3nm, 5nm, 7/6nm, 28nm above
    [16, data0, data0, 1, 5, 35],
    [17, data0, data0, 3, 12, 26],
    [18, data0, data0, 3, 13, 17],
    [19, 1, 6, 10, 5, 15],
    [20, 1, 8, 9, 12, 12]];

    // ↓客戶提供的範例 3nm 顯示要在最上面（最後生成的資料），所以順序要顛倒：28nm above → 3nm
    for (var i = 0; i < dataTable.length; i++) {
      var yy = dataTable[i][0]; // 紀錄年份
      dataTable[i].splice(0, 1); // 先把年份移除
      dataTable[i].reverse(); // 順序反轉
      dataTable[i].splice(0, 0, '20' + yy); // 再把年份加入（另外把20加回來）
    }
    data.addRows(dataTable);
    // [END] 建立 Data table

    var option = chartOptionGet(data);

    // 建立object，以使用.draw方法畫出帶有data, options資料的圖表
    // 若要換成繪製圓餅圖改用google.visualization.PieChart，但data數據需要再調整
    var currentChart = new google.visualization.ColumnChart(chartEl);
    currentChart.draw(data, option);

    // =================================== Google Chart 以外的功能

    // 建立資料
    var chartData = {
      el: chartEl, // Chart 物件
      index: chartIndex, // Chart 序數
      dataEl: chartDataEl, // DataTable 物件
      data: data, // 只顯示 Bar的
      chart: currentChart, // Google Chart 生成後的資料
      showNum: false // Bar是否要顯示數字
    };

    chartEl.chart = chartData; // 記錄在物件上 el.chart
    chart.push(chartData); // 推上Array

    // =================================== START Event
    // 為了符合設計稿、手機板樣式優化，要客製化生成legend
    var legendList = document.createElement('ul');
    var legendClassStadard = 'asic-suc-chart__legend';
    legendList.className = legendClassStadard;

    // 為了符合圖表（最上面 === 最後生成的資料），要從最後一個開始生成
    for (var i = legends.length - 1; i >= 0; i--) {
      var val = legends[i];
      var legendItem = document.createElement('li');
      legendItem.className = "".concat(legendClassStadard, "__item");
      legendItem.innerHTML = "<span class=\"".concat(legendClassStadard, "__color\" style=\"color: ").concat(colors[i], "\"></span>").concat(val);

      legendItem.col = i + 1; // Hover Active 用

      // Hover 呼應相關的資料
      legendItem.addEventListener('mouseenter', function () {
        var e = { row: null, column: this.col };
        chartEl.chart.chart.setSelection([e]);
      });

      legendItem.addEventListener('mouseleave', function () {
        chartEl.chart.chart.setSelection([{ row: null, column: null }]);
      });

      legendList.appendChild(legendItem);
    }
    chartEl.parentElement.insertBefore(legendList, chartEl); // 生成在 Chart 的上方
    // =================================== END Event

    chartIndex += 1; // 增加 Index 序數
  }
  /* ---------------------------------------- [END] Chart Module */

  /* ---------------------------------------- [START] Draw Chart */
  // Init Chart
  function drawChart() {
    var chartTarget = document.querySelector('#asic-suc-chart');
    pageScrollAdd(chartTarget, function () {
      drawChartInit(chartTarget);
    });
  }
  window.addEventListener('load', drawChart);

  // 重製 Chart
  function redrawChart() {
    // Ref: https://stackoverflow.com/a/11147816/11240898
    for (var i = 0; i < chart.length; i++) {
      var option = chartOptionGet(chart[i].data);
      chart[i].chart.draw(chart[i].showNum ? chart[i].view : chart[i].data, option);
    }
    // chart.draw(data, options);
  }
  /* ---------------------------------------- [END] Draw Chart */

  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */

  /* ---------------------------------------- [START] 整頁 Scroll 監測 (After) */
  var pageScrollThrottle = null;
  var pageScrollHandler = function pageScrollHandler() {
    if (pageScrollCheckList.length === 0) {
      return false;
    } // 因為改成 Page Global 執行，所以不會停止

    for (var i = 0; i < pageScrollCheckList.length; i++) {
      if (isInViewport(pageScrollCheckList[i].el)) {
        pageScrollCheckList[i].fun();
        pageScrollCheckList[i].build = true;
      }
    }
    pageScrollClean();
    if (pageScrollCheckList.length === 0) {
      window.removeEventListener('scroll', pageScrollThrottle);
    }
  };
  window.addEventListener('load', function () {
    pageScrollThrottle = throttle(pageScrollHandler, 50, 1000); // 節流作用
    window.addEventListener('scroll', pageScrollThrottle);
    pageScrollHandler();
  });
  /* ---------------------------------------- [END] 整頁 Scroll 監測 (After) */

  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* 因為需要變數 pageScrollHandler()，所以 Chart Start 要搬到下方 */

  /* ---------------------------------------- [START] Start Chart */
  /* START Chart */
  function chartInit() {
    var htmlLang = document.documentElement.getAttribute('lang');
    var language = htmlLang === 'zh-Hant-TW' ? 'zh-tw' : htmlLang.match(/zh-/g) ? 'zh-cn' : 'en';
    /* 載入Visualization API，括號裡第一個參數是版本名稱或數字 */
    google.charts.load('current', {
      packages: ['corechart'],
      language: language });

    // google.charts.setOnLoadCallback(drawChart); // 一般的寫法
    google.charts.setOnLoadCallback(pageScrollHandler); // 為了要配合 Scroll 監測的啟動方式
  }

  /* START After Loaded */
  window.addEventListener('load', chartInit);
  /* ---------------------------------------- [END] Start Chart */

  /* ---------------------------------------- [START] Resize Chart */
  // Ref: https://stackoverflow.com/a/20384135/11240898
  /* Create the event */
  var event = new CustomEvent('resizeEnd');

  /* Create trigger to resizeEnd event */
  window.addEventListener('resize', function () {
    if (this.resizeTimer) {
      clearTimeout(this.resizeTimer);
    }
    this.resizeTimer = setTimeout(function () {
      /* Dispatch/Trigger/Fire the event */
      window.dispatchEvent(event);
    }, 100);
  });

  /* Add an event listener */
  /* Redraw graph when window resize is completed   */
  var ww = window.innerWidth;
  window.addEventListener('resizeEnd', function (e) {
    if (ww !== window.innerWidth) {
      ww = window.innerWidth; // 寬度有變化才重新Render
      redrawChart();
    }
  });
  /* ---------------------------------------- [END] Resize Chart */
})(window, document);