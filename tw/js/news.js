(function (window, document) {
  // 隱藏手機板的麵包屑最後一項(顯示新聞標題)
  document.body.classList.add('news-cont');

  /* ---------------------------------------- [START] Share to SNS */
  var anchors = document.querySelectorAll('.sharer');
  [].forEach.call(anchors, function (anchor) {
    anchor.addEventListener(
    'click',
    function (e) {
      e.preventDefault();

      // Get title
      var articleTitle = document.querySelector('#article-title') ?
      document.querySelector('#article-title').innerText :
      document.title;

      // change title to encode
      articleTitle = encodeURI(articleTitle);

      // grab the base sharing URL
      var url = e.target.href;

      // Change "{articleUrl}" to page url
      var shareUrl = url.
      replace(/{articleUrl}/gm, window.location.href).
      replace(/{articleTitle}/gm, articleTitle);

      // grab the name we want to give the window (parameter 2 on window.open)
      var windowName = e.target.getAttribute('data-windowName');

      // Grab parameter 3 for window.oprn (window settings)
      var windowSettings =
      e.target.getAttribute('data-settings') ||
      'menubar=no,toolbar=no,resizable=no,scrollbars=yes,height=700,width=550';

      // call window.open
      var otherWindow = window.open(shareUrl, windowName, windowSettings);
      otherWindow.opener = null;
      // otherWindow.location = shareUrl;
    },
    false);

  });
  /* ---------------------------------------- [END] Share to SNS */

  /* ---------------------------------------- [START] Navigator Share */
  /* 條件：一定要是HTTPS環境下才可以使用
   * Ref: https://w3c.github.io/web-share/
   */
  var shareButton = document.querySelectorAll('.js-navigator-share');
  var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  [].forEach.call(shareButton, function (el) {
    if (!isMobile || !navigator.share) {
      // 將非 Mobile 裝置、不支援 navigator share 擋下
      // el.classList.add('no-share');
      el.classList.add('hide'); // Hide Class
      el.tabIndex = -1;
    } else {
      el.classList.remove('hide');
    }

    el.addEventListener('click', function (event) {
      if (navigator.share) {
        navigator.
        share({
          title: document.title,
          url: location.href })["catch"](

        console.error);
      }
    });
  });
  /* ---------------------------------------- [END] Navigator Share */

  /* ---------------------------------------- [START] Copy link */
  /* clipboard.js — Copy to clipboard without Flash
   * https://clipboardjs.com/
   */

  var successMsg;
  var clipTimer = null;

  /* 複製成功訊息 */
  function addCopySuccess(text) {
    if (successMsg === undefined) {
      successMsg = document.createElement('div');
      successMsg.className = 'copysuctip';
      // successMsg.innerText = '複製成功';
      successMsg.setAttribute('aria-live', 'assertive');
    }
    successMsg.innerText = text || '複製成功';
    document.body.appendChild(successMsg);

    // Remove
    clearTimeout(clipTimer);
    clipTimer = null;
    clipTimer = setTimeout(function () {
      successMsg.remove();
      clipTimer = null;
    }, 3000);
  }

  function buildClipboardJS() {
    if (window.ClipboardJS !== undefined) {
      var clipboard = new ClipboardJS('.js-copy-link', {
        text: function text(trigger) {
          var text = window.location.href;
          return text;
        } });


      clipboard.on('success', function (e) {
        e.clearSelection();
        addCopySuccess(e.trigger.getAttribute('data-success'));
        e.trigger.blur();
      });
    }
  }
  window.addEventListener('load', buildClipboardJS);
  /* ---------------------------------------- [END] Copy link */

  /* ---------------------------------------- [START] Image Gallery */
  // Doc: https://photoswipe.com/documentation/getting-started.html
  var items = []; // Item array
  function openPhotoSwipe(itemIndex, selector) {
    var pswpElement = document.querySelectorAll('.pswp')[0];

    // 讀取圖片資料
    if (items.length === 0 && selector !== undefined) {
      var itemsEl = selector ? document.querySelectorAll(selector) : [];
      if (itemsEl.length === 0) {
        return false;
      }
      [].forEach.call(itemsEl, function (item, index) {
        var src = item.getAttribute('src');
        if (src) {
          items.push({
            el: item,
            src: src,
            w: 0,
            h: 0 });

        }
      });
    }

    var options = {
      history: false,
      focus: false,
      // showAnimationDuration: 0,
      // hideAnimationDuration: 0,
      index: itemIndex || 0, // start at first slide
      getThumbBoundsFn: function getThumbBoundsFn(itemIndex) {
        var thumbnail = items[itemIndex].el;
        var pageYScroll = window.pageYOffset || document.documentElement.scrollTop;
        var rect = thumbnail.getBoundingClientRect();
        return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
      } };


    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

    // Get Unknow Size
    // https://stackoverflow.com/a/40525732/11240898
    gallery.listen('gettingData', function (index, item) {
      if (item.w < 1 || item.h < 1) {
        // unknown size
        var img = new Image();
        img.onload = function () {
          // will get size after load
          item.w = this.width; // set image width
          item.h = this.height; // set image height
          gallery.invalidateCurrItems(); // reinit Items
          gallery.updateSize(true); // reinit Items
        };
        img.src = item.src; // let's download image
      }
    });

    gallery.init();
  }
  window.addEventListener('load', function () {
    var selector = '#article-container img';
    var galleryImg = document.querySelectorAll(selector);
    [].forEach.call(galleryImg, function (item, index) {
      item.index = index;
      item.setAttribute('draggable', false);
      item.addEventListener('click', function () {
        openPhotoSwipe(item.index, selector);
      });
    });
    // openPhotoSwipe(0, selector);
  });
  /* ---------------------------------------- [END] Image Gallery */
})(window, document);