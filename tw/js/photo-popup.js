(function (window, document) {
  /* ---------------------------------------- [START] Image Gallery */
  // Doc: https://photoswipe.com/documentation/getting-started.html
  /* 圖片點擊放大功能 */

  /* 注意事項：
   * 「非上稿」使用，上稿頁面改使用「article.min.js」檔案
   */

  /* 使用方式：
   * 1. 確認該頁／該單元的 SCSS 有使用到，兩個 SCSS 檔：
  	（注意兩個 SCSS 無生成出實際CSS，無法直接在頁面中引入）
  	@import 'vendor/photoswipe/photoswipe';
  	@import 'vendor/photoswipe/default-skin';
  	 * 2. 在頁面中引入 JS （包含此檔案）：
  	script(src="js/vendor/photoswipe-with-ui.min.js" async)
  	script(src="js/photo-popup.min.js" defer)
  	※已合併「js/vendor/photoswipe.min.js」、「js/vendor/photoswipe-ui-default.min.js」為「photoswipe-with-ui.min.js」
  	
   * 3. 在頁面中加入相關的 HTML，PUG 使用：
  	include _modal_phpto_popup
  	 * 4. 在要使用的 <img> 增加 class 「.js-photo-popup」
   * 可參考頁面：investor-company.pug
   */



  // var items = []; // Item array
  var photoGroups = {}; // 紀錄 Group
  var pswpElement = document.querySelectorAll('.pswp')[0];

  function getGroupValue(el, groupName) {
    if (photoGroups[groupName]) {
      return { data: photoGroups[groupName], index: el.index };
    } else {
      var items = document.querySelectorAll("[data-group=\"".concat(groupName, "\"]"));
      var itemData = [];
      var index = 0;

      [].forEach.call(items, function (item, i) {
        var value = {
          el: item,
          src: item.getAttribute('src'),
          w: 0,
          h: 0,
          index: i };


        item.index = i;

        if (item === el) {
          index = i;
        }

        itemData.push(value);
      });
      photoGroups[groupName] = itemData;

      return { data: itemData, index: index };
    }
  }

  function openPhotoSwipe(el) {
    if (el === undefined || el === null) {
      return false;
    }

    // 讀取圖片資料
    var items = [];
    var index = 0;
    if (el.dataset.group !== undefined) {
      var groupValue = getGroupValue(el, el.dataset.group);
      items = groupValue.data;
      index = groupValue.index;
    } else {
      items = [
      {
        el: el,
        src: el.getAttribute('src'),
        w: 0,
        h: 0 }];


    }

    var options = {
      history: false,
      focus: false,
      // showAnimationDuration: 0,
      // hideAnimationDuration: 0,
      index: index,
      getThumbBoundsFn: function getThumbBoundsFn(itemIndex) {
        var thumbnail = items[itemIndex].el;
        var pageYScroll = window.pageYOffset || document.documentElement.scrollTop;
        var rect = thumbnail.getBoundingClientRect();
        return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
      } };


    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

    // Get Unknow Size
    // https://stackoverflow.com/a/40525732/11240898
    gallery.listen('gettingData', function (index, item) {
      if (item.w < 1 || item.h < 1) {
        var img = new Image();
        img.onload = function () {
          // will get size after load
          item.w = this.width; // set image width
          item.h = this.height; // set image height
          gallery.invalidateCurrItems(); // reinit Items
          gallery.updateSize(true); // reinit Items
        };
        img.src = item.src; // let's download image
      }
    });

    gallery.init();
  }
  window.addEventListener('load', function () {
    var selector = 'img.js-photo-popup';
    var galleryImg = document.querySelectorAll(selector);

    // 產生提示文字
    var tipText = getTipText();
    var tipStr = "<p class=\"text-tip text-left hide-for-large\"><i class=\"be-icon be-icon-search\" aria-hidden=\"true\"></i>".concat(tipText, "</p>");
    var tip = stringToHTML(tipStr);

    [].forEach.call(galleryImg, function (item, index) {
      item.setAttribute('draggable', false);
      item.addEventListener('click', function () {
        openPhotoSwipe(item);
      });

      // Add Tip
      var clone = tip.cloneNode(true);
      insertAfter(clone, item);
    });
  });

  function getTipText() {
    var lang = document.documentElement.getAttribute('lang');
    switch (lang) {
      case 'zh-Hant-TW':
        return '點擊放大圖片';
      case 'zh-Hans':
        return '点击放大图片';
      default:
        return 'Click to enlarge the picture.';}

  }
  /* ---------------------------------------- [END] Image Gallery */

  /* ---------------------------------------- [START] Tool */
  var stringToHTML = function stringToHTML(str) {
    var dom = document.createElement('div');
    dom.innerHTML = str;
    return dom.firstChild;
  };

  // https://stackoverflow.com/a/4793630/11240898
  function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
  }
  /* ---------------------------------------- [END] Tool */
})(window, document);