(function (window, document) {
  // 隱藏手機板的麵包屑最後一項(顯示新聞標題)
  document.body.classList.add('inv-com-board');

  /* ---------------------------------------- [START] Toogle Panel(jQ) */
  var borderMainCont = $('#board-main-cont');
  var contHasToggleEl = borderMainCont.find('.has-toggle');
  var activeClass = 'is-active';
  contHasToggleEl.each(function (index, item) {
    var currentCont = $(item);
    var currentBtn = currentCont.find('.board-block__subtitle');
    var currentPanel = currentCont.find('.board-block__panel');
    var indexAdjust = index + 1;

    // 設定初始狀態
    currentBtn.attr({
      id: "board-item-".concat(indexAdjust, "-label"),
      role: 'tab',
      tabindex: 0,
      'aria-controls': "board-item-".concat(indexAdjust),
      'aria-expanded': false,
      'aria-selected': false });


    currentPanel.attr({
      id: "board-item-".concat(indexAdjust),
      role: 'tabpanel',
      'aria-labelledby': "board-item-".concat(indexAdjust, "-label"),
      'aria-hidden': true });


    currentBtn.on('click', function () {
      var $this = $(this);
      var $parent = $this.parent();
      var $panel = $this.next();
      $parent.toggleClass(activeClass);
      $panel.slideToggle(400, function () {
        // 更改 Foundation Component Magellan 紀錄的位置
        $('[data-magellan]').foundation('calcPoints');
      });

      // 更改狀態
      if ($this.parent().hasClass(activeClass)) {
        // open
        currentBtn.attr({
          'aria-expanded': true,
          'aria-selected': true });

        currentPanel.attr({
          'aria-hidden': false });

      } else {
        // close
        currentBtn.attr({
          'aria-expanded': false,
          'aria-selected': false });

        currentPanel.attr({
          'aria-hidden': true });

      }
    });
  });
  /* ---------------------------------------- [END] Toogle Panel(jQ) */
})(window, document);

/* ---------------------------------------- [START] Dropdown */
(function (window, document) {
  /* ---------------------------------------- [START] Windows Setting */
  var html = document.documentElement;
  // const body = document.body || document.querySelector('body');
  var ww = window.innerWidth;
  var wh = window.innerHeight;
  var ws = 0;
  function getScrollTop() {var target = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : window;
    return (target.pageYOffset || html.scrollTop) - (html.clientTop || 0);
  }
  function getWinSet() {
    ww = window.innerWidth;
    wh = window.innerHeight;
    ws = getScrollTop();
  }
  on(window, 'load', getWinSet);
  on(window, 'resize', throttle(getWinSet, 50, 100));
  /* ---------------------------------------- [END] Windows Setting */

  /* ---------------------------------------- [START] 取得裝置判斷 */
  // 取得裝置判斷
  var isMobile = false;
  // var isTablet = false;
  // var isPhone = false;
  var deviceDetect = function deviceDetect() {
    // IsPhone
    // isPhone = ww <= 640;

    // IsMobile
    // 防止測試時一直用開發者工具Resize出現Bug
    isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    // if (isMobile) html.classList.add('is-mobile');
    // else html.classList.remove('is-mobile');

    // IsTablet
    // if (navigator.userAgent.match(/Android/i)) {
    // 	if (!navigator.userAgent.match(/Mobile/i)) {
    // 		isTablet = true;
    // 	}
    // } else if (navigator.userAgent.match(/BlackBerry|iPad|Opera Mini|IEMobile/i)) {
    // 	isTablet = true;
    // }
  };
  deviceDetect();
  window.addEventListener('resize', throttle(deviceDetect, 50, 100));
  /* ---------------------------------------- [END] 取得裝置判斷 */

  var dropdownMenuSelector = '.js-dropdown-menu';
  var dropdownMenu = $(dropdownMenuSelector);
  var dropdownSelect = dropdownMenu.find('.select');
  var dropdownSelectDefualtText = dropdownSelect.text();
  var dropdownList = dropdownMenu.find('.list');
  var dropdownParent = dropdownMenu.parent();
  var dropdownOpenClass = 'is-open';

  var fdStickyEnable = true; // Foundation
  var fdSticky;

  var moSticky;

  var breakpoint = window.matchMedia('(min-width: 1024px)');
  var breakpointChecker = function breakpointChecker() {
    if (breakpoint.matches) {
      if (moSticky) {
        moSticky.destroy();
        dropdownSelect.off('click', dropdownSelectClick);
        dropdownList.off('click', 'a', dropdownListClick);
        moSticky = null;
      }
      // Large
      if (!fdStickyEnable) {
        fdSticky = new Foundation.Sticky(dropdownMenu);
      }
    } else {
      fdStickyEnable = false;
      dropdownMenu.foundation('_destroy'); // 破壞 Foundatino Sticky
      new Foundation.Magellan(dropdownList, { offset: 40 }); // 不明原因也被破壞掉了

      moSticky = fixedMenuMobile();
      dropdownSelect.on('click', dropdownSelectClick);
      dropdownList.on('click', 'a', dropdownListClick);
    }
  };
  // Start
  window.addEventListener('load', function () {
    breakpointChecker();
    breakpoint.addListener(breakpointChecker);

    // 使用 Foundation 套件 Event 更換 Select 文字
    $(document).on('update.zf.magellan', dropdownList, function (event, active) {
      if (active.length) {
        dropdownSelect.text(active.text()); // Select Text
      } else {
        dropdownSelect.text(dropdownSelectDefualtText);
      }
    });
  });

  // 智慧型裝置 sticky
  function fixedMenuMobile() {
    var fixedMenuEl = dropdownParent;
    if (!fixedMenuEl.length) {
      return false;
    }

    var fixedMenuOffsetTop = fixedMenuEl.offset().top;
    var headerHeight = document.querySelector('#header').clientHeight;
    var fixedMenuClass = 'is-sticky';

    function fixedMenuScroll() {
      ws = getScrollTop();
      if (ws + headerHeight > fixedMenuOffsetTop) {
        fixedMenuEl[0].classList.add(fixedMenuClass);
      } else {
        fixedMenuEl[0].classList.remove(fixedMenuClass);
      }
    }

    function fixedMenuResize() {
      fixedMenuOffsetTop = fixedMenuEl.offset().top;
    }

    window.addEventListener('scroll', fixedMenuScroll);
    window.addEventListener('resize', fixedMenuResize);
    fixedMenuScroll();

    var obj = {};

    obj.destroy = function () {
      window.removeEventListener('scroll', fixedMenuScroll);
      window.removeEventListener('resize', fixedMenuResize);
    };

    return obj;
  }

  // Dropdown Select
  function dropdownSelectClick() {
    this.classList.toggle(dropdownOpenClass);
    docClickDetected();
  }

  // Dropdown list
  function dropdownListClick() {
    var item = $(this);
    item.blur();

    // Close Select
    dropdownClose();
  }

  // Add Listener
  function docClickDetected() {
    if (dropdownSelect.hasClass(dropdownOpenClass)) {
      if (isMobile) {
        // Mobile
        setTimeout(function () {
          document.addEventListener('touchend', docClickHandaler, true);
        }, 500);
      } else {
        // PC
        // Click
        setTimeout(function () {
          document.addEventListener('click', docClickHandaler, true);
        }, 500);
      }
    }
  }

  function docClickHandaler(event) {
    var isList = $(event.target).parents(dropdownMenuSelector).length > 0;
    if (!isList) {
      event.preventDefault();
      dropdownClose();
    }
  }

  function dropdownClose() {
    dropdownSelect.removeClass(dropdownOpenClass);
    dropdownSelect.blur();
    dropdownList.find('a').blur();

    // 停止EventLitener
    document.removeEventListener('touchend', docClickHandaler, true);
    document.removeEventListener('click', docClickHandaler, true);
  }
})(window, document);
/* ---------------------------------------- [END] Dropdown */