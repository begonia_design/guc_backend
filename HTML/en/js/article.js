(function (window, document) {
  // 隱藏手機板的麵包屑最後一項(顯示新聞標題)
  document.body.classList.add('news-cont');

  /* ---------------------------------------- [START] Share to SNS */
  var anchors = document.querySelectorAll('.sharer');
  [].forEach.call(anchors, function (anchor) {
    anchor.addEventListener(
    'click',
    function (e) {
      e.preventDefault();

      // Get title
      var articleTitle = document.querySelector('#article-title') ?
      document.querySelector('#article-title').innerText :
      document.title;

      // change title to encode
      articleTitle = encodeURI(articleTitle);

      // grab the base sharing URL
      var url = e.target.href;

      // Change "{articleUrl}" to page url
      var shareUrl = url.
      replace(/{articleUrl}/gm, window.location.href).
      replace(/{articleTitle}/gm, articleTitle);

      // grab the name we want to give the window (parameter 2 on window.open)
      var windowName = e.target.getAttribute('data-windowName');

      // Grab parameter 3 for window.oprn (window settings)
      var windowSettings =
      e.target.getAttribute('data-settings') ||
      'menubar=no,toolbar=no,resizable=no,scrollbars=yes,height=700,width=550';

      // call window.open
      var otherWindow = window.open(shareUrl, windowName, windowSettings);
      otherWindow.opener = null;
      // otherWindow.location = shareUrl;
    },
    false);

  });
  /* ---------------------------------------- [END] Share to SNS */

  /* ---------------------------------------- [START] Navigator Share */
  /* 條件：一定要是HTTPS環境下才可以使用
   * Ref: https://w3c.github.io/web-share/
   */
  var shareButton = document.querySelectorAll('.js-navigator-share');
  var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  [].forEach.call(shareButton, function (el) {
    if (!isMobile || !navigator.share) {
      // 將非 Mobile 裝置、不支援 navigator share 擋下
      // el.classList.add('no-share');
      el.classList.add('hide'); // Hide Class
      el.tabIndex = -1;
    } else {
      el.classList.remove('hide');
    }

    el.addEventListener('click', function (event) {
      if (navigator.share) {
        navigator.
        share({
          title: document.title,
          url: location.href })["catch"](

        console.error);
      }
    });
  });
  /* ---------------------------------------- [END] Navigator Share */

  /* ---------------------------------------- [START] Copy link */
  /* clipboard.js — Copy to clipboard without Flash
   * https://clipboardjs.com/
   */

  var successMsg;
  var clipTimer = null;

  /* 複製成功訊息 */
  function addCopySuccess(text) {
    if (successMsg === undefined) {
      successMsg = document.createElement('div');
      successMsg.className = 'copysuctip';
      // successMsg.innerText = '複製成功';
      successMsg.setAttribute('aria-live', 'assertive');
    }
    successMsg.innerText = text || '複製成功';
    document.body.appendChild(successMsg);

    // Remove
    clearTimeout(clipTimer);
    clipTimer = null;
    clipTimer = setTimeout(function () {
      successMsg.remove();
      clipTimer = null;
    }, 3000);
  }

  function buildClipboardJS() {
    if (window.ClipboardJS !== undefined) {
      var clipboard = new ClipboardJS('.js-copy-link', {
        text: function text(trigger) {
          var text = window.location.href;
          return text;
        } });


      clipboard.on('success', function (e) {
        e.clearSelection();
        addCopySuccess(e.trigger.getAttribute('data-success'));
        e.trigger.blur();
      });
    }
  }
  window.addEventListener('load', buildClipboardJS);
  /* ---------------------------------------- [END] Copy link */

  /* ---------------------------------------- [START] Table Layout */
  /* 將上稿區內的表格改為：
   * 視第一橫列為標題，將標題生成於相對的子欄位
   * 在手機板會破壞表格的排版
   */
  function tableLayout() {
    var tables = document.querySelectorAll('#article-cont table');

    if (!tables.length) {
      return false;
    }

    // ======================================== [START] Wrap & Scroll
    var scrollArea; // 紀錄 ScrollArea
    var tableWrap = function tableWrap() {
      [].forEach.call(tables, function (table) {
        var wrapper = document.createElement('div');
        wrapper.className = 'table-wrapper is-scroll-start';

        var scrollArea = document.createElement('div');
        scrollArea.className = 'table-wrapper__scroll-area';

        scrollArea.addEventListener('scroll', scrollAreaDetected);
        wrap(table, scrollArea);
        wrap(scrollArea, wrapper);
      });

      // 讀取 ScrollArea 物件
      scrollArea = document.querySelectorAll('#article-cont .table-wrapper__scroll-area');

      // 監測 Resize
      scrollDetected();
      window.addEventListener('resize', throttle(scrollDetected, 50, 100));
    };
    tableWrap();

    // Resize All
    function scrollDetected() {
      [].forEach.call(scrollArea, function (item) {
        scrollAreaResize(item, item.parentElement);
      });
    }

    // Resize
    function scrollAreaResize(currentScrollArea, detectEl) {
      if (checkHasScroll(currentScrollArea)) {
        detectEl.classList.add('has-scroll');
      } else {
        detectEl.classList.remove('has-scroll');
      }
      scrollAreaDetected(currentScrollArea);
    }

    // Scroll 監測
    function scrollAreaDetected(target) {
      var self = target.tagName ? target : this;
      var targetWrapper = self.parentElement;

      // Scroll Start
      if (self.scrollLeft === 0) {
        targetWrapper.classList.add('is-scroll-start');
      } else {
        targetWrapper.classList.remove('is-scroll-start');
      }

      // Scroll End
      if (self.scrollLeft + self.clientWidth === self.scrollWidth) {
        targetWrapper.classList.add('is-scroll-end');
      } else {
        targetWrapper.classList.remove('is-scroll-end');
      }
    }

    // Tool
    // Ref: https://stackoverflow.com/a/18453767/11240898
    function wrap(toWrap, wrapper) {
      wrapper = wrapper || document.createElement('div');
      // 因為 toWrap 可能不是最後一個物件，建議將「appendChild」改為「insertBefore」方式（該篇有他人留言建議）
      toWrap.parentNode.insertBefore(wrapper, toWrap);
      return wrapper.appendChild(toWrap);
    }

    function checkHasScroll(el) {
      return el.scrollWidth > el.clientWidth;
    }
    // ======================================== [END] Wrap & Scroll

    // ======================================== [START] Width Calculate
    // 2021/10/15 UAT 49 - 因為客戶上稿區會有合併表格，造成排版不齊，所以決定移除
    // var tableDataCache = {}; // 紀錄資料

    // var paddingX = 1 / 12; // 左右 padding 寬為 1 grid
    // var contentWidth = 1 - paddingX * 2; // 扣除掉左右 padding 寬的內容寬

    // // Large: 讀取寬度比例 > 重新計算寬度
    // var tableCalcWidth = function () {
    // 	[].forEach.call(tables, function (table) {
    // 		var hasData = tableDataCache[table] !== undefined; // 是否有資料
    // 		var currentData = tableDataCache[table]; // 從快取撈取資料

    // 		var tds = hasData ? currentData.tds : table.querySelector('tr').querySelectorAll('td'); // 抓取第一個 tr 的 td
    // 		var tableWidth = hasData ? currentData.tableWidth : table.clientWidth; // 整個表格寬
    // 		var tdWidthArray = hasData ? currentData.tdWidth : [];

    // 		[].forEach.call(tds, function (td, index) {
    // 			var tdWidth;
    // 			if (hasData) {
    // 				tdWidth = tdWidthArray[index];
    // 			} else {
    // 				var tdWidthRate = td.clientWidth / tableWidth; // 計算各欄位寬度比例
    // 				var adjust = index === 0 || index === tds.length - 1 ? paddingX : 0; // 第一個跟最後一個要加上左右padding
    // 				tdWidth = (contentWidth * tdWidthRate + adjust) * 100 + '%';
    // 				tdWidthArray.push(tdWidth);
    // 			}
    // 			td.style.width = tdWidth;
    // 		});

    // 		// 紀錄資料
    // 		var data = {
    // 			tds: tds,
    // 			tableWidth: tableWidth,
    // 			tdWidth: tdWidthArray,
    // 		};
    // 		tableDataCache[table] = data;

    // 		// 最外層增加Class
    // 		table.classList.add('table-x-padding');
    // 	});
    // };

    // // Small + Medium: 移除寬度設定
    // var tableCalcWidthRemove = function () {
    // 	[].forEach.call(tables, function (table) {
    // 		var tds = table.querySelector('tr').querySelectorAll('td');
    // 		[].forEach.call(tds, function (item, index) {
    // 			item.style.width = '';
    // 		});
    // 	});
    // };

    // // Breakpoint 監測
    // var tableBreakpoint = window.matchMedia('(min-width: 1024px)');
    // var tableBreakpointChecker = function () {
    // 	if (tableBreakpoint.matches) {
    // 		// Large
    // 		tableCalcWidth();
    // 	} else {
    // 		// Small + Medium
    // 		tableCalcWidthRemove();
    // 	}
    // };

    // // Start
    // tableBreakpointChecker();
    // tableBreakpoint.addListener(tableBreakpointChecker);
    // ======================================== [END] Width Calculate
  }
  window.addEventListener('load', tableLayout);
  /* ---------------------------------------- [END] Table Layout */

  /* ---------------------------------------- [START] Image Gallery */
  // Doc: https://photoswipe.com/documentation/getting-started.html
  var items = []; // Item array
  function openPhotoSwipe(itemIndex, selector) {
    var pswpElement = document.querySelectorAll('.pswp')[0];

    // 讀取圖片資料
    if (items.length === 0 && selector !== undefined) {
      var itemsEl = selector ? document.querySelectorAll(selector) : [];
      if (itemsEl.length === 0) {
        return false;
      }
      [].forEach.call(itemsEl, function (item, index) {
        var src = item.getAttribute('src');
        if (src) {
          items.push({
            el: item,
            src: src,
            w: 0,
            h: 0 });

        }
      });
    }

    var options = {
      history: false,
      focus: false,
      // showAnimationDuration: 0,
      // hideAnimationDuration: 0,
      index: itemIndex || 0, // start at first slide
      getThumbBoundsFn: function getThumbBoundsFn(itemIndex) {
        var thumbnail = items[itemIndex].el;
        var pageYScroll = window.pageYOffset || document.documentElement.scrollTop;
        var rect = thumbnail.getBoundingClientRect();
        return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
      } };


    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);

    // Get Unknow Size
    // https://stackoverflow.com/a/40525732/11240898
    gallery.listen('gettingData', function (index, item) {
      if (item.w < 1 || item.h < 1) {
        // unknown size
        var img = new Image();
        img.onload = function () {
          // will get size after load
          item.w = this.width; // set image width
          item.h = this.height; // set image height
          gallery.invalidateCurrItems(); // reinit Items
          gallery.updateSize(true); // reinit Items
        };
        img.src = item.src; // let's download image
      }
    });

    gallery.init();
  }
  window.addEventListener('load', function () {
    var selector = '#article-cont img';
    var galleryImg = document.querySelectorAll(selector);

    var tipText = getTipText();
    var tipStr = "<p class=\"text-tip text-left hide-for-large\"><i class=\"be-icon be-icon-search\" aria-hidden=\"true\"></i>".concat(tipText, "</p>");
    var tip = stringToHTML(tipStr);
    [].forEach.call(galleryImg, function (item, index) {
      item.index = index;
      item.setAttribute('draggable', false);
      item.addEventListener('click', function () {
        openPhotoSwipe(item.index, selector);
      });

      // Add Tip
      var clone = tip.cloneNode(true);
      insertAfter(clone, item);
    });
  });

  function getTipText() {
    var lang = document.documentElement.getAttribute('lang');
    switch (lang) {
      case 'zh-Hant-TW':
        return '點擊放大圖片';
      case 'zh-Hans':
        return '點擊放大圖片';
      default:
        return 'Click to enlarge the picture.';}

  }
  /* ---------------------------------------- [END] Image Gallery */

  /* ---------------------------------------- [START] Tool */
  var stringToHTML = function stringToHTML(str) {
    var dom = document.createElement('div');
    dom.innerHTML = str;
    return dom.firstChild;
  };

  // https://stackoverflow.com/a/4793630/11240898
  function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
  }
  /* ---------------------------------------- [END] Tool */
})(window, document);