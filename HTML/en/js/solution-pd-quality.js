(function (window, document) {
  var pdQualityList = $('.pd-quality-list');
  var pdQualityListL = pdQualityList.find('.left .pd-quality-list__text-box');
  var pdQualityListR = pdQualityList.find('.right .pd-quality-list__text-box');

  function pdQuaMatchHeight() {
    pdQualityListL.each(function (index, item) {
      // Title
      var titleMax = 0;

      var title1 = $(item).find('.pd-quality-list__title');
      var title2 = pdQualityListR.eq(index).find('.pd-quality-list__title');

      title1.height('');
      title2.height('');

      titleMax = titleMax < title1.height() ? title1.height() : titleMax;
      titleMax = titleMax < title2.height() ? title2.height() : titleMax;

      title1.height(titleMax);
      title2.height(titleMax);

      // ======

      var contMax = 0;

      // Content
      var cont1 = $(item).find('.pd-quality-list__list');
      var cont2 = pdQualityListR.eq(index).find('.pd-quality-list__list');

      cont1.height('');
      cont2.height('');

      contMax = contMax < cont1.height() ? cont1.height() : contMax;
      contMax = contMax < cont2.height() ? cont2.height() : contMax;

      cont1.height(contMax);
      cont2.height(contMax);
    });
  }

  function pdQuaMatchHeightRemove() {
    pdQualityListL.each(function (index, item) {
      // Title
      $(item).find('.pd-quality-list__title').height('');
      pdQualityListR.eq(index).find('.pd-quality-list__title').height('');

      // ======

      // Content
      $(item).find('.pd-quality-list__list').height('');
      pdQualityListR.eq(index).find('.pd-quality-list__list').height('');
    });
  }

  var breakpoint = window.matchMedia('(min-width: 1024px)');
  function breakpointChecker() {
    if (breakpoint.matches) {
      pdQuaMatchHeight();
      window.addEventListener('resize', pdQuaMatchHeight);
    } else {
      window.removeEventListener('resize', pdQuaMatchHeight);
      pdQuaMatchHeightRemove();
    }
  }

  window.addEventListener('load', function () {
    breakpointChecker();
    breakpoint.addListener(breakpointChecker);
  });
})(window, document);