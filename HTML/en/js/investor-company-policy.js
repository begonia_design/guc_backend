(function (window, document) {
  /* ---------------------------------------- [START] Toogle Panel(jQ) */
  var borderMainCont = $('.sec-com-policy');
  var contHasToggleEl = borderMainCont.find('.has-toggle');
  var activeClass = 'is-active';
  contHasToggleEl.each(function (index, item) {
    var currentCont = $(item);
    var currentBtn = currentCont.find('.board-block__subtitle');
    var currentPanel = currentCont.find('.board-block__panel');
    var indexAdjust = index + 1;

    // 設定初始狀態
    currentBtn.attr({
      id: "board-item-".concat(indexAdjust, "-label"),
      role: 'tab',
      tabindex: 0,
      'aria-controls': "board-item-".concat(indexAdjust),
      'aria-expanded': false,
      'aria-selected': false });


    currentPanel.attr({
      id: "board-item-".concat(indexAdjust),
      role: 'tabpanel',
      'aria-labelledby': "board-item-".concat(indexAdjust, "-label"),
      'aria-hidden': true });


    currentBtn.on('click', function () {
      var $this = $(this);
      var $parent = $this.parent();
      var $panel = $this.next();
      $parent.toggleClass(activeClass);
      $panel.slideToggle(400, function () {
        // 更改 Foundation Component Magellan 紀錄的位置
        // $('[data-magellan]').foundation('calcPoints');
      });

      // 更改狀態
      if ($this.parent().hasClass(activeClass)) {
        // open
        currentBtn.attr({
          'aria-expanded': true,
          'aria-selected': true });

        currentPanel.attr({
          'aria-hidden': false });

      } else {
        // close
        currentBtn.attr({
          'aria-expanded': false,
          'aria-selected': false });

        currentPanel.attr({
          'aria-hidden': true });

      }
    });
  });
  /* ---------------------------------------- [END] Toogle Panel(jQ) */
})(window, document);