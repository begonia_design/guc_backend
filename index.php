<?php
$urlArr = explode('/', $_SERVER['REQUEST_URI']);
switch($urlArr[1]) {
    case 'zh-tw':
        header('HTTP/1.1 301 Moved Permanently');
        header("Location: /tw/");
        exit;
    case 'en-global':
        header('HTTP/1.1 301 Moved Permanently');
}

header("Location: /en/");
exit;
