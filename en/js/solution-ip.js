(function (window, document) {
  /* ---------------------------------------- [START] 取得裝置判斷 */
  // 取得裝置判斷
  var isMobile = false;
  // var isTablet = false;
  // var isPhone = false;
  var deviceDetect = function deviceDetect() {
    // IsPhone
    // isPhone = ww <= 640;

    // IsMobile
    // 防止測試時一直用開發者工具Resize出現Bug
    isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    // if (isMobile) html.classList.add('is-mobile');
    // else html.classList.remove('is-mobile');

    // IsTablet
    // if (navigator.userAgent.match(/Android/i)) {
    // 	if (!navigator.userAgent.match(/Mobile/i)) {
    // 		isTablet = true;
    // 	}
    // } else if (navigator.userAgent.match(/BlackBerry|iPad|Opera Mini|IEMobile/i)) {
    // 	isTablet = true;
    // }
  };
  deviceDetect();
  window.addEventListener('resize', throttle(deviceDetect, 50, 100));
  /* ---------------------------------------- [END] 取得裝置判斷 */

  /* ---------------------------------------- [START] 橫向選單：有ScrollBar時，設定操作按鈕 */
  /* 監測是否有 ScrollBar，增加控制 */
  var setScrollCtrl = function setScrollCtrl(selector) {
    // 抓取物件
    var detectEls = document.querySelectorAll(selector);

    [].forEach.call(detectEls, function (detectEl) {
      var scrollArea = document.getElementById(detectEl.dataset.scrollArea);
      var $scrollArea = $(scrollArea);
      var arrowAppendTarget = scrollArea.parentElement;

      var useArrow = detectEl.dataset.arrow ? detectEl.dataset.arrow !== 'false' : true;

      var hasScroll; // 確認選單是否有 Scroll Bar
      var arrowLeft = null;
      var arrowRight = null;

      if (useArrow) {
        // 向左箭頭
        arrowLeft = document.createElement('div');
        arrowLeft.className = 'btn-arrow left disabled';
        arrowLeft.setAttribute('role', 'button');
        arrowLeft.setAttribute('tabindex', '0');
        var arrowLeftIcon = document.createElement('i');
        arrowLeftIcon.className = 'be-icon be-icon-arrow-left';
        arrowLeft.appendChild(arrowLeftIcon);

        // 向右箭頭
        arrowRight = document.createElement('div');
        arrowRight.className = 'btn-arrow right';
        arrowRight.setAttribute('role', 'button');
        arrowRight.setAttribute('tabindex', '0');
        var arrowRightIcon = document.createElement('i');
        arrowRightIcon.className = 'be-icon be-icon-arrow-right';
        arrowRight.appendChild(arrowRightIcon);

        // 綁定事件：點擊
        arrowLeft.addEventListener('click', function () {
          scrollMethod($scrollArea, 'left');
        });
        arrowRight.addEventListener('click', function () {
          scrollMethod($scrollArea, 'right');
        });

        // 生成物件
        arrowAppendTarget.appendChild(arrowLeft);
        arrowAppendTarget.appendChild(arrowRight);
      }

      // [START] 滑鼠拖曳
      // Ref: https://codepen.io/thenutz/pen/VwYeYEE
      var mouseEnable = false;
      var isDragClass = 'is-drag';
      var isDown = false;
      var startX = 0;
      var scrollLeft = null;

      function mouseDown(e) {
        if (isMobile || !hasScroll) return;
        isDown = true;
        scrollArea.classList.add(isDragClass);
        startX = e.pageX - scrollArea.offsetLeft;
        scrollLeft = scrollArea.scrollLeft;
      }

      function mouseLeave() {
        if (isMobile || !hasScroll) return;
        isDown = false;
        scrollArea.classList.remove(isDragClass);
        document.activeElement.blur();
      }

      function mouseMove(e) {
        if (isMobile || !hasScroll || !isDown) return;
        e.preventDefault();
        var x = e.pageX - scrollArea.offsetLeft;
        var walk = (x - startX) * 3; // scroll-fast
        scrollArea.scrollLeft = scrollLeft - walk;
      }

      // 滑鼠事件建立(限電腦版、有滾軸的狀態)
      function mouseEventInit() {
        if (isMobile || !hasScroll || mouseEnable) return;
        mouseEnable = true;
        scrollArea.addEventListener('mousedown', mouseDown);
        scrollArea.addEventListener('mouseleave', mouseLeave);
        scrollArea.addEventListener('mouseup', mouseLeave);
        scrollArea.addEventListener('mousemove', mouseMove);
      }

      // 滑鼠事件破壞(限電腦版)
      function mouseEventDestroy() {
        if (isMobile || !mouseEnable) return;
        scrollArea.removeEventListener('mousedown', mouseDown);
        scrollArea.removeEventListener('mouseleave', mouseLeave);
        scrollArea.removeEventListener('mouseup', mouseLeave);
        scrollArea.removeEventListener('mousemove', mouseMove);
        mouseEnable = false;
      }
      // [END] 滑鼠拖曳

      // Resize
      function scrollCtrlResize() {
        hasScroll = checkHasScroll(scrollArea);

        if (hasScroll) {
          detectEl.classList.add('has-scroll');
          mouseEventInit(); // 滑鼠事件建立
        } else {
          detectEl.classList.remove('has-scroll');
          mouseEventDestroy(); // 滑鼠事件破壞
        }

        scrollDetected();
      }

      // 監測物件顯示/消失
      function scrollDetected() {
        var self = this.tagName ? this : scrollArea;

        // 沒有滾軸直接讓左右物件消失
        if (!hasScroll) {
          if (useArrow) {
            arrowLeft.classList.add('disabled');
            arrowRight.classList.add('disabled');
          }
          return false;
        }

        // Disabled - 向左箭頭
        if (self.scrollLeft === 0) {
          detectEl.classList.add('is-scroll-start');
          useArrow && arrowLeft.classList.add('disabled');
        } else {
          detectEl.classList.remove('is-scroll-start');
          useArrow && arrowLeft.classList.remove('disabled');
        }

        // Disabled - 向右箭頭
        if (self.scrollLeft + self.clientWidth === self.scrollWidth) {
          detectEl.classList.add('is-scroll-end');
          useArrow && arrowRight.classList.add('disabled');
        } else {
          detectEl.classList.remove('is-scroll-end');
          useArrow && arrowRight.classList.remove('disabled');
        }
      }

      scrollCtrlResize();
      window.addEventListener('resize', throttle(scrollCtrlResize, 50, 100));
      scrollArea.addEventListener('scroll', throttle(scrollDetected, 50, 100));
    });

    function checkHasScroll(el) {
      return el.scrollWidth > el.clientWidth;
    }

    function scrollMethod($el, dir) {
      var move = dir === 'left' ? '-=' + $el.get(0).clientWidth : '+=' + $el.get(0).clientWidth;
      $el.animate({ scrollLeft: move });
    }
  };

  // Init
  window.addEventListener('load', function () {
    setScrollCtrl('.js-scroll-detect');
  });
  /* ---------------------------------------- [END] 橫向選單：有ScrollBar時，設定操作按鈕 */
})(window, document);

(function () {
  function tableInit() {
    var tb = $('.ip-tb');
    var tbCopy = tb.clone();
    var tbScrollArea = $('.ip-tb-scroll-area');

    tbCopy.find('.ip-tb__row').each(function (index, item) {
      var cell = $(item).children('.ip-tb__cell');
      for (var i = 1; i < cell.length; i++) {
        cell.eq(i).remove();
      }
    });

    tbCopy.addClass('ip-tb--copy');

    tb.parents('.ip-tb-wrapper').append(tbCopy);

    tb.addClass('ip-tb--origin');

    tbScrollArea.scrollLeft(tb.innerWidth());
  }

  window.addEventListener('load', tableInit);
})();