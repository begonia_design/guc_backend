(function (window, document) {
  var asicLowPowerCell = $('.asic-low-power-magic .cell');
  var asicLowPowerTitle = [];

  asicLowPowerCell.each(function (index, item) {
    asicLowPowerTitle.push($(item).find('.hex-title-box__title').eq(0).get(0));
  });

  function matchHeight() {
    var maxHeight = 0;
    asicLowPowerTitle.forEach(function (item) {
      item.style.height = '';

      if (item.clientHeight > maxHeight) {
        maxHeight = item.clientHeight;
      }
    });

    asicLowPowerTitle.forEach(function (item) {
      item.style.height = maxHeight + 'px';
    });
  }

  function matchHeightRemove() {
    asicLowPowerTitle.forEach(function (item) {
      item.style.height = '';
    });
  }

  var breakpoint = window.matchMedia('(min-width: 1024px)');
  function breakpointChecker() {
    if (breakpoint.matches) {
      matchHeight();
      window.addEventListener('resize', matchHeight);
    } else {
      window.removeEventListener('resize', matchHeight);
      matchHeightRemove();
    }
  }

  window.addEventListener('load', function () {
    breakpointChecker();
    breakpoint.addListener(breakpointChecker);
  });

})(window, document);