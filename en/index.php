<?php
ini_set("display_errors", "On");
error_reporting(E_ALL & ~E_NOTICE);

date_default_timezone_set("Asia/Taipei");

define("SITE_DIR", dirname(dirname(__FILE__)));
include SITE_DIR . '/php/vendor/autoload.php';

use lib\Config;
use lib\Router;

Logger::configure(Config::LOG);
Router::router();

exit;