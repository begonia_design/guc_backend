(function (window, document) {
  /* ---------------------------------------- [START] 整頁 Scroll 監測 (Before) */
  // 為了要 Scroll 到該物件才生成，一方面為了初始效能，另一方面為了呈現動畫
  var pageScrollCheckList = []; // 紀錄物件
  var pageScrollAdd = function pageScrollAdd(selector, buildFunction) {
    var el = typeof selector === 'string' ? document.querySelector(selector) : selector;
    if (el === null) {
      return false;
    }
    pageScrollCheckList.push({
      build: false,
      el: el,
      fun: buildFunction });

  };
  var pageScrollClean = function pageScrollClean() {
    pageScrollCheckList = Array.prototype.filter.call(pageScrollCheckList, function (item) {
      return item.build === false;
    });
  };
  /* ---------------------------------------- [END] 整頁 Scroll 監測 (Before) */

  /* ---------------------------------------- [START] Chart Module */
  var chart = []; // 紀錄生成後的 Chart 物件(Google Chart)
  var chartIndex = 0; // 對 Chart 計數

  // 設定圖表選項
  var chartOptionGet = function chartOptionGet(data) {
    var isSmall = window.innerWidth < 640;

    return {
      width: window.innerWidth < 1024 ? '100%' : 710,
      height:
      window.innerWidth < 1024 ?
      window.innerWidth < 640 ?
      window.innerWidth - 80 :
      window.innerWidth - 140 :
      695,
      backgroundColor: { fill: 'transparent' },
      tooltip: {
        trigger: 'none' // Hover 不顯示資料
      },
      legend: 'none',
      // colors: [rgba('#009966', 0.5)],
      colors: ['#096'],
      pointSize: isSmall ? 4 : 16,
      dataOpacity: 0.8, // 點的透明度
      animation: {
        // 載入動畫
        startup: true,
        duration: 1000,
        easing: 'out' },

      chartArea: {
        backgroundColor: '#f5f5f5',
        top: 20,
        left: isSmall ? 60 : 120,
        bottom: isSmall ? 60 : 100,
        right: 20 },

      // 數據顯示的字
      annotations: {
        stemColor: 'none', // 隱藏指出哪個點的線段
        textStyle: {
          fontSize: isSmall ? 12 : 18,
          color: '#000' } },


      vAxis: {
        // Y軸（垂直）
        title: 'Interface Speed (Gbps)', // 標題
        titleTextStyle: {
          // 標題字樣式
          fontSize: isSmall ? 14 : 26, // 設定字級大小
          bold: true,
          italic: false },

        format: 'decimal',
        textStyle: {
          fontSize: isSmall ? 12 : 16 // 設定字級大小
        },
        viewWindow: {
          max: 120 },

        gridlines: {
          count: 7,
          color: '#82cab3' // 水平線段顏色
        },
        minorGridlines: {
          // 次要網格線
          color: 'transparent' },

        baselineColor: '#82cab3' // 0 的線段顏色
      },
      hAxis: {
        // x軸（水平）
        title: 'Chip Power Consumption (W)', // 標題
        titleTextStyle: {
          // 標題字樣式
          fontSize: isSmall ? 14 : 26, // 設定字級大小
          bold: true,
          italic: false },

        textStyle: {
          fontSize: isSmall ? 12 : 16 // 設定字級大小
        },
        viewWindow: {
          max: 800 },

        gridlines: {
          count: isSmall ? 5 : 2, // 手機板會無法顯示 2(最後一個數值)
          color: '#82cab3' // 垂直線段顏色
        },
        minorGridlines: {
          // 次要網格線(手機板會完全無視這部分)
          count: 5,
          color: 'transparent' },

        baselineColor: '#82cab3' // 0 的線段顏色
      } };

  };

  function drawChartInit(chartEl) {
    // 預防剛進入頁面時 google chart 尚未讀取完 => 延遲生成
    // 避免未讀取到： google.visualization
    if (!google.visualization || google.visualization && !google.visualization.DataTable) {
      setTimeout(function () {
        drawChartInit(chartEl);
      }, 1000);
      return false;
    }

    // 取得 DataTable HTML 物件（此頁用來擋下，二次讀取作用）
    var chartDataEl = chartEl.querySelector('.data-table');

    // 因為有時候 setOnLoadCallback 會 Call 兩次，以防萬一擋下來
    if (chartDataEl === null) {
      return false;
    }

    // Add Index Data
    chartEl.setAttribute('data-index', chartIndex);

    // [START] 建立 Data table
    var data = new google.visualization.DataTable();

    // 建立直欄資料
    data.addColumn('number', 'Interface Speed (Gbps)');
    data.addColumn('number', 'Chip Power Consumption (W)');
    data.addColumn({ type: 'string', role: 'annotation' });

    // // 建立橫列資料
    var dataTable = [
    [687, 112, 'SerDes 112G'], // 112G
    [244, 57, 'SerDes 56G'], // 56G
    [406, 26, 'SerDes 28G'], // 28G
    [85, 26, 'SerDes 28G'], // 28G
    [100, 26, null],
    [39, 26, null],
    [25, 26, null],
    [25, 16, null],
    [159, 15, null],
    [204, 17, null],
    [276, 15, null],
    [17, 10, null],
    [32, 9, null],
    [51, 5, null],
    [17, 7, null]];


    data.addRows(dataTable);
    // [END] 建立 Data table

    var option = chartOptionGet(data);

    // 建立object，以使用.draw方法畫出帶有data, options資料的圖表
    // 若要換成繪製圓餅圖改用google.visualization.PieChart，但data數據需要再調整
    var currentChart = new google.visualization.ScatterChart(chartEl);
    currentChart.draw(data, option);

    // =================================== Google Chart 以外的功能

    // 建立資料
    var chartData = {
      el: chartEl, // Chart 物件
      index: chartIndex, // Chart 序數
      dataEl: chartDataEl, // DataTable 物件
      data: data, // 只顯示 Bar的
      chart: currentChart, // Google Chart 生成後的資料
      showNum: false // Bar是否要顯示數字
    };

    chartEl.chart = chartData; // 記錄在物件上 el.chart
    chart.push(chartData); // 推上Array

    chartIndex += 1; // 增加 Index 序數
  }

  /* ---------------------------------------- [END] Chart Module */

  /* ---------------------------------------- [START] Draw Chart */
  // Init Chart
  function drawChart() {
    var chartTarget = document.querySelector('#pd-test-chart');
    pageScrollAdd(chartTarget, function () {
      drawChartInit(chartTarget);
    });
  }
  window.addEventListener('load', drawChart);

  // 重製 Chart
  function redrawChart() {
    // Ref: https://stackoverflow.com/a/11147816/11240898
    for (var i = 0; i < chart.length; i++) {
      var option = chartOptionGet(chart[i].data);
      chart[i].chart.draw(chart[i].showNum ? chart[i].view : chart[i].data, option);
    }
    // chart.draw(data, options);
  }
  /* ---------------------------------------- [END] Draw Chart */

  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */

  /* ---------------------------------------- [START] 整頁 Scroll 監測 (After) */
  var pageScrollThrottle = null;
  var pageScrollHandler = function pageScrollHandler() {
    if (pageScrollCheckList.length === 0) {
      return false;
    } // 因為改成 Page Global 執行，所以不會停止

    for (var i = 0; i < pageScrollCheckList.length; i++) {
      if (isInViewport(pageScrollCheckList[i].el)) {
        pageScrollCheckList[i].fun();
        pageScrollCheckList[i].build = true;
      }
    }
    pageScrollClean();
    if (pageScrollCheckList.length === 0) {
      window.removeEventListener('scroll', pageScrollThrottle);
    }
  };
  window.addEventListener('load', function () {
    pageScrollThrottle = throttle(pageScrollHandler, 50, 1000); // 節流作用
    window.addEventListener('scroll', pageScrollThrottle);
    pageScrollHandler();
  });
  /* ---------------------------------------- [END] 整頁 Scroll 監測 (After) */

  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* 因為需要變數 pageScrollHandler()，所以 Chart Start 要搬到下方 */

  /* ---------------------------------------- [START] Start Chart */
  /* START Chart */
  function chartInit() {
    var htmlLang = document.documentElement.getAttribute('lang');
    var language = htmlLang === 'zh-Hant-TW' ? 'zh-tw' : htmlLang.match(/zh-/g) ? 'zh-cn' : 'en';
    /* 載入Visualization API，括號裡第一個參數是版本名稱或數字 */
    google.charts.load('current', {
      packages: ['corechart'],
      language: language });

    // google.charts.setOnLoadCallback(drawChart); // 一般的寫法
    google.charts.setOnLoadCallback(pageScrollHandler); // 為了要配合 Scroll 監測的啟動方式
  }

  /* START After Loaded */
  window.addEventListener('load', chartInit);
  /* ---------------------------------------- [END] Start Chart */

  /* ---------------------------------------- [START] Resize Chart */
  // Ref: https://stackoverflow.com/a/20384135/11240898
  /* Create the event */
  var event = new CustomEvent('resizeEnd');

  /* Create trigger to resizeEnd event */
  window.addEventListener('resize', function () {
    if (this.resizeTimer) {
      clearTimeout(this.resizeTimer);
    }
    this.resizeTimer = setTimeout(function () {
      /* Dispatch/Trigger/Fire the event */
      window.dispatchEvent(event);
    }, 100);
  });

  /* Add an event listener */
  /* Redraw graph when window resize is completed  */
  var ww = window.innerWidth;
  window.addEventListener('resizeEnd', function (e) {
    if (ww !== window.innerWidth) {
      ww = window.innerWidth; // 寬度有變化才重新Render
      redrawChart();
    }
  });
  /* ---------------------------------------- [END] Resize Chart */
})(window, document);