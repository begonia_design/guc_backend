/* ---------------------------------------- [START] 輸入 Filter */
(function (window, document) {
  /* 只能輸入數字 */
  // setInputFilter(document.querySelectorAll('.js-input-num'), function (value) {
  // 	return /^\d*$/.test(value);
  // });

  /* 只能輸入數字 + 英文 */
  // setInputFilter(document.querySelectorAll('.js-input-num-a-z'), function (value) {
  // 	return /^[a-zA-Z0-9]*$/.test(value);
  // });

  /* 只能輸入電話相關 */
  setInputFilter(document.querySelectorAll('.js-input-tel'), function (value) {
    return /^[\d-+\s()#]*$/.test(value);
  });
})(window, document);

/* Ref (Richart): https://richart.tw/TSDIB_RichartWeb/static/revamp/js/InputFilter.js
 * Restricts input for the given textbox to the given inputFilter.
 */
function setInputFilter(x, inputFilter) {
  if (x.length > 0) {
    ['focus', 'input', 'keydown', 'keyup', 'mousedown', 'mouseup', 'select', 'contextmenu', 'drop'].forEach(
    function (event) {
      for (var i = 0; i < x.length; i++) {
        x[i].addEventListener(event, function () {
          if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
          } else if (this.hasOwnProperty('oldValue')) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
          } else {
            this.value = '';
          }
        });
      }
    });

  }
}
/* ---------------------------------------- [END] 輸入 Filter */

(function (window, document) {
  /* ---------------------------------------- [START] Windows Setting */
  var html = document.documentElement;
  var body = document.body || document.querySelector('body');
  var ww = window.innerWidth;
  var wh = window.innerHeight;
  var ws = 0;
  function getScrollTop() {var target = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : window;
    return (target.pageYOffset || html.scrollTop) - (html.clientTop || 0);
  }
  function getWinSet() {
    ww = window.innerWidth;
    wh = window.innerHeight;
    ws = getScrollTop();
  }
  function scrollHandler() {
    ws = getScrollTop();
  }
  window.addEventListener('load', getWinSet);
  window.addEventListener('scroll', scrollHandler);
  window.addEventListener('resize', throttle(getWinSet, 50, 100));
  /* ---------------------------------------- [END] Windows Setting */

  /* ---------------------------------------- [START] 寫入各語言的必填提醒文字 */
  // 如果要使用設計的錯誤樣式，得要避免使用HTML5預設Error，要將required改為aria-required
  // 造成瀏覽器不能顯示預設提醒文字
  var htmlLang = document.documentElement.getAttribute('lang') || 'en';
  var form = document.querySelector('form.contact-form');
  var fields = form.querySelectorAll('input[name], select[name], textarea[name]');
  var requireText = {
    en: {
      text: 'Please fill out this field.',
      select: 'Please select an item in the list.' },

    'zh-Hant-TW': {
      text: '請填寫此欄位。',
      select: '請在清單中選出一個項目。' },

    'zh-Hans': {
      text: '请填写此字段。',
      select: '请在列表中选择一项。' },

    jp: {
      text: 'このフィールドは必須です。',
      select: 'リスト内の項目選択してください。' },

    kr: {
      text: '필수 항목입니다.',
      select: '목록에서 항목 선택하십시오.' } };



  [].forEach.call(fields, function (item) {
    if (item.getAttribute('aria-required') === 'true') {
      item.setAttribute(
      'title',
      requireText[htmlLang][item.tagName.toUpperCase() === 'SELECT' ? 'select' : 'text']);

    }
  });
  /* ---------------------------------------- [END] 寫入各語言的必填提醒文字 */

  /* ---------------------------------------- [START] Select Filter (select2) */
  function buildSelectFilter() {
    $('.js-select-filter').each(function (index, item) {
      var $this = $(item);
      var searchInput;

      // 尋找對應的關鍵字
      // Ref: https://stackoverflow.com/a/58218967/11240898
      function matchCustom(params, data) {
        // If there are no search terms, return all of the data
        if ($.trim(params.term) === '') {
          return data;
        }

        // Do not display the item if there is no 'text' property
        if (typeof data.text === 'undefined') {
          return null;
        }

        // `params.term` should be the term that is used for searching
        // `data.text` is the text that is displayed for the data object
        if (data.text.toUpperCase().indexOf(params.term.toUpperCase()) > -1) {
          return data;
        }

        // custom search using lookup data
        // 尋找對應的關鍵字
        if (
        data.element.dataset.keywords &&
        data.element.dataset.keywords.toUpperCase().indexOf(params.term.toUpperCase()) > -1)
        {
          return data;
        }

        // Return `null` if the term should not be displayed
        return null;
      }

      var mySelect = $this.
      select2({
        matcher: matchCustom,
        placeholder: $this.data('placeholder'),
        allowClear: true,
        width: '100%',
        dropdownParent: $this.parent(),
        multiple: true, // 將多選強制設置為單選樣式
        maximumSelectionSize: 1 // 將多選強制設置為單選樣式
      }).
      on('select2:init', function (e) {
        // Init Event
        $this.removeAttr('multiple'); // Remove Multiple
        if (!$this.val()) {
          $this.parent().find('.select2-selection__clear').addClass('hide');
        }
      }).
      on('select2:select', function (e) {
        // 使用單選但又不要下拉呈現
        // Ref: https://stackoverflow.com/a/32575136/11240898
        $this.val([]);
        $this.val([e.params.data.id]).trigger('change');
        $(':focus').blur();
      }).
      on('change', function (e) {
        // 隱藏 Clear All
        // 選擇項目時，會 trigger 兩次 change ，原因不明
        if (!$this.val()) {
          $this.parent().find('.select2-selection__clear').addClass('hide');
        } else {
          $this.parent().find('.select2-selection__clear').removeClass('hide');
        }
      }).
      on('select2:unselect', function (e) {
        // 多選直接按 ←Backspace 時，會出現原項目文字編輯，非整個刪除
        // ↓達成整個刪除
        $this.val(null);
        setTimeout(function () {
          searchInput.val('');
          $this.trigger('change');
          searchInput.trigger('input'); // 重刷 Search
        }, 1);
      }).
      on('select2:clear', function () {
        setTimeout(function () {
          $this.parent().find('input').focus();
        }, 20);
      });

      // Select El
      // Trigger Init, Ref: https://github.com/select2/select2/issues/2572#issuecomment-166941793
      $this.trigger('select2:init');

      $this.on('focus', function () {
        $this.select2('open');
      });

      // ---
      // Input El
      searchInput = $this.parent().find('input');

      // Chrome 會忽略設置 off 的 autocomplete，改其它寫法
      // https://github.com/select2/select2/issues/3313#issuecomment-98642015
      searchInput.attr('autocomplete', 'xxxxxxxxxxx');
    });
  }

  window.addEventListener('load', buildSelectFilter);
  /* ---------------------------------------- [START] Select Filter (select2) */

  /* ---------------------------------------- [START] Textarea 計算 */
  /* Reference: https://codepen.io/amanda328/pen/aammmq/ */
  var textCountEl = $('.js-textarea-count');

  var countTextarea = function countTextarea() {
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

    if (textCountEl.length) {
      var countTextarea = textCountEl.find('textarea');
      var countBox = textCountEl.find('.text-counter');
      var countText = textCountEl.find('.num');
      var countStartMax = false; // 是否從最大值開始倒數

      // 取得 Max值
      var countMax = parseInt(countTextarea.attr('data-max')) || 1000;

      // 初始值
      countBox.addClass('is-hide');
      countText.text(countStartMax ? countMax : 0);

      // 計算字數
      // function countTextareaHandler(el){
      var countTextareaHandler = function countTextareaHandler(el) {
        var item = el.parentElement;

        // 移除Enter鍵計算 https://stackoverflow.com/a/10031113
        var thisValueLength = $(el).val().length;
        var countDown = countStartMax ? countMax - thisValueLength : thisValueLength;

        // 改字
        countText.text(countDown);

        // 改狀態
        if (countDown < 0) {
          item.dataset.status = 'error';
        } else if (countDown < 100) {
          item.dataset.status = 'warning';
        } else {
          delete item.dataset.status;
          if (thisValueLength === 0) {
            countBox.addClass('is-hide');
          } else {
            countBox.show();
          }
        }
      };
      countTextarea.on('keydown keyup keypress change', function () {
        countTextareaHandler(this);
      });

      // -----------------------------
      // Change Textarea Height
      // https://stackoverflow.com/a/5665555
      // 20181024 : 目前因為此計算文字區域高度，造成ios9判斷不良，問題顯示為函數寫法問題，「SyntaxError: Strict mode does not allow function declarations in a lexically nested statement.」
      // 有查到相關的資訊，https://www.programering.com/a/MTMwITMwATk.html
      var autoSize = function autoSize(ele) {
        ele.style.height = 'auto';
        var newHeight = ele.scrollHeight > 150 ? ele.scrollHeight + 16 : 150;
        ele.style.height = newHeight.toString() + 'px';
      };
      $('textarea').on('keyup keypress change', function (e) {
        autoSize(this);
      });
      if (isMobile) {
        // 貼上 (For Mobile)
        $('textarea').on('paste', function () {
          var item = this;
          setTimeout(function () {
            autoSize(item);
            countTextareaHandler(item);

            // Change Scroll 如果超過 Sreen
            var textareaOffsetBottom = countTextarea.offset().top + countTextarea.innerHeight();
            if (ws + wh < textareaOffsetBottom) {
              $(window).scrollTop(textareaOffsetBottom - wh + 40);
            }
          }, 1);
        });
      }

      // 按下重設
      // Form Before & After Reset
      // https://stackoverflow.com/a/10319514
      var resetBtn = $('button[type="reset"]');
      resetBtn.closest('form').on('reset', function (event) {
        // executes before the form has been reset
        var oldOffset = resetBtn.offset().top;
        document.querySelector('textarea').style.height = '';
        countBox.addClass('is-hide');

        // Change Scorll Top
        if (isMobile) {
          setTimeout(function () {
            // executes after the form has been reset
            var scrollTopChange = oldOffset - resetBtn.offset().top;
            // $(window).scrollTop( ws - scrollTopChange);
            $('html, body').animate({ scrollTop: ws - scrollTopChange }, 100);
          }, 10);
        }
      });
    }
  };
  window.addEventListener('load', countTextarea);
  /* ---------------------------------------- [END] Textarea 計算 */
})(window, document);