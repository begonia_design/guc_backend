(function (window, document) {
  /* ---------------------------------------- [START] 更改 Foundation Offset 方法 */
  // 更改 offset 方法： https://foundation.discourse.group/t/how-to-update-magellans-offset-dynamically/2830/3

  var fdElem = null; // 裝 Foundation 項目

  var breakpoint = window.matchMedia('(min-width: 1024px)');
  var breakpointChecker = function breakpointChecker() {
    if (breakpoint.matches) {
      // Large
      fdElem.options.offset = 80 + 60 + 20;
    } else {
      // Small + Medium
      fdElem.options.offset = 80;
    }
  };

  window.addEventListener('load', function () {
    // ReInit
    fdElem = new Foundation.Magellan($('[data-magellan]'));

    breakpointChecker();
    breakpoint.addListener(breakpointChecker);
  });
  /* ---------------------------------------- [END] 更改 Foundation Offset 方法 */

})(window, document);