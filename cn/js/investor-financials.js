(function (window, document) {
  /* ---------------------------------------- [START] 整頁 Scroll 監測 (Before) */
  // 為了要 Scroll 到該物件才生成，一方面為了初始效能，另一方面為了呈現動畫
  var pageScrollCheckList = []; // 紀錄物件
  var pageScrollAdd = function pageScrollAdd(selector, buildFunction) {
    var el = typeof selector === 'string' ? document.querySelector(selector) : selector;
    if (el === null) {
      return false;
    }
    pageScrollCheckList.push({
      build: false,
      el: el,
      fun: buildFunction });

  };
  var pageScrollClean = function pageScrollClean() {
    pageScrollCheckList = Array.prototype.filter.call(pageScrollCheckList, function (item) {
      return item.build === false;
    });
  };
  /* ---------------------------------------- [END] 整頁 Scroll 監測 (Before) */

  /* ---------------------------------------- [START] Chart Module */
  var chart = []; // 紀錄生成後的 Chart 物件(Google Chart)
  var chartIndex = 0; // 對 Chart 計數

  // 設定圖表選項
  var chartOptionGet = function chartOptionGet(data) {
    // 取得最大值
    var maxV = 0;
    for (var i = 0; i < data.getNumberOfRows(); i++) {
      if (data.getValue(i, 1) > maxV) {
        maxV = data.getValue(i, 1);
      }
    }

    var isSmall = window.innerWidth < 640;

    return {
      // width: chartEl.parentElement.innerWidth,
      width: '100%',
      height: window.innerWidth < 1024 ? 250 : 465,
      backgroundColor: { fill: 'transparent' },
      tooltip: { isHtml: true }, // CSS styling affects only HTML tooltips.
      legend: { position: 'none' }, // 移除圖例
      colors: ['#66b06b'],
      // bars: 'vertical',
      bar: { groupWidth: '80%' },
      animation: {
        // 載入動畫
        startup: true,
        duration: 1000,
        easing: 'out' },

      chartArea: {
        top: 10,
        left: 70,
        bottom: 40,
        right: isSmall ? 10 : 0 },

      vAxis: {
        // Y軸（垂直）
        format: 'decimal',
        textStyle: {
          fontSize: 13 // 設定字級大小
        },
        viewWindow: {
          max: isSmall ? maxV * 1.2 : maxV * 1.1 // maximum value of annotations
        } },

      hAxis: {
        // x軸（水平）
        // title: '', // 標題移除
        textStyle: {
          fontSize: isSmall ? 14 : 16 // 設定字級大小
        } },

      // Bar 上方顯示的字
      annotations: {
        style: 'point',
        alwaysOutside: true } };


  };

  function drawChartInit(chartEl) {
    // 預防剛進入頁面時 google chart 尚未讀取完 => 延遲生成
    // 避免未讀取到： google.visualization
    if (!google.visualization || google.visualization && !google.visualization.DataTable) {
      setTimeout(function () {
        drawChartInit(chartEl);
      }, 1000);
      return false;
    }

    // 取得 DataTable HTML 物件
    var chartDataEl = chartEl.querySelector('.data-table');

    // 因為有時候 setOnLoadCallback 會 Call 兩次，以防萬一擋下來
    if (chartDataEl === null) {
      return false;
    }

    // Add Index Data
    chartEl.setAttribute('data-index', chartIndex);

    // 建立 Data table
    var data = new google.visualization.DataTable();

    // 建立直欄資料
    var cols = chartDataEl.querySelectorAll('thead th');
    var rowType = []; // 紀錄資料類型
    for (var i = 0; i < cols.length; i++) {
      data.addColumn(cols[i].dataset.type, cols[i].innerText);
      rowType.push(cols[i].dataset.type);
    }
    // A column for custom tooltip content
    data.addColumn({ type: 'string', role: 'tooltip', p: { html: true } });
    var dataNameUnit = cols[1].innerText;

    // 建立橫列資料
    var dataTable = [];
    var rows = chartDataEl.querySelectorAll('tbody tr');
    for (var i = 0; i < rows.length; i++) {
      var value = [];
      var currentTd = rows[i].querySelectorAll('td');
      for (var j = 0; j < currentTd.length; j++) {
        value.push(rowType[j] === 'number' ? parseInt(currentTd[j].innerText) : currentTd[j].innerText);
      }
      value.push(createCustomHTMLContent(value[0], value[1], dataNameUnit));
      dataTable.push(value);
    }
    data.addRows(dataTable);

    /* Bar上方顯示數字的data版本
     * 使用：chart.draw 的時候將 data 改為 view
     * Ref: https://developers.google.com/chart/interactive/docs/gallery/columnchart#labeling-columns
     */
    var view = new google.visualization.DataView(data);
    view.setColumns([
    0,
    1,
    {
      calc: 'stringify',
      sourceColumn: 1,
      type: 'string',
      role: 'annotation' },

    2]);


    var option = chartOptionGet(data);

    // 建立object，以使用.draw方法畫出帶有data, options資料的圖表
    // 若要換成繪製圓餅圖改用google.visualization.PieChart，但data數據需要再調整
    var currentChart = new google.visualization.ColumnChart(chartEl);
    currentChart.draw(data, option);

    // =================================== Google Chart 以外的功能

    // 取得顯示的 y 軸最大值
    // var cli = currentChart.getChartLayoutInterface();
    // var bb = cli.getChartAreaBoundingBox();
    // var vAxisMax = cli.getVAxisValue(bb.top);
    // console.log('vAxisMax', vAxisMax);

    // 下載的檔案名稱
    var fileName = chartEl.dataset.download || '';
    fileName = "".concat(fileName, "_").concat(rows[0].querySelector('.year').innerText, "_").concat(
    rows[rows.length - 1].querySelector('.year').innerText, ".png");


    // 抓取標題敘述資料
    var parent = closestParent(chartEl, 'fs-chart');
    var title = parent.querySelector('.fs-chart__title').innerText;
    var desc = parent.querySelector('.fs-chart__desc').innerText;

    // 建立資料
    var chartData = {
      el: chartEl, // Chart 物件
      index: chartIndex, // Chart 序數
      dataEl: chartDataEl, // DataTable 物件
      colEl: cols, // 直欄(標頭)物件
      rowEl: rows, // 橫列物件(tr)
      data: data, // 只顯示 Bar的
      view: view, // 顯示數字的
      chart: currentChart, // Google Chart 生成後的資料
      showNum: false, // Bar是否要顯示數字
      download: fileName, // 下載的檔案名稱
      parent: parent,
      title: title,
      desc: desc };


    chartEl.chart = chartData; // 記錄在物件上 el.chart
    chart.push(chartData); // 推上Array

    // =================================== [START] EVENT
    // -------------------------- [START] Google Chart Event
    // Mouseover Event
    // https://developers.google.com/chart/interactive/docs/examples#mouseover-tooltip-example
    // Add our over/out handlers.
    google.visualization.events.addListener(chartEl.chart.chart, 'onmouseover', function (e) {
      chartEl.chart.chart.setSelection([e]);
    });
    google.visualization.events.addListener(chartEl.chart.chart, 'onmouseout', function () {
      chartEl.chart.chart.setSelection([{ row: null, column: null }]);
    });

    // Animation Finish
    // Bar Hover 後更改其餘 Bar 的顏色
    // 說明
    // 專案使用的 new google.visualization.ColumnChart 原本是沒有這個互動效果
    // 是 new google.charts.Bar 才有，但 google.charts.Bar ，無法使用出場動畫（目前找不到官方範例）
    // 為了要 google.visualization.ColumnChart 也可以有 Hover 後淡化其它 Bar
    // 要使用「暴力」改寫方式：找到 Bar 的外層<g>，增加 .bar-wrapper class
    // 在 animation 結束前，會生成不同的 DOM ，所以要等 animation 結束後再找
    google.visualization.events.addListener(chartEl.chart.chart, 'animationfinish', function (e) {
      var groups = chartEl.querySelector('g[clip-path]').querySelectorAll('g');
      // 找<g>的<rect>與資料數符合的<g> === bar 的外層
      for (var i = 0; i < groups.length; i++) {
        var group = groups[i];
        if (group.querySelectorAll('rect').length === chartEl.chart.rowEl.length) {
          group.classList.add('bar-wrapper');
          break;
        }
      }
    });
    // -------------------------- [END] Google Chart Event

    // -------------------------- [START] Show Number Toggle
    // Show Number Toggle
    var btnShowNum = chartEl.parentElement.querySelector('.js-chart-show-num');
    if (btnShowNum) {
      btnShowNum.setAttribute('data-index', chartIndex); // 增加對應的 Index 資料

      btnShowNum.addEventListener('click', function () {
        this.blur();

        // 抓取目前資料
        var currentIndex = this.dataset.index;
        var currentChartData = chart[currentIndex];

        // 更改文字
        if (currentChartData.showNum) {
          // 隱藏
          this.innerText = this.dataset.show;
        } else {
          // 顯示
          this.innerText = this.dataset.hide;
        }

        showNumToggle(currentChartData); // Toggle Number

      });
    }
    // -------------------------- [END] Show Number Toggle

    // -------------------------- [START] Download Img
    // Download Img
    // Ref: https://developers.google.com/chart/interactive/docs/printing
    var btnDownloadImg = chartEl.parentElement.querySelector('.js-chart-download-img');
    if (btnDownloadImg) {
      btnDownloadImg.setAttribute('data-index', chartIndex); // 增加對應的 Index 資料

      // 建立下載圖片
      var donwloadImg = function donwloadImg(dataImg) {
        var downloadLink = document.createElement('a');
        downloadLink.href = dataImg;
        downloadLink.target = '_blank';
        downloadLink.download = chartEl.chart.download || 'chart.png';

        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
      };

      btnDownloadImg.addEventListener('click', function () {
        this.blur();
        // window.open(currentChart.getImageURI());

        // 抓取目前資料
        var currentIndex = this.dataset.index;
        var currentChartData = chart[currentIndex];

        // 讀取圖片大小 → 用 Canvas 建立新圖片
        var img = new Image();
        img.onload = function () {
          var canvas = document.createElement('canvas');
          var ctx = canvas.getContext('2d');
          canvas.width = img.width + 40;
          canvas.height = img.height + 100;

          // Background
          ctx.fillStyle = '#fff';
          ctx.fillRect(0, 0, canvas.width, canvas.height);

          // Chart
          ctx.drawImage(img, 20, 100);

          // Title
          ctx.fillStyle = '#000';
          ctx.font = 'bold 26px "sans-serif"';
          ctx.fillText(currentChartData.title, 20, 20 + 19);

          // Description
          ctx.fillStyle = '#565656';
          ctx.font = '18px "sans-serif"';
          ctx.fillText(currentChartData.desc, 20, 60 + 14);

          // 生成下載圖片
          donwloadImg(canvas.toDataURL());
        };
        img.src = currentChart.getImageURI();
      });
    }
    // -------------------------- [END] Download Img
    // =================================== [END] EVENT

    chartIndex += 1; // 增加 Index 序數
  }

  function createCustomHTMLContent(year, number, dataNameUnit) {
    return "\n\t\t\t<div class=\"chart-tooltip\">\n\t\t\t\t<div class=\"chart-tooltip__title h3\">".concat(

    year, "</div>\n\t\t\t\t<div class=\"chart-tooltip__desc h4\">").concat(
    dataNameUnit, "</div>\n\t\t\t\t<div class=\"chart-tooltip__number h4 text-em\">").concat(
    numberWithCommas(number), "</div>\n\t\t\t</div>\n\t\t");


  }

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
  /* ---------------------------------------- [END] Chart Module */

  /* ---------------------------------------- [START] Draw Chart */
  // Init Chart
  function drawChart() {
    [].forEach.call(document.querySelectorAll('.fs-chart__chart'), function (item) {
      pageScrollAdd(item, function () {
        drawChartInit(item);
      });
    });
  }
  window.addEventListener('load', drawChart);

  // 重製 Chart
  function redrawChart() {
    // Ref: https://stackoverflow.com/a/11147816/11240898
    for (var i = 0; i < chart.length; i++) {
      var option = chartOptionGet(chart[i].data);
      chart[i].chart.draw(chart[i].showNum ? chart[i].view : chart[i].data, option);
    }
    // chart.draw(data, options);
  }

  // 顯示數字的 Function
  function showNumToggle(chartData) {
    var option = chartOptionGet(chartData.data);
    var showNum = !chartData.el.chart.showNum; // 是否顯示，true/false

    // 更新資料
    chartData.el.chart.showNum = showNum;
    chart[chartData.index].showNum = showNum;

    chartData.chart.draw(showNum ? chartData.view : chartData.data, option);
  }
  /* ---------------------------------------- [END] Draw Chart */

  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */

  /* ---------------------------------------- [START] Chart Tool */
  var toolBreakpoint = window.matchMedia('(min-width: 640px)');
  var toolDropdown;
  var toolEnable = true;
  var toolBreakpointChecker = function toolBreakpointChecker() {
    if (toolBreakpoint.matches) {
      // Medium, Large+: 破壞 Dropdown，直接觀看按鈕
      toolDropdown.foundation('_destroy');
      toolDropdown.css('display', '');
      toolEnable = false;
    } else if (!toolEnable) {
      toolEnable = true;
      toolDropdown.css('display', '');

      // Small
      toolDropdown.each(function (index, item) {
        var elem = new Foundation.Dropdown($(item));
      });
    }
  };
  toolBreakpoint.addListener(toolBreakpointChecker);
  window.addEventListener('load', function () {
    toolDropdown = $('.fs-chart .dropdown-pane');
    toolBreakpointChecker();

    toolDropdown.find('.button').on('click', function () {
      if (toolEnable) {
        $(this).parents('.dropdown-pane').foundation('close');
      }
    });
  });
  /* ---------------------------------------- [END] Chart Tool */

  /* ---------------------------------------- [START] Tool Function */
  // 尋找最接近的物件
  function closestParent(child, className) {
    if (!child || child === document) {
      return null;
    }
    if (child.classList.contains(className)) {
      return child;
    } else {
      return closestParent(child.parentNode, className);
    }
  }
  /* ---------------------------------------- [END] Tool Function */

  /* ---------------------------------------- [START] 整頁 Scroll 監測 (After) */
  var pageScrollThrottle = null;
  var pageScrollHandler = function pageScrollHandler() {
    if (pageScrollCheckList.length === 0) {
      return false;
    } // 因為改成 Page Global 執行，所以不會停止

    for (var i = 0; i < pageScrollCheckList.length; i++) {
      if (isInViewport(pageScrollCheckList[i].el)) {
        pageScrollCheckList[i].fun();
        pageScrollCheckList[i].build = true;
      }
    }
    pageScrollClean();
    if (pageScrollCheckList.length === 0) {
      window.removeEventListener('scroll', pageScrollThrottle);
    }
  };
  window.addEventListener('load', function () {
    pageScrollThrottle = throttle(pageScrollHandler, 50, 1000); // 節流作用
    window.addEventListener('scroll', pageScrollThrottle);
    pageScrollHandler();
  });
  /* ---------------------------------------- [END] 整頁 Scroll 監測 (After) */

  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* ---------------------------------------- */
  /* 因為需要變數 pageScrollHandler()，所以 Chart Start 要搬到下方 */

  /* ---------------------------------------- [START] Start Chart */
  /* START Chart */
  function chartInit() {
    var htmlLang = document.documentElement.getAttribute('lang');
    var language = htmlLang === 'zh-Hant-TW' ? 'zh-tw' : htmlLang.match(/zh-/g) ? 'zh-cn' : 'en';
    /* 載入Visualization API，括號裡第一個參數是版本名稱或數字 */
    google.charts.load('current', {
      packages: ['corechart'],
      language: language });

    // google.charts.setOnLoadCallback(drawChart); // 一般的寫法
    google.charts.setOnLoadCallback(pageScrollHandler); // 為了要配合 Scroll 監測的啟動方式
  }

  /* START After Loaded */
  window.addEventListener('load', chartInit);
  /* ---------------------------------------- [END] Start Chart */

  /* ---------------------------------------- [START] Resize Chart */
  // Ref: https://stackoverflow.com/a/20384135/11240898
  /* Create the event */
  var event = new CustomEvent('resizeEnd');

  /* Create trigger to resizeEnd event */
  window.addEventListener('resize', function () {
    if (this.resizeTimer) {
      clearTimeout(this.resizeTimer);
    }
    this.resizeTimer = setTimeout(function () {
      /* Dispatch/Trigger/Fire the event */
      window.dispatchEvent(event);
    }, 100);
  });

  /* Add an event listener */
  /* Redraw graph when window resize is completed   */
  var ww = window.innerWidth;
  window.addEventListener('resizeEnd', function (e) {
    if (ww !== window.innerWidth) {
      ww = window.innerWidth; // 寬度有變化才重新Render
      redrawChart();
    }
  });
  /* ---------------------------------------- [END] Resize Chart */
})(window, document);