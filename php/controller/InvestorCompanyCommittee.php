<?php

namespace controller;

use lib\Tools;
use model\IndexBannerDao;
use model\IndexContentDao;
use model\InvCorCommitteesDao;
use model\InvFinCalendarDao;
use model\InvFinancialsDao;
use model\InvFinHistoricalDao;
use model\InvFinMonthlyDao;
use model\InvFinMonthlyValueDao;
use model\InvFinNetDao;
use model\InvFinQuarterlyDao;
use model\InvFinRevenuesDao;
use model\Tab;


class InvestorCompanyCommittee extends Controller
{

    protected function run()
    {
        $this->assign('list', InvCorCommitteesDao::getAll());
    }
}