<?php

namespace controller;

use model\InvFaqDao;


class Faq extends Controller
{

    protected function run()
    {

        $this->assign('list', InvFaqDao::getList());
    }
}