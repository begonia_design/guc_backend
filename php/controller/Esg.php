<?php

namespace controller;

use model\EsgDao;
use model\EsgReportDao;


class Esg extends Controller
{
    protected function run()
    {
        $this->assign('list', EsgDao::getAllList());
        $this->assign('report', EsgReportDao::getFirst());
    }
}