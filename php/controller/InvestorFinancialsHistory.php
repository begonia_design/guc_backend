<?php

namespace controller;

use lib\Tools;
use model\IndexBannerDao;
use model\IndexContentDao;
use model\InvFinancialsDao;
use model\InvFinHistoricalDao;
use model\InvFinMonthlyDao;
use model\InvFinMonthlyValueDao;
use model\InvFinNetDao;
use model\InvFinQuarterlyDao;
use model\InvFinRevenuesDao;
use model\Tab;


class InvestorFinancialsHistory extends Controller
{

    protected function run()
    {
        $files = InvFinHistoricalDao::getAll();
        if ($files) {
            foreach($files as $k => $v) {
                $files[$k]['ext'] = Tools::getFileExt($v['file']);
            }
        }
        $this->assign('f', $files);
    }
}