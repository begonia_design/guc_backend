<?php

namespace controller;

use enum\IpTitle;
use model\SolIpDao;

class SolutionIpD2d extends Controller
{
    protected function run()
    {
        $this->assign('list2d', SolIpDao::getByTitle(IpTitle::D2D_2D));
        $this->assign('list3d', SolIpDao::getByTitle(IpTitle::D2D_3D));
    }
}