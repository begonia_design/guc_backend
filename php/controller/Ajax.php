<?php

namespace controller;

use controller\api\Game;
use lib\Format;
use lib\Mail;
use lib\Router;
use model;
use lib\Tools;
use lib\Db;
use lib\Config;

class Ajax extends Controller
{
    function __construct()
    {
        parent::__construct();

        $method = $_GET['type'];

        if (method_exists($this, $method)) {
            $this->$method();
        };
        exit;
    }

    private function errMsg($msg, $field = null)
    {
        $rs['status'] ='error';
        $rs['msg'] = $msg;
        if ($field != null) {
            $rs['field'] = $field;
        }
        echo json_encode($rs);
        exit;
    }

    private function successMsg($rs = null)
    {
        $rs['status'] = 'success';
        echo json_encode($rs);
        exit;
    }

    private function contact()
    {

        $fields = [
            "firstName" => "first_name",
            "lastName"=> "last_name",
            "company"=> "company",
            "email"=> "email",
            "country"=> "country",
            "phone"=> "phone",
            "desc"=> "description"
        ];

        $data = [
            'create_time' => date("Y-m-d H:i:s"),
            'status' => 0
        ];

        if (Router::getLang()=='cn'){
            if (!isset($_POST['captcha']) || $_POST['captcha'] != $_SESSION['contact']) {
                self::errMsg("recaptcha error");
            }
        } else {
            if (!Tools::checkRecaptcha()) {
                self::errMsg("recaptcha error");
            }
        }

        foreach ($fields as $key => $value) {
            if (!isset($_POST[$key]) || trim($_POST[$key]) == "") {
                self::errMsg("");
                exit;
            }
            $data[$value] = $_POST[$key];
        }
        Db::insert('contact', $data);

        self::successMsg();
    }
}