<?php

namespace controller;

use model\IndexBannerDao;
use model\IndexContentDao;
use model\InvFinancialsDao;
use model\InvFinMonthlyDao;
use model\InvFinMonthlyValueDao;
use model\InvFinNetDao;
use model\InvFinRevenuesDao;
use model\Tab;


class InvestorFinancialsMonthly extends Controller
{
    protected function run()
    {
        $yearArr = InvFinMonthlyDao::getLast3year();
        $year = (isset($yearArr[$_GET['year']]))?$_GET['year']:reset($yearArr);
        $list = InvFinMonthlyDao::getByYear($year);
        $sumValue = 0;
        $sumYOYJson = InvFinMonthlyValueDao::getYoYChange()['value'];
        $sumYOYArray = json_decode($sumYOYJson,true);
        $sumYOY = "";
        if (isset($sumYOYArray[$year])) {
            $sumYOY = $sumYOYArray[$year];
        }
        if ($list) {
            foreach($list as $k => $v) {
                $sumValue += $v['value'];
            }
        }
        $this->assign('years', $yearArr);
        $this->assign('list', $list);
        $this->assign('sumValue', $sumValue);
        $this->assign('sumYOY', $sumYOY);
        $this->assign('file', InvFinMonthlyValueDao::getFile()['value']);
    }
}