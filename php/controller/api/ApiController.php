<?php

namespace controller\api;

use lib\Config;
use lib\Tools;
use Logger;

/**
 * @SWG\Swagger(
 *     basePath="/api",
 *     produces={"application/json"},
 *     consumes={"application/json"},
 *     @SWG\Info(
 *         version="1.0",
 *         title="曼巴API",
 *         description="api網址範例 => http://mamba.demo-site.pro/api/member/get_token"
 *     )
 * )
 */
abstract Class ApiController
{
    private static $httpVersion = "HTTP/1.1";

    private static $key = Config::API_KEY;

    private static $iv = Config::API_IV;

    private static $isEncrypt = Config::API_IS_ENCRYPT;

    function __construct()
    {
        $auth = self::getHeader('Authorization');
        $ip = Tools::getIp();
        if(Config::TEST_LOG) {
            $data = json_encode(self::getRequest());
            Logger::getLogger(__METHOD__)->info("auth:{$auth}, ip:{$ip}");
            Logger::getLogger(__METHOD__)->info("data:{$data}");
        }
    }

    protected static function setIsEncrypt($boolean)
    {
        self::$isEncrypt = $boolean;
    }

    protected static function getRequest($isJson = true)
    {
        $content = file_get_contents('php://input');
        if (Config::TEST_LOG) {
            Logger::getLogger(__METHOD__)->info($content);
        }

        if (self::$isEncrypt) {
            $data = json_decode($content, true);
            $decryptData = self::requestDecrypt($data);
            return json_decode($decryptData, true);
        }

        if ($isJson) {
            return json_decode($content, true);
        } else {
            return $content;
        }
    }

    /**
     * 取得 header 值
     * @param $name
     * @return string
     */
    protected static function getHeader($name)
    {
        if (function_exists('apache_request_headers')) {
            return isset(apache_request_headers()[$name]) ? apache_request_headers()[$name] : '';
        }

        $name = 'HTTP_' . strtoupper(str_replace('-', '_', $name));
        return isset($_SERVER[$name]) ? $_SERVER[$name] : '';
    }

    /**
     * 取得訪問頁面時的請求方法
     * @return string
     */
    protected static function getRequestMethod()
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }

    protected static function isPost()
    {
        return (self::getRequestMethod() == 'post');
    }

    protected static function isGet()
    {
        return (self::getRequestMethod() == 'get');
    }


    /**
     * 回傳成功的預設格式
     * @param null $data
     */
    /**
     * @SWG\Definition(
     *     definition="ResponseSuccess",
     *     @SWG\Property(
     *          property="status",
     *          type="string",
     *          example="success"
     *    )
     *  )
     */
    protected static function responseSuccess($data = null)
    {
        if ($data == null && !is_array($data)) {
            $data['status'] = 'success';
        } else {
            if (self::$isEncrypt) {
                $data = self::responseEncrypt($data);
            }
        }
        self::response($data, 200);
    }

    /**
     * 回傳錯誤的預設格式
     * @param int $statusCode
     * @param null $msg
     */
    /**
     * @SWG\Definition(
     *     definition="ResponseError",
     *     @SWG\Property(
     *          property="status",
     *          type="string",
     *          example="error"
     *    ),
     *     @SWG\Property(
     *          property="msg",
     *          type="string",
     *          example="error msg"
     *    )
     *  )
     */
    protected static function responseError($statusCode = 500, $msg = null)
    {
        $data = ['status' => 'error'];
        if ($msg) {
            $data['msg'] = $msg;
        }
        self::response($data, $statusCode);
    }

    /**
     * 回傳格式
     * @param $msg
     * @param $statusCode
     */
    protected static function response($msg, $statusCode)
    {
        $contentType = 'application/json';
        $requestBody = json_encode($msg);
        if (Config::TEST_LOG) {
            Logger::getLogger(__METHOD__)->info($requestBody);
        }
        self::setHttpHeaders($contentType, $statusCode);
        echo $requestBody;
        exit;
    }

    protected static function responseEncrypt($params)
    {
        $encryptData = self::createMpgAesEncrypt($params);
        $hash = self::createSHA256($encryptData);
        return [
            'hash' => $hash,
            'data' => $encryptData
        ];
    }

    protected static function requestDecrypt($data)
    {
        if (!isset($data['hash']) || !isset($data['data'])) {
            self::responseError(400, "參數錯誤");
        }
        if ($data['hash'] != self::createSHA256($data['data'])) {
            self::responseError(400, "資料錯誤");
        }
        return self::createAesDecrypt($data['data']);
    }


    protected static function setHttpHeaders($contentType, $statusCode)
    {
        header('Access-Control-Allow-Origin:*');
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept');

        $statusMessage = self::getStatusCodeMessage($statusCode);
        header(self::$httpVersion . " " . $statusCode . " " . $statusMessage);
        header("Content-Type:" . $contentType);
    }

    /**
     * 定義http status code
     * @param $statusCode
     * @return mixed|string
     */
    protected static function getStatusCodeMessage($statusCode)
    {

        $codes = Array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported'
        );

        return (isset($codes[$statusCode])) ? $codes[$statusCode] : '';
    }

    protected static function checkRequired($jsonArray, $params)
    {
        foreach($params as $v) {
            if (!isset($jsonArray[$v]) || empty($jsonArray[$v])) {
                self::responseError(400, "{$v} is required");
                exit;
            }
        }
    }

    private static function createSHA256($ascData)
    {
        $key = self::$key;
        $iv = self::$iv;
        $string = "HashKey={$key}&{$ascData}&HashIV={$iv}";
        return strtoupper(hash("sha256",$string));
    }

    private static function createMpgAesEncrypt ($parameter = "")
    {
        $return_str = '';
        if (!empty($parameter)) {
            //將參數經過 URL ENCODED QUERY STRING
            $return_str = json_encode($parameter);
        }
        return trim(bin2hex(openssl_encrypt(
            self::addPadding($return_str),
            'aes-256-cbc',
            self::$key,
            OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING,
            self::$iv)));
    }

    private static function addPadding($string, $blocksize = 32)
    {
        $len = strlen($string);
        $pad = $blocksize - ($len % $blocksize);
        $string .= str_repeat(chr($pad), $pad);
        return $string;
    }

    private static function createAesDecrypt($parameter = "")
    {
        return self::strIpPadding(openssl_decrypt(
            hex2bin($parameter),
            'AES-256-CBC',
            self::$key,
            OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING,
            self::$iv
        ));
    }

    private static function strIpPadding($string)
    {
        $slast = ord(substr($string, -1));
        $slastc = chr($slast);
        $pcheck = substr($string, -$slast);
        if (preg_match("/$slastc{" . $slast . "}/", $string)) {
            $string = substr($string, 0, strlen($string) - $slast);
            return $string;
        } else {
            return false;
        }
    }
}