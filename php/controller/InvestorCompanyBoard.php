<?php

namespace controller;

use model\InvCorBoardDao;
use model\InvCorBoardTitleDao;

class InvestorCompanyBoard extends Controller
{

    protected function run()
    {
        $this->assign('titleList', InvCorBoardTitleDao::getList());
        $this->assign('list', InvCorBoardDao::get2LevelList());
    }
}