<?php

namespace controller;

use model\NewsSpotlightDao;


class NewsSpotlight extends Controller
{
    protected function run()
    {
        $yearArr = NewsSpotlightDao::getAllYear();
        $year = (isset($yearArr[$_GET['year']]))?$_GET['year']:reset($yearArr);
        $list = NewsSpotlightDao::getListByYear($year);

        $this->assign('years', $yearArr);
        $this->assign('list', $list);
    }
}