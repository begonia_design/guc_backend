<?php

namespace controller;

use lib\Tools;
use model\InvFinReportDao;


class InvestorFinancialsReport extends Controller
{

    protected function run()
    {
        $yearArr = InvFinReportDao::getLast7year();
        $year = (isset($yearArr[$_GET['year']]))?$_GET['year']:reset($yearArr);

        $newList = [];
        $list = InvFinReportDao::getByYear($year);
        if ($list) {
            foreach($list as $k => $v) {
                $newList[$v['quarterly']][$k] = $v;
                $newList[$v['quarterly']][$k]['ext'] = Tools::getFileExt($v['file']);
            }
        }
        $this->assign('years', $yearArr);
        $this->assign('year', $year);
        $this->assign('list', $newList);
    }
}