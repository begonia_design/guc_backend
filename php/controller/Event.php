<?php

namespace controller;

use model\EventDao;

class Event extends Controller
{
    protected function run()
    {
        $this->assign('list', EventDao::getAllList());
    }
}