<?php

namespace controller;

use model\IndexSort;
use model\NewsSpotlightDao;


class NewsSpotlightCont extends Controller
{
    protected function run()
    {
        $data = null;
        if(isset($_GET['k'])) {
            $data = NewsSpotlightDao::getByUrlId($_GET['k']);
        }
        if ($data == null) {
            header("location:news-spotlight.php");
            exit;
        }

        $this->assign('pageTitle', $data['title']);
        $this->assign('data', $data);
        $this->assign('prev', NewsSpotlightDao::getPrev($data['start_date'], $data['id']));
        $this->assign('next', NewsSpotlightDao::getNext($data['start_date'], $data['id']));
        $this->assign('list', IndexSort::getIndexSort(3));
    }
}