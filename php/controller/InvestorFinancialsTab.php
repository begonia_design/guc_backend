<?php

namespace controller;

use model\IndexBannerDao;
use model\IndexContentDao;
use model\InvFinancialsDao;
use model\InvFinNetDao;
use model\InvFinRevenuesDao;
use model\InvFinTabDao;
use model\Tab;


class InvestorFinancialsTab extends Controller
{
    protected function run()
    {
        $data = null;
        if (isset($_GET['k'])) {
            $data = InvFinTabDao::getByUrlId($_GET['k']);
        }

        if ($data == null) {
            header("location:error.php");
            exit;
        }

        $this->assign('pageTitle', $data['title']." - GUC");
        $this->assign('tab', $data);
    }
}