<?php

namespace controller;

use model\InvShaTabDao;

class InvestorShareholderTab extends Controller
{
    protected function run()
    {
        $data = null;
        if (isset($_GET['k'])) {
            $data = InvShaTabDao::getByUrlId($_GET['k']);
        }

        if ($data == null) {
            header("location:error.php");
            exit;
        }

        $this->assign('pageTitle', $data['title']." - GUC");
        $this->assign('tab', $data);
    }
}