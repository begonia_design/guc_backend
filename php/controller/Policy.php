<?php

namespace controller;

use model\PolicyRiskDao;


class Policy extends Controller
{
    protected function run()
    {
        $this->assign('list', PolicyRiskDao::getAll());
    }
}