<?php

namespace controller;

use model\InvCorCgoDao;
use model\InvCorPolicyDao;


class InvestorCompanyPolicy extends Controller
{

    protected function run()
    {
        $this->assign('list', InvCorPolicyDao::getAllList());
        $this->assign('updateTime', InvCorPolicyDao::getLastUpdateTime());
    }
}