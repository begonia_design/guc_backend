<?php

namespace controller;

use model\IndexBannerDao;
use model\IndexContentDao;
use model\IndexSort;
use model\InvFinancialsDao;
use model\InvFinMonthlyDao;
use model\InvFinMonthlyValueDao;
use model\InvFinNetDao;
use model\InvFinRevenuesDao;
use model\Tab;
use model\NewsDao;


class NewsCont extends Controller
{
    protected function run()
    {
        $data = null;
        if(isset($_GET['k'])) {
            $data = NewsDao::getByUrlId($_GET['k']);
        }
        if ($data == null) {
            header("location:news.php");
            exit;
        }

        $this->assign('pageTitle', $data['title']);
        $this->assign('data', $data);
        $this->assign('prev', NewsDao::getPrev($data['start_date'], $data['id']));
        $this->assign('next', NewsDao::getNext($data['start_date'], $data['id']));
        $this->assign('list', IndexSort::getIndexSort(3));
    }
}