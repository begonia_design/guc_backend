<?php

namespace controller;

use model\IndexBannerDao;
use model\IndexContentDao;
use model\InvFinancialsDao;
use model\InvFinMonthlyDao;
use model\InvFinMonthlyValueDao;
use model\InvFinNetDao;
use model\InvFinRevenuesDao;
use model\Tab;
use model\NewsDao;


class News extends Controller
{
    protected function run()
    {
        $yearArr = NewsDao::getAllYear();
        $year = (isset($yearArr[$_GET['year']]))?$_GET['year']:reset($yearArr);
        $list = NewsDao::getListByYear($year);

        $this->assign('years', $yearArr);
        $this->assign('list', $list);
    }
}