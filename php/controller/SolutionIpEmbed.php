<?php

namespace controller;

use enum\IpTitle;
use model\SolIpDao;

class SolutionIpEmbed extends Controller
{
    protected function run()
    {
        $this->assign('sram', SolIpDao::getByTitle(IpTitle::EMBED_SRAM));
        $this->assign('tcam', SolIpDao::getByTitle(IpTitle::EMBED_TCAM));
    }
}