<?php

namespace controller;

use model\PolicyRiskDao;


class PolicyCont extends Controller
{
    protected function run()
    {
        $p = null;
        if (isset($_GET['k'])) {
            $p = PolicyRiskDao::getByUrlId($_GET['k']);
        }
        if (!$p) {
            header("location:policy.php");
            exit;
        }
        $this->assign('pageTitle', $p['title']." - GUC");
        $this->assign('p', $p);
    }
}