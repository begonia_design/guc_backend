<?php

namespace controller;

use model\InvShaHistoryDao;


class InvestorShareholderHistory extends Controller
{

    protected function run()
    {

        $this->assign('list', InvShaHistoryDao::getAll());
        $this->assign('updateTime', InvShaHistoryDao::getLastUpdateTime());
    }
}