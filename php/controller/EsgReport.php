<?php

namespace controller;

use model\EsgDao;
use model\EsgReportDao;


class EsgReport extends Controller
{
    protected function run()
    {
        $this->assign('list', EsgReportDao::getList());
    }
}