<?php

namespace controller;

use model\AdminConfigDao;
use model\InvShaMajorDao;


class InvestorShareholderMajor extends Controller
{

    protected function run()
    {
        $this->assign('list', InvShaMajorDao::getAll());
        $this->assign('updateTime', AdminConfigDao::getMajorLastUpdate());
    }
}