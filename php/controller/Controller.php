<?php

namespace controller;

use model\InvCorTabDao;
use model\InvFinTabDao;
use model\InvShaTabDao;

class Controller extends BaseController
{
    function __construct()
    {
        parent::__construct();
        $this->assign("tabF", InvFinTabDao::getAll());
        $this->assign("tabC", InvCorTabDao::getAll());
        $this->assign("tabS", InvShaTabDao::getAll());
    }

    protected function run()
    {

    }
}