<?php

namespace controller;

use model\IndexBannerDao;
use model\IndexContentDao;
use model\InvFinAnnualDao;
use model\InvFinancialsDao;
use model\InvFinMonthlyDao;
use model\InvFinMonthlyValueDao;
use model\InvFinNetDao;
use model\InvFinRevenuesDao;
use model\Tab;


class InvestorFinancialsAnnual extends Controller
{
    protected function run()
    {
        $this->assign('list', InvFinAnnualDao::getList());
    }
}