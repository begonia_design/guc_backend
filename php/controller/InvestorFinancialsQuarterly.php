<?php

namespace controller;

use lib\Tools;
use model\IndexBannerDao;
use model\IndexContentDao;
use model\InvFinancialsDao;
use model\InvFinMonthlyDao;
use model\InvFinMonthlyValueDao;
use model\InvFinNetDao;
use model\InvFinQuarterlyDao;
use model\InvFinRevenuesDao;
use model\Tab;


class InvestorFinancialsQuarterly extends Controller
{

    protected function run()
    {
        $yearArr = InvFinQuarterlyDao::getLast7year();
        $year = (isset($yearArr[$_GET['year']]))?$_GET['year']:reset($yearArr);

        $newList = [];
        $list = InvFinQuarterlyDao::getByYear($year);
        if ($list) {
            foreach($list as $k => $v) {
                $newList[$v['quarterly']][$k] = $v;
                $newList[$v['quarterly']][$k]['ext'] = Tools::getFileExt($v['file']);
            }
        }
        $this->assign('years', $yearArr);
        $this->assign('year', $year);
        $this->assign('list', $newList);
    }
}