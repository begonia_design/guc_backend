<?php

namespace controller;

use model\InvCorCgoDao;


class InvestorCompanyCgo extends Controller
{

    protected function run()
    {
        $this->assign('data', InvCorCgoDao::getFirst());
    }
}