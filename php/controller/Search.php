<?php

namespace controller;

use lib\Config;
use lib\Restful;
use lib\Router;
use model\PolicyRiskDao;


class Search extends Controller
{
    protected function run()
    {
        if (Router::getLang()=='cn') {
            $count = 10;
            $page = (isset($_GET['page']))?(int)$_GET['page']:1;
            $startIndex = (($page-1)*$count) + 1;
            $param = [
                'cx' => Config::GOOGLE_SEARCH_CN,
                'key' => Config::GOOGLE_SEARCH_API_KEY,
                'q' => urlencode($_GET['q']),
                'start' => $startIndex,
                ];
            Restful::setCurlOption(Config::CURL_OPTION);
            $rs = Restful::getApi("https://customsearch.googleapis.com/customsearch/v1", $param);
            $list = [];
            $total = 0;
            $searchTime = 0;

            if (isset($rs['items'])) {
                $list = $rs['items'];
            }
            if (isset($rs['searchInformation']['totalResults'])) {
                $total = $rs['searchInformation']['totalResults'];
            }
            if (isset($rs['searchInformation']['formattedSearchTime'])) {
                $searchTime = $rs['searchInformation']['formattedSearchTime'];
            }

            //計算總頁數
            $totalPage = ceil($total / $count);

            $this->assign('searchTime', $searchTime);
            $this->assign('count', $count);
            $this->assign('total', $total);
            $this->assign('list', $list);
            $this->assign('totalPage', $totalPage);
            $this->assign('page', $page);
        }
    }
}