<?php

namespace controller;

use lib\Format;
use model\Common;
use lib\Db;
use lib\Tools;
use lib\Config;
use controller\Profession;
use model\Verify;

abstract Class BaseController
{
    private $assign = [];

    function __construct()
    {
        $this->run();
    }

    abstract protected function run();

    public function assign($key, $value)
    {
        $this->assign[$key] = $value;
    }

    public function getAssign()
    {
        return $this->assign;
    }

    public function setPageName($pageName)
    {
        $this->assign["pageName"] = $pageName;
    }

}