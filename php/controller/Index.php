<?php

namespace controller;

use model\IndexBannerDao;
use model\IndexContentDao;
use model\IndexSort;


class Index extends Controller
{
    protected function run()
    {
        $this->assign('bannerList', IndexBannerDao::getLimit3());
        $this->assign('newsList', IndexSort::getIndexSort());
        $this->assign('indexContent', IndexContentDao::getFirst());
    }
}