<?php

namespace controller;

use model\InvCorTabDao;

class InvestorCompanyTab extends Controller
{
    protected function run()
    {
        $data = null;
        if (isset($_GET['k'])) {
            $data = InvCorTabDao::getByUrlId($_GET['k']);
        }

        if ($data == null) {
            header("location:error.php");
            exit;
        }

        $this->assign('pageTitle', $data['title']." - GUC");
        $this->assign('tab', $data);
    }
}