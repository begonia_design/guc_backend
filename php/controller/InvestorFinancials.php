<?php

namespace controller;

use model\IndexBannerDao;
use model\IndexContentDao;
use model\InvFinancialsDao;
use model\InvFinNetDao;
use model\InvFinRevenuesDao;
use model\Tab;


class InvestorFinancials extends Controller
{
    protected function run()
    {
        //$this->assign('tab', Tab::getFinancials());
        $this->assign('c', InvFinancialsDao::getFirst());
        $this->assign('r', InvFinRevenuesDao::getAllList());
        $this->assign('n', InvFinNetDao::getAllList());
    }
}