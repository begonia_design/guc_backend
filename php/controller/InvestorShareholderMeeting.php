<?php

namespace controller;

use model\InvShaMeetingDao;


class InvestorShareholderMeeting extends Controller
{

    protected function run()
    {
        $yearArr = InvShaMeetingDao::getLast7year();
        $year = (isset($yearArr[$_GET['year']]))?$_GET['year']:reset($yearArr);

        $list = InvShaMeetingDao::getByYear($year);

        $this->assign('years', $yearArr);
        $this->assign('year', $year);
        $this->assign('list', $list);
    }
}