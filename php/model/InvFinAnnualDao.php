<?php

namespace model;

use lib\Db;
use lib\Router;

class InvFinAnnualDao extends BaseDao
{
    const TABLE_NAME = 'inv_fin_annual';

    public static function getList()
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE ".self::getBetweenDateSql()." ORDER BY year DESC LIMIT 4";
        return Db::getRowArray($sql, 'id');
    }

}