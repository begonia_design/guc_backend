<?php

namespace model;

use lib\Db;

class SolIpDao extends BaseDao
{
    const TABLE_NAME = 'sol_ip';

    public static function deleteAll()
    {
        Db::bindDelete(static::getDbName(),"1 = :id", [':id'=>1]);
    }

    public static function insert($excelRowData, $sort)
    {
        $time =  date("Y-m-d H:i:s");
        $data = [
            'part_number' => $excelRowData[0],
            'geometry' => $excelRowData[1],
            'page_name' => $excelRowData[2],
            'title' => $excelRowData[3],
            'description' => $excelRowData[4],
            'download_url' => $excelRowData[5],
            'sort' => $sort,
            'create_time' => $time,
            'update_time' => $time
        ];
        Db::insert(static::getDbName(), $data);
    }

    public static function getAllList()
    {
        $sql = "SELECT * FROM ". static::getDbName() ." ORDER BY sort";
        return Db::getRowArray($sql);
    }

    public static function getByTitle($title)
    {
        $sql = "SELECT * FROM ". static::getDbName() ." WHERE title = :title ORDER BY sort";
        return Db::getBindRowArray($sql, [':title' => $title], 'id' );
    }

    public static function get2LevelByPage($page)
    {
        $sql = "SELECT * FROM ". static::getDbName() ." WHERE page_name = :page ORDER BY sort";
        $data = Db::getBindRowArray($sql, [':page' => $page], 'id' );
        $newData = [];
        if ($data) {
            foreach ($data as $k => $v) {
                $newData[$v['title']][$k] = $v;
            }
        }
        return $newData;
    }

}