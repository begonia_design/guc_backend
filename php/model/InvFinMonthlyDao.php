<?php

namespace model;

use lib\Db;
use lib\Router;

class InvFinMonthlyDao extends BaseDao
{
    const TABLE_NAME = 'inv_fin_monthly';

    public static function getByYear($year)
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE year = :year AND ".self::getBetweenDateSql()." ORDER BY month";
        return Db::getBindRowArray($sql, [':year' => $year], 'id');
    }

    public static function getAllYear()
    {
        $sql = "SELECT DISTINCT(year) AS year FROM ".self::getDbName()." WHERE ".self::getBetweenDateSql()." ORDER BY year DESC";
        return Db::getRowArray($sql,'year', 'year');
    }

    public static function getLast3year()
    {
        $sql = "SELECT DISTINCT(year) AS year FROM ".self::getDbName()." WHERE ".self::getBetweenDateSql()." ORDER BY year DESC LIMIT 3";
        return Db::getRowArray($sql,'year', 'year');
    }

}