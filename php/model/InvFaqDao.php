<?php

namespace model;

use lib\Db;

class InvFaqDao extends BaseDao
{
    const TABLE_NAME = 'inv_faq';

    public static function getList()
    {
        $sql = "SELECT * FROM ". static::getDbName() . " WHERE status = 1 ORDER BY sort ASC";
        return Db::getRowArray($sql, 'id');
    }


}