<?php

namespace model;

use lib\Db;

class InvCorBoardDao extends BaseDao
{
    const TABLE_NAME = 'inv_cor_board';

    public static function get2LevelList(): array
    {
        $data = Db::getRowArray("SELECT * FROM ".self::getDbName()." WHERE status = 1 ORDER BY sort");
        $twoLevelData = [];
        if ($data) {
            foreach($data as $k => $v) {
                $twoLevelData[$v['title_id']][$k] = $v;
            }
        }
        return $twoLevelData;
    }

    public static function getByTitleId($titleId)
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE title_id = :titleId ORDER BY sort";
        return Db::getBindRowArray($sql, [':titleId' => $titleId], 'id');
    }

    public static function updateCloseTypeByTitleId($titleId)
    {
        Db::update(self::getDbName(), ['close_type' =>'2'], "title_id = :titleId", [':titleId' => $titleId]);
    }
}