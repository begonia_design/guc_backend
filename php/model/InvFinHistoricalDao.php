<?php

namespace model;

use lib\Db;
use lib\Router;

class InvFinHistoricalDao extends BaseDao
{
    const TABLE_NAME = 'inv_fin_historical';
}