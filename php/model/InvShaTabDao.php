<?php

namespace model;

use enum\InvestorTabType;
use lib\Db;
use lib\Router;

class InvShaTabDao extends InvFinTabDao
{
    const TABLE_NAME = 'inv_sha_tab';
}