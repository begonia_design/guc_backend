<?php

namespace model;

use lib\Db;
use lib\Router;

class InvFinReportDao extends BaseDao
{
    const TABLE_NAME = 'inv_fin_report';

    public static function getByYear($year)
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE year = :year AND ".self::getBetweenDateSql()." ORDER BY quarterly DESC, sort";
        return Db::getBindRowArray($sql, [':year' => $year], 'id');
    }

    public static function getLast7year()
    {
        $sql = "SELECT DISTINCT(year) AS year FROM ".self::getDbName()." WHERE ".self::getBetweenDateSql()." ORDER BY year DESC LIMIT 7";
        return Db::getRowArray($sql,'year', 'year');
    }

}