<?php

namespace model;

use lib\Db;

abstract class BaseStatusDao extends BaseDao
{
    const STATUS = 'status';

    public static function getListByStatus($status)
    {
        $sql = "SELECT * FROM " . static::TABLE_NAME . " WHERE " . static::STATUS. " = :status" ;
        return DB::getBindRowArray($sql, [':status' => $status], static::ID);
    }

    public static function getByIdAndStatus($id, $status)
    {
        $sql = "SELECT * FROM " . static::TABLE_NAME . " WHERE ".static::ID." = :id AND " . static::STATUS. " = :status";
        return DB::getBindRowData($sql, [':id' => $id, ':status' => $status]);
    }

}