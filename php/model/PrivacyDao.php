<?php

namespace model;

use lib\Db;
use lib\Router;

class PrivacyDao extends BaseDao
{
    const TABLE_NAME = 'privacy';
}