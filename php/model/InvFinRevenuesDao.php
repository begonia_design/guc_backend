<?php

namespace model;

use lib\Db;
use lib\Router;

class InvFinRevenuesDao extends BaseDao
{
    const TABLE_NAME = 'inv_fin_revenues';

    public static function getAllList()
    {
        return Db::getRowArray("SELECT * FROM ".self::getDbName()." ORDER BY year, quarterly");
    }

}