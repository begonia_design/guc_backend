<?php

namespace model;

use lib\Db;
use lib\Router;

class EsgDao extends BaseDao
{
    const TABLE_NAME = 'esg';

    public static function getAllList()
    {
        return Db::getRowArray("SELECT * FROM ".self::getDbName()." WHERE ".self::getBetweenDateSql()." ORDER BY sort");
    }
}