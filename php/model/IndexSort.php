<?php


namespace model;


class IndexSort
{
    public static function updateSort($indexSort, $id, $type = 0)
    {
        NewsDao::setBackend();
        NewsSpotlightDao::setBackend();

        $data = NewsDao::getByIndexSort($indexSort, ($type == 0)?$id:null);
        if ($data) {
            $indexSort = ($indexSort == 6)?0:$indexSort+1;
            NewsDao::updateIndexSort($data['id'], $indexSort);
            if ($indexSort > 0) {
                self::updateSort($indexSort, $data['id'], 0);
            }
        } else {
            $data = NewsSpotlightDao::getByIndexSort($indexSort, ($type == 1)?$id:null);
            if ($data) {
                $indexSort = ($indexSort == 6) ? 0 : $indexSort + 1;
                NewsSpotlightDao::updateIndexSort($data['id'], $indexSort);
                if ($indexSort > 0) {
                    self::updateSort($indexSort, $data['id'], 1);
                }
            }
        }



    }

    public static function getIndexSort($limit = 6)
    {
        $newsData = NewsDao::getIndexList();
        $spotLightData = NewsSpotlightDao::getIndexList();
        $data = $newsData + $spotLightData;
        ksort($data);
        return array_slice($data, 0, $limit);
    }

    public static function getIndexSortForAdmin($limit = 6)
    {
        $newsData = NewsDao::getIndexListForAdmin();
        $spotLightData = NewsSpotlightDao::getIndexListForAdmin();
        $data = $newsData + $spotLightData;
        ksort($data);
        return array_slice($data, 0, $limit);
    }
}