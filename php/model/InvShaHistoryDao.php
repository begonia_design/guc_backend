<?php

namespace model;

use lib\Db;

class InvShaHistoryDao extends BaseDao
{
    const TABLE_NAME = 'inv_sha_history';

    public static function getAll()
    {
        $sql = "SELECT * FROM ". static::getDbName() . " ORDER BY year DESC, id ASC";
        return Db::getRowArray($sql, 'id');
    }


}