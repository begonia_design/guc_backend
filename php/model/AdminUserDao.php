<?php

namespace model;

use lib\Db;

class AdminUserDao extends BaseDao
{
    const TABLE_NAME = 'admin_user';
    const ACCOUNT = 'account';
    const PASSWORD = 'password';
    const NAME = 'name';
    const EMAIL = 'email';
    const STATUS = 'status';

}