<?php

namespace model;

use lib\Db;
use lib\Router;

class InvCorPolicyDao extends BaseDao
{
    const TABLE_NAME = 'inv_cor_policy';

    public static function getAllList()
    {
        return Db::getRowArray("SELECT * FROM ".self::getDbName()." WHERE ".self::getBetweenDateSql()." ORDER BY sort");
    }

    public static function getLastUpdateTime()
    {
        $sql = "SELECT * FROM ". self::getDbName() . " WHERE" .self::getBetweenDateSql()." ORDER BY update_time DESC LIMIT 1";
        return Db::getRowData($sql,'update_time');
    }
}