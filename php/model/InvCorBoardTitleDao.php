<?php

namespace model;

use lib\Db;
use lib\Router;

class InvCorBoardTitleDao extends BaseDao
{
    const TABLE_NAME = 'inv_cor_board_title';

    public static function getTitleList()
    {
        return Db::getRowArray("SELECT id, title FROM ".self::getDbName()." ORDER BY sort",'id','title');
    }

    public static function getList()
    {
        return Db::getRowArray("SELECT * FROM ".self::getDbName()." WHERE status = 1 ORDER BY sort");
    }
}