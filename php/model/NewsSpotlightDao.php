<?php

namespace model;

use enum\InvestorTabType;
use lib\Db;
use lib\Router;

class NewsSpotlightDao extends NewsDao
{
    const TABLE_NAME = 'news_spotlight';

    public static function getIndexList()
    {
        $sql = "SELECT *,'news-spotlight' AS page FROM ".self::getDbName()." WHERE status = 1 AND ". self::getBetweenDateSql()." AND index_sort > 0 ORDER BY index_sort";
        return Db::getRowArray($sql, 'index_sort');
    }

    public static function getIndexListForAdmin()
    {
        $sql = "SELECT *,'news-spotlight' AS page FROM ".self::getDbName()." WHERE index_sort > 0 ORDER BY index_sort";
        return Db::getRowArray($sql, 'index_sort');
    }

}