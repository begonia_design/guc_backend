<?php

namespace model;

use lib\Db;
use lib\Router;

class InvFinNetDao extends BaseDao
{
    const TABLE_NAME = 'inv_fin_net';

    public static function getAllList()
    {
        return Db::getRowArray("SELECT * FROM ".self::getDbName()." ORDER BY year, quarterly");
    }

}