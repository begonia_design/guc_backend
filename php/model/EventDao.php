<?php

namespace model;

use lib\Db;
use lib\Router;

class EventDao extends BaseDao
{
    const TABLE_NAME = 'event';

    public static function getAllList()
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE status = 1 AND ".self::getBetweenDateSql()." ORDER BY event_start_date DESC, id DESC";
        return Db::getRowArray($sql,'id');
    }

}