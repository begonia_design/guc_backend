<?php

namespace model;

use lib\Db;
use lib\Router;

class IndexContentDao extends BaseDao
{
    const TABLE_NAME = 'index_content';

    public static function get()
    {
        $sql = "SELECT * FROM ".self::getDb()." WHERE id = 1 " ;
        return DB::getRowData($sql);
    }

}