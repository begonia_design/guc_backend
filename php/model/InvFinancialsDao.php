<?php

namespace model;

use lib\Db;
use lib\Router;

class InvFinancialsDao extends BaseDao
{
    const TABLE_NAME = 'inv_financials';
}