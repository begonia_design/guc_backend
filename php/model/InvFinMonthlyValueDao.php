<?php

namespace model;

use lib\Db;
use lib\Router;

class InvFinMonthlyValueDao extends BaseDao
{
    const TABLE_NAME = 'inv_fin_mon_value';

    public static function getFile()
    {
        return self::getByKey(1);
    }

    public static function getYoYChange()
    {
        return self::getByKey(2);
    }

}