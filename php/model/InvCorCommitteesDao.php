<?php

namespace model;

use lib\Db;

class InvCorCommitteesDao extends BaseDao
{
    const TABLE_NAME = 'inv_cor_committees';

    public static function getAll()
    {
        $sql = "SELECT * FROM ". static::getDbName() . " WHERE " . self::getBetweenDateSql(). " ORDER BY sort ASC";
        return Db::getRowArray($sql, 'id');
    }

}