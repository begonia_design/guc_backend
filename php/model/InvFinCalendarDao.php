<?php

namespace model;

use lib\Db;
use lib\Router;

class InvFinCalendarDao extends BaseDao
{
    const TABLE_NAME = 'inv_fin_calendar';

    public static function getAll()
    {
        $sql = "SELECT * FROM ". static::getDbName() . " ORDER BY date ASC";
        return Db::getRowArray($sql, 'id');
    }

}