<?php

namespace model;

use enum\InvestorTabType;
use lib\Db;
use lib\Router;

class InvCorTabDao extends InvFinTabDao
{
    const TABLE_NAME = 'inv_cor_tab';
}