<?php

namespace model;

use lib\Db;

class InvShaMajorDao extends BaseDao
{
    const TABLE_NAME = 'inv_sha_major';

    public static function getAll()
    {
        $sql = "SELECT * FROM ". static::getDbName() . " ORDER BY number ASC, id ASC";
        return Db::getRowArray($sql, 'id');
    }


}