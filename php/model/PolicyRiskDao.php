<?php

namespace model;

use enum\InvestorTabType;
use lib\Db;
use lib\Router;

class PolicyRiskDao extends BaseDao
{
    const TABLE_NAME = 'policy_risk';

    public static function getAll()
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE status = 1 AND ". self::getBetweenDateSql()." ORDER BY sort";
        return Db::getRowArray($sql, 'id');
    }

    public static function getByUrlId($key)
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE status = 1 AND ". self::getBetweenDateSql()." AND url_id = :key";
        return Db::getBindRowData($sql, [':key' => $key]);
    }

    public static function getAllByUrlId($key, $excludeId = null)
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE url_id = :key";
        $bind = [':key' => $key];
        if ($excludeId != null) {
            $sql .= " AND id != :id";
            $bind[':id'] = $excludeId;
        }
        return Db::getBindRowData($sql, $bind);
    }

}