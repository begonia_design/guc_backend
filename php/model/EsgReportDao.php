<?php

namespace model;

use lib\Db;
use lib\Router;

class EsgReportDao extends BaseDao
{
    const TABLE_NAME = 'esg_report';

    public static function getList()
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE ".self::getBetweenDateSql()." ORDER BY year DESC";
        return Db::getRowArray($sql, 'id');
    }

    public static function getFirst()
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE ".self::getBetweenDateSql()." ORDER BY year DESC Limit 1";
        return Db::getRowData($sql);
    }

}