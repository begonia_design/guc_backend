<?php

namespace model;

use lib\Db;
use lib\Router;

class IndexBannerDao extends BaseDao
{
    const TABLE_NAME = 'index_banner';

    public static function getLimit3()
    {
        $sql = "SELECT * FROM " . self::getDbName() . " WHERE status = 1 ORDER BY sort LIMIT 3" ;
        return DB::getRowArray($sql, static::ID);
    }

}