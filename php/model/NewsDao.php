<?php

namespace model;

use enum\InvestorTabType;
use lib\Db;
use lib\Router;

class NewsDao extends BaseDao
{
    const TABLE_NAME = 'news';

    public static function getAll()
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE status = 1 AND ". self::getBetweenDateSql()." ORDER BY start_date DESC, id DESC";
        return Db::getRowArray($sql, 'id');
    }

    public static function getByUrlId($key)
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE status = 1 AND ". self::getBetweenDateSql()." AND url_id = :key";
        return Db::getBindRowData($sql, [':key' => $key]);
    }

    public static function getAllByUrlId($key, $excludeId = null)
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE url_id = :key";
        $bind = [':key' => $key];
        if ($excludeId != null) {
            $sql .= " AND id != :id";
            $bind[':id'] = $excludeId;
        }
        return Db::getBindRowData($sql, $bind);
    }

    public static function getListByYear($year)
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE status = 1 AND ". self::getBetweenDateSql();
        $sql .= " AND YEAR(start_date) = :year ORDER BY start_date DESC, id DESC";
        return Db::getBindRowArray($sql, [':year' => $year], 'id');
    }

    public static function getLast3year()
    {
        $sql = "SELECT DISTINCT(YEAR(start_date)) AS year FROM ".self::getDbName()." WHERE status = 1 AND ".self::getBetweenDateSql()." ORDER BY start_date DESC  LIMIT 3";
        return Db::getRowArray($sql,'year', 'year');
    }

    public static function getAllYear()
    {
        $sql = "SELECT DISTINCT(YEAR(start_date)) AS year FROM ".self::getDbName()." WHERE status = 1 AND ".self::getBetweenDateSql()." ORDER BY start_date DESC ";
        return Db::getRowArray($sql,'year', 'year');
    }

    public static function getIndexList()
    {
        $sql = "SELECT *,'news' AS page FROM ".self::getDbName()." WHERE status = 1 AND ". self::getBetweenDateSql()." AND index_sort > 0 ORDER BY index_sort";
        return Db::getRowArray($sql, 'index_sort');
    }

    public static function getIndexListForAdmin()
    {
        $sql = "SELECT *,'news' AS page FROM ".self::getDbName()." WHERE index_sort > 0 ORDER BY index_sort";
        return Db::getRowArray($sql, 'index_sort');
    }

    public static function getPrev($startDate, $id)
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE status = 1 AND ". self::getBetweenDateSql();
        $sql .= " AND start_date > :startDate OR (start_date = :startDate2 AND id > :id)";
        $sql .= " ORDER BY start_date ASC, id ASC LIMIT 1";
        return Db::getBindRowData($sql, [':startDate' => $startDate, ':startDate2' => $startDate, ':id' => $id]);
    }

    public static function getNext($startDate, $id)
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE status = 1 AND ". self::getBetweenDateSql();
        $sql .= " AND start_date < :startDate OR (start_date = :startDate2 AND id < :id)";
        $sql .= " ORDER BY start_date DESC, id DESC LIMIT 1";
        return Db::getBindRowData($sql, [':startDate' => $startDate, ':startDate2' => $startDate, ':id' => $id]);
    }


    public static function getByIndexSort($indexSort, $excludeId = null)
    {
        $sql = "SELECT * FROM ".self::getDbName()." WHERE index_sort = :indexSort";
        $bind = [':indexSort'=>$indexSort];
        if ($excludeId != null) {
            $sql .= " AND id != :id";
            $bind[':id'] = $excludeId;
        }
        return Db::getBindRowData($sql, $bind);
    }

    public static function updateIndexSort($id, $indexSort)
    {
        Db::update(self::getDbName(),['index_sort' => $indexSort],"id = :id", [':id' => $id]);
    }

}