<?php

namespace model;

use lib\Db;
use lib\Router;

abstract class BaseDao
{
    const TABLE_NAME = "";
    const ID = 'id';
    const CREATE_TIME = 'create_time';
    const UPDATE_TIME = 'update_time';

    protected static $backend = false;

    public static function getByKey($id = 1)
    {
        $sql = sprintf("SELECT * FROM %s WHERE %s = :id ", static::getDbName(), static::ID);
        return Db::getBindRowData($sql,[':id' => $id]);
    }

    public static function getFirst()
    {
        return self::getByKey(1);
    }

    public static function getAll()
    {
        $sql = "SELECT * FROM ". static::getDbName();
        return Db::getRowArray($sql, 'id');
    }

    protected static function getBetweenDateSql(): string
    {
        return " start_date <= DATE(NOW()) AND IF(end_date='',DATE(NOW()),end_date) >= DATE(NOW())";
    }

    public static function getDbName(): string
    {
        if (self::$backend) {
            return static::TABLE_NAME . "_" . $_SESSION['lang'];
        } else {
            return static::TABLE_NAME . "_" . Router::getLang();
        }
    }

    public static function setBackend()
    {
        self::$backend = true;
    }


    public static function getLastUpdateTime()
    {
        $sql = "SELECT * FROM ". self::getDbName() . " ORDER BY update_time DESC LIMIT 1";
        return Db::getRowData($sql,'update_time');
    }

    public static function update(array $arrayData, $whereQuery, array $bindValue = null)
    {
        Db::update(static::getDbName(),$arrayData, $whereQuery, $bindValue);
    }

}