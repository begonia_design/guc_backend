<?php
namespace enum;

class BuildingCategory extends Enum
{
    const CAT1 = 1;
    const CAT2 = 2;

    const ENUM = [
        self::CAT1 => '營建工程',
        self::CAT2 => '鋼構工程'
    ];

    const ENUM_EN = [
        self::CAT1 => 'Construction project',
        self::CAT2 => 'Steel structure'
    ];

    const ENUM_JP = [
        self::CAT1 => '建設工事',
        self::CAT2 => '鉄骨工事'
    ];

}