<?php
namespace enum;

class InvestorTabType extends Enum
{
    const FINANCIALS = 1;
    const CORPORATE = 2;
    const SHAREHOLDER = 3;

    const ENUM = [
        self::FINANCIALS => 'Financials',
        self::CORPORATE => 'Corporate Governance',
        self::SHAREHOLDER => 'Shareholder Services',
    ];

}