<?php
namespace enum;

class IpPage extends Enum
{
    const D2D = 'Die-to-Die (GLink-2.5D 3D) IP';
    const EMBED = 'Embedded Memory IP';
    const HBM = 'High Bandwidth Memory IP';
    const MIXED = 'Mixed-Signal Front-End IP';
    const SERDES = 'High Speed SerDes IP';

    const ENUM = [
        self::D2D => self::D2D,
        self::EMBED => self::EMBED,
        self::HBM => self::HBM,
        self::MIXED => self::MIXED,
        self::SERDES => self::SERDES
    ];
}