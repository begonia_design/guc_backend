<?php
namespace enum;

class BoardCloseType extends Enum
{
    const NOT_CLOSE = 1;
    const CLOSE = 2;

    const ENUM = [
        self::NOT_CLOSE => '不收折',
        self::CLOSE => '收折',
    ];
}