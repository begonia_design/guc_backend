<?php
namespace enum;

use lib\Config;
use lib\Router;

abstract class Enum
{
    const ENUM = [];

    const ENUM_TW = [];

    const ENUM_CN = [];

    public static function get($key = null)
    {
        switch (Router::getLang()) {
            case 'tw':
                $arr = (static::ENUM_TW)?static::ENUM_TW:static::ENUM;
                break;
            case 'cn':
                $arr = (static::ENUM_CN)?static::ENUM_CN:static::ENUM;
                break;
            default:
                $arr = static::ENUM;
        }

        if ($key === null) {
            return $arr;
        } else {
            return $arr[$key];
        }
    }

    /**
     * @param $key
     * @return bool
     */
    public static function isKey($key)
    {
        $arr = static::ENUM;
        return isset($arr[$key]);
    }

    /**
     * @param $value
     * @return bool
     */
    public static function exist($value)
    {
        return in_array($value, static::ENUM);
    }
}