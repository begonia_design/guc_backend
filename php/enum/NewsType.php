<?php
namespace enum;

class NewsType extends Enum
{
    const ADVANCED = 1;
    const DESIGN = 2;
    const CUSTOMER = 3;
    const INVESTORS = 4;
    const PRESS_RELEASE = 5;

    const ENUM = [
        self::ADVANCED => 'Advanced Packaging Technology Leadership',
        self::DESIGN => 'Design Engineering Excellence',
        self::CUSTOMER => 'Customer Trusted Service',
        self::INVESTORS => 'Investors',
        self::PRESS_RELEASE => 'Press Release'
    ];

    public static function isVisible($key): bool
    {
        return ($key <= 3);
    }

}