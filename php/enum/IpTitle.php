<?php
namespace enum;

class IpTitle extends Enum
{
    const D2D_3D = 'Die-on-Die (GLink-3D) IP';
    const D2D_2D = 'Die-to-Die (GLink-2.5D) IP';
    const EMBED_SRAM = 'SRAM Compiler';
    const EMBED_TCAM = 'TCAM Compiler';
    const HBM_HBM = 'High Bandwidth Memory IP';
    const SERDES_SERDES = 'SerDes IP';
    const MIXED_5G = '5G AFE';
    const MIXED_ADC = 'ADC';
    const MIXED_DAC = 'DAC';
    const MIXED_WIFI = 'Wi-Fi AFE';

    const ENUM = [
        self::D2D_3D => self::D2D_3D,
        self::D2D_2D => self::D2D_2D,
        self::EMBED_SRAM => self::EMBED_SRAM,
        self::EMBED_TCAM => self::EMBED_TCAM,
        self::HBM_HBM => self::HBM_HBM,
        self::SERDES_SERDES => self::SERDES_SERDES,
        self::MIXED_5G => self::MIXED_5G,
        self::MIXED_ADC => self::MIXED_ADC,
        self::MIXED_DAC => self::MIXED_DAC,
        self::MIXED_WIFI => self::MIXED_WIFI
    ];
}