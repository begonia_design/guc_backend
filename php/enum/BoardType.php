<?php
namespace enum;

class BoardType extends Enum
{
    const TEXT = 1;
    const TABLE = 2;
    const TEXT_AND_TABLE = 3;

    const ENUM = [
        self::TEXT => '文字',
        self::TABLE => '表格',
        self::TEXT_AND_TABLE => '文字+表格'
    ];
}