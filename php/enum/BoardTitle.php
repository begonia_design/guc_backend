<?php
namespace enum;

class BoardTitle extends Enum
{
    const ALL_CLOSE = 1;
    const CUSTOM = 2;

    const ENUM = [
        self::ALL_CLOSE => '全部收折',
        self::CUSTOM => '各別設定',
    ];
}