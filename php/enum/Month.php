<?php
namespace enum;

class Month extends Enum
{
    const JAN = 1;
    const FEB = 2;
    const MAR = 3;
    const APR = 4;
    const MAY = 5;
    const JUN = 6;
    const JUL = 7;
    const AUG = 8;
    const SEP = 9;
    const OCT = 10;
    const NOV = 11;
    const DEC = 12;

    const ENUM = [
        self::JAN => 'January',
        self::FEB => 'February',
        self::MAR => 'March',
        self::APR => 'April',
        self::MAY => 'May',
        self::JUN => 'June',
        self::JUL => 'July',
        self::AUG => 'August',
        self::SEP => 'September',
        self::OCT => 'October',
        self::NOV => 'November',
        self::DEC => 'December'
    ];

    const ENUM_TW = [
        self::JAN => '一月',
        self::FEB => '二月',
        self::MAR => '三月',
        self::APR => '四月',
        self::MAY => '五月',
        self::JUN => '六月',
        self::JUL => '七月',
        self::AUG => '八月',
        self::SEP => '九月',
        self::OCT => '十月',
        self::NOV => '十一月',
        self::DEC => '十二月'
    ];

    const ENUM_CN = self::ENUM_TW;

}