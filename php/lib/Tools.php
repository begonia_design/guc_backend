<?php
namespace lib;

use Phpmailer;
use ReCaptcha;

class Tools
{
    /**
     * 輸出縮圖
     * @param $dir
     * @param $img
     * @param $w
     * @param $h
     * @return string
     */
    public static function sImg($dir, $img, $w, $h)
    {
        Config::UPLOAD_DIR;
        return "/timthumb.php?src=/".Config::UPLOAD_DIR."{$dir}/{$img}&h={$h}&w={$w}&zc=1";
    }

    /**
     * 輸出驗証圖型
     * @param $w
     * @param $h
     * @param string $session_name
     * @return string
     */
    public static function check_img($w, $h, $session_name = '')
    {
        $site_dir = (count(config::$site_language) > 1) ? '../' : '';
        $img = 'checkimg.php?';
        $img .= ($session_name == '') ? '' : 'n=' . $session_name . '&';
        return "<a href=\"javascript:;\" onClick=\"$(this).find('img').attr('src','{$img}'+Math.random())\"><img src=\"{$site_dir}{$img}\" style=\"vertical-align:middle;width:{$w}px;height:{$h}px;\"></a>";
    }

    /**
     * 取得檔案大小
     * @param $path
     * @return string
     */
    public static function getFileSize($path)
    {
        $bytes = sprintf('%u', @filesize($path));
        if ($bytes > 0) {
            $unit = intval(log($bytes, 1024));
            $units = array('B', 'KB', 'MB', 'GB');
            if (array_key_exists($unit, $units) === true) {
                return sprintf('%d %s', $bytes / pow(1024, $unit), $units[$unit]);
            }
        }
        return $bytes;
    }

    /**
     * 取得檔案副檔名
     * @param $path
     * @return mixed
     */
    public static function getFileExt($path)
    {
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        return $ext;
    }

    /**
     * 取得網址最後網頁名稱
     * @return mixed|string
     */
    public static function getPageNameFromUrl()
    {
        $path = parse_url($_SERVER['REQUEST_URI']);
        $pathArr = explode('/', $path['path']);
        $page = end($pathArr);
        $page = (substr($page, -4) == '.php') ? substr($page, 0, -4) : $page;
        return $page;
    }


    /**
     * 轉換成駝峰命名 aaa_bbb => AaaBbb
     * @param $txt
     * @param bool $first
     * @return string
     */
    public static function getUpperCamel($txt, $first = true)
    {
        $arr = explode("-", $txt);
        $return = "";
        if ($arr) {
            foreach ($arr as $k => $v) {
                if ($k == 0 && !$first) {
                    $return .= $v;
                } else {
                    $return .= ucfirst($v);
                }
            }
        }
        return $return;
    }

    /**
     * 計算座標距離
     * @param $lng1
     * @param $lat1
     * @param $lng2
     * @param $lat2
     * @return int
     */
    public static function getDistance($lat1, $lng1, $lat2, $lng2)
    {
        $earthRadius = 6367000; //approximate radius of earth in meters

        $lat1 = ($lat1 * pi() ) / 180;
        $lng1 = ($lng1 * pi() ) / 180;

        $lat2 = ($lat2 * pi() ) / 180;
        $lng2 = ($lng2 * pi() ) / 180;

        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);  $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;

        return round($calculatedDistance);
    }


    /**
     * 寄送EMAIL
     * @param $mailTo
     * @param $subject
     * @param $msg
     * @param null $BCc
     */
    public static function sendMail($mailTo, $subject, $msg, $testMode = false)
    {
        if (strtolower(Config::EMAIL_TYPE) == 'smtp') {
            self::smtpMail($mailTo, $subject, $msg, $testMode);
        } else {
            self::phpMail($mailTo, $subject, $msg);
        }
    }

    /**
     * 使用PHP內建方式寄送
     * @param $mailTo
     * @param $subject
     * @param $msg
     * @param $BCc
     */
    private static function phpMail($mailTo, $subject, $msg)
    {
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=utf-8\r\n";
        $headers .= "From: " . Config::EMAIL_FROM_NAME . "<" . Config::EMAIL_FROM . ">\r\n";
        $subject = "=?utf-8?B?" . base64_encode($subject) . "?=";
        //$headers =  iconv("UTF-8" ,"Big5" , $headers);
        if (is_array($mailTo)) {
            foreach ($mailTo as $mail) {
                mail($mail, $subject, $msg, $headers);
            }
        } else {
            mail($mailTo, $subject, $msg, $headers);
        }
    }

    /**
     * 使用SMTP的方式寄送
     * @param $mailTo
     * @param $subject
     * @param $msg
     * @param $BCc
     */
    private static function smtpMail($mailTo, $subject, $msg)
    {
        $mail = new \PHPMailer\PHPMailer\PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPAutoTLS = false;
        if (Config::EMAIL_SMTP_USERNAME != "") {
            $mail->SMTPAuth = true;
        } else {
            $mail->SMTPAuth = false;
        }
        $mail->Port = (Config::EMAIL_SMTP_PORT == '')?25:Config::EMAIL_SMTP_PORT;
        $mail->CharSet = "utf-8";
        $mail->Host = Config::EMAIL_SMTP_HOST;
        $mail->Username = Config::EMAIL_SMTP_USERNAME;
        $mail->Password = Config::EMAIL_SMTP_PASSWORD;
        $mail->From = Config::EMAIL_FROM;
        $mail->FromName = Config::EMAIL_FROM_NAME;
        $mail->IsHTML(true);
        if (Config::EMAIL_SMTP_SECURE != '') {
            $mail->SMTPSecure = Config::EMAIL_SMTP_SECURE;
        }

        $mail->Subject = "=?utf-8?B?" . base64_encode($subject) . "?=";
        $mail->Body = $msg;

        try {
            if (is_array($mailTo)) {
                foreach ($mailTo as $to) {
                    $mail->AddAddress($to);
                    if (!$mail->Send()) {
                        \Logger::getLogger(__METHOD__)->error($mail->ErrorInfo);
                    }
                }
            } else {
                $mail->AddAddress($mailTo);
                if (!$mail->Send()) {
                    \Logger::getLogger(__METHOD__)->error($mail->ErrorInfo);
                }
            }
        } catch (\Exception $e) {
            \Logger::getLogger(__METHOD__)->error($e->getMessage());
        }
    }

    /**
     * 簡查Email格式是否正確
     * @param $email
     * @return int
     */
    public static function checkEmail($email)
    {
        return preg_match('/^[_.0-9a-z-]+@([0-9a-z-]+.)+[a-z]{2,3}$/', $email);
    }

    /**
     * 身份証字號
     */
    public static function checkTwId($id)
    {
        $id = strtoupper($id);
        //建立字母分數陣列
        $headPoint = array(
            'A' => 1, 'I' => 39, 'O' => 48, 'B' => 10, 'C' => 19, 'D' => 28,
            'E' => 37, 'F' => 46, 'G' => 55, 'H' => 64, 'J' => 73, 'K' => 82,
            'L' => 2, 'M' => 11, 'N' => 20, 'P' => 29, 'Q' => 38, 'R' => 47,
            'S' => 56, 'T' => 65, 'U' => 74, 'V' => 83, 'W' => 21, 'X' => 3,
            'Y' => 12, 'Z' => 30
        );
        //建立加權基數陣列
        $multiply = array(8, 7, 6, 5, 4, 3, 2, 1);
        //檢查身份字格式是否正確
        if (preg_match("/^[a-zA-Z][1-2][0-9]+$/", $id) AND strlen($id) == 10) {
            //切開字串
            $stringArray = str_split($id);
            //取得字母分數(取頭)
            $total = $headPoint[array_shift($stringArray)];
            //取得比對碼(取尾)
            $point = array_pop($stringArray);
            //取得數字部分分數
            $len = count($stringArray);
            for ($j = 0; $j < $len; $j++) {
                $total += $stringArray[$j] * $multiply[$j];
            }
            //計算餘數碼並比對
            $last = (($total % 10) == 0) ? 0 : (10 - ($total % 10));
            if ($last != $point) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }


    /**
     * 輸出限制字串長度
     * @param $txt
     * @param int $num
     * @param string $suffix
     * @return string
     */
    public static function cutTxt($txt, $num = 20, $suffix = "...")
    {
        if ($txt != '') {
            $tt = (mb_strlen($txt, "UTF8") > $num) ? $suffix : "";
            return mb_substr(strip_tags($txt), 0, $num, 'UTF-8') . $tt;
        }
    }

    /**
     * 取得星期
     * @param $datetime
     * @return string
     */
    public static function getWeekday($datetime)
    {
        $weekday = date('w', strtotime($datetime));
        $week = array('日', '一', '二', '三', '四', '五', '六');
        return '星期' . $week[$weekday];
    }

    /**
     * 取得IP
     * @return mixed
     */
    public static function getIp()
    {
        if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else {
            $ip = $_SERVER["REMOTE_ADDR"];
        }
        return $ip;
    }

    /**
     * @param $filePath
     * @param $paramArray
     * @return mixed
     */
    public static function mailLayout($filePath, $paramArray)
    {
        //讀取mail內容
        $file = fopen($filePath,"r");
        $mailContent = fread($file, 9999);
        fclose($file);

        $replaceKeys = array_keys($paramArray);

        //套入mail
        $msg = str_replace($replaceKeys, $paramArray, $mailContent);
        return $msg;
    }


    public static function ga($ga_id = config::ga_id)
    {
        if ($ga_id) {
            ?>
            <script>
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                ga('create', '<?php echo config::ga_id?>', 'auto');
                ga('send', 'pageview');
            </script>
            <?php
        }
    }


    public static function isMobile()
    {

        if (isset ($_SERVER['HTTP_X_WAP_PROFILE'])) {
            return true;
        }

        if (isset ($_SERVER['HTTP_VIA'])) {
            return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
        }

        if (isset ($_SERVER['HTTP_USER_AGENT'])) {
            $clientKeyords = array('nokia',
                'sony',
                'ericsson',
                'mot',
                'samsung',
                'htc',
                'sgh',
                'lg',
                'sharp',
                'sie-',
                'philips',
                'panasonic',
                'alcatel',
                'lenovo',
                'iphone',
                'ipod',
                'blackberry',
                'meizu',
                'android',
                'netfront',
                'symbian',
                'ucweb',
                'windowsce',
                'palm',
                'operamini',
                'operamobi',
                'openwave',
                'nexusone',
                'cldc',
                'midp',
                'wap',
                'mobile'
            );

            if (preg_match("/(" . implode('|', $clientKeyords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
                return true;
            }
        }

        if (isset ($_SERVER['HTTP_ACCEPT'])) {

            if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
                return true;
            }
        }
        return false;
    }


    public static function getYoutubeId($url)
    {
        $pattern = '#^(?:https?://|//)?(?:www\.|m\.)?(?:youtu\.be/|youtube\.com/(?:embed/|v/|watch\?v=|watch\?.+&v=))([\w-]{11})(?![\w-])#';
        preg_match($pattern, $url, $matches);
        return (isset($matches[1])) ? $matches[1] : false;
    }

    /**
     * 生成recaptch圖
     * @param string $language
     */
    public static function recaptche($language = 'zh-Tw')
    {
        ?>
        <div class="g-recaptcha" data-sitekey="<?php echo Config::RECAPTCHA_SITE_KEY; ?>"></div>
        <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=<?php echo $language ?>" async
                defer></script>
        <?php
    }

    /**
     * 驗証reacptch
     * @return bool
     */
    public static function checkRecaptcha()
    {
        $recaptcha = new \ReCaptcha\ReCaptcha(Config::RECAPTCHA_SECRET_KEY);
        $resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
        if ($resp->isSuccess()) {
            return true;
        } else {
            return false;
        }
    }


    public static function generateId($idLen = 8){
        $id = '';
        $word = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';//字典檔 你可以將 數字 0 1 及字母 O L 排除
        $len = strlen($word);//取得字典檔長度
        for($i = 0; $i < $idLen; $i++){
            $id .= $word[rand() % $len];
        }
        return $id;
    }

    public static function ArrayKeyFirst($array)
    {
        if (is_array($array) && count($array)>0 ) {
            foreach ($array as $k => $v) {
                return $k;
            }
        }
        return false;
    }

}