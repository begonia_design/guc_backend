<?php


namespace lib;


class SessionDb implements \SessionHandlerInterface
{

    /**
     * @access private
     * @var string session名
     */
    private $_sessionName;
    /**
     * @const 過期時間
     */
    const SESSION_EXPIRE = 1800;

    const SESSION_TABLE = Config::SESSION_TABLE;

    /**
     * 開啟
     * @access public
     * @param $sessionSavePath
     * @param $sessionName
     * @return integer
     */
    public function open($sessionSavePath, $sessionName) {
        $this->_sessionName = $sessionName;
        return 0;
    }
    /**
     * 關閉
     * @access public
     * @return integer
     */
    public function close() {
        return 0;
    }

    /**
     * 讀取session
     * @access public
     * @param $sessionId
     * @return string
     */
    public function read($sessionId) {
        $sql = "SELECT * FROM ".self::SESSION_TABLE." WHERE sid = :sessionId AND UNIX_TIMESTAMP(expiration) + :sessionExpire > UNIX_TIMESTAMP(NOW())";
        $session = Db::getBindRowData($sql, array(':sessionId'=>$sessionId, ':sessionExpire'=>self::SESSION_EXPIRE));
        if($session == false) {
            return "";
        }
        $query = "UPDATE ".self::SESSION_TABLE." SET expiration = CURRENT_TIMESTAMP() WHERE sid = :sessionId";
        Db::connect()->prepare($query)->execute(array(":sessionId" => $sessionId));
        return $session['value'];
    }

    /**
     * 寫入session
     * @access public
     * @param $sessionId
     * @param $sessionData
     * @return integer
     */
    public function write($sessionId, $sessionData) {
        $query = "REPLACE INTO ".self::SESSION_TABLE." (sid, value) value (:sessionId, :sessionData)";
        $rs = Db::connect()->prepare($query);
        $result = $rs->execute(array(":sessionId" => $sessionId, ":sessionData" => $sessionData));
        if($result){
            return 0;
        }
        else{
            return 1;
        }
    }

    /**
     * 銷魂session
     * @access public
     * @param $sessionId
     * @return integer
     */
    public function destroy($sessionId) {
        Db::bindDelete(self::SESSION_TABLE, "sid = :sid", array(":sid" => $sessionId));
        return 0;
    }
    /**
     * 垃圾回收
     * @access public
     * @param string $maxlifetime session 最長生存時間
     * @return integer
     */
    public function gc($maxlifetime) {
        $query = "DELETE FROM ".self::SESSION_TABLE." WHERE UNIX_TIMESTAMP(expiration) < UNIX_TIMESTAMP(NOW()) - :ex";
        Db::connect()->prepare($query)->execute(array(":ex" => self::SESSION_EXPIRE));
        return 0;
    }
}