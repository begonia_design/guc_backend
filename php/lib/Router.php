<?php

namespace lib;

use controller\Controller;
use Latte;

class Router
{
    private static $urlArr = array();

    private static $siteLang = Config::SITE_LANG;

    private static $lang = '';

    private static $sessionTable = Config::SESSION_TABLE;

    public static function router()
    {
        self::$urlArr = explode('/', $_SERVER['REQUEST_URI']);

        if (self::$urlArr[1] == 'api') {
            self::api();
        } else {
            if (isset(self::$siteLang[self::$urlArr[1]])) {
                self::$lang = self::$urlArr[1];
            } else {
                header("location:/en/");
                exit;
            }
            self::web();
        }
    }

    private static function web()
    {
        //header("Content-Security-Policy: default-src *; script-src ‘unsafe-inline’");
        //header("Clear-Site-Data: \"cache\", \"cookies\", \"storage\"");
        $pageName = Tools::getPageNameFromUrl();
        if ($pageName == '') {
            $pageName = 'index';
        }

        $controllerClass = Tools::getUpperCamel($pageName);
        Session_start();

        $class = "controller\\{$controllerClass}";
        if (class_exists($class)) {
            $controller = new $class;
        } else {
            $controller = new Controller();
        }

        $controller->setPageName($pageName);

//        if (self::$lang != '') {
//            $templateFile = sprintf("%s/php/template/%s/%s.html",
//                SITE_DIR,
//                self::$lang,
//                $pageName
//            );
//        } else {
//            $templateFile = sprintf("%s/php/template/%s.html",
//                SITE_DIR,
//                $pageName
//            );
//        }


        $templateFile = sprintf("%s/php/template/%s-%s.html",
            SITE_DIR,
            $pageName,
            self::$lang
        );

        if (!is_file($templateFile)) {
            $templateFile = sprintf("%s/php/template/%s.html",
                SITE_DIR,
                $pageName
            );
        }

        if (is_file($templateFile)) {
            header("Content-Type:text/html; charset=utf-8");

            if (self::$sessionTable != '') {
                session_set_save_handler(new SessionDb());
            }

            $latte = new Latte\Engine();
            $latte->setTempDirectory(SITE_DIR. "/php/cache");
//            $latte->addFilter('onTime', function ($startTime, $endTime) {
//                if ($startTime == '' || $endTime == '') {
//                    return false;
//                }
//                if (strtotime($startTime) < time() && time() < strtotime($endTime)) {
//                    return true;
//                }
//                return false;
//            });

            $lang = file_get_contents(SITE_DIR."/php/template/layout/lang.json");
            $langJson = json_decode($lang, true);

            $latte->addFilter('translate', function ($original) use($pageName, $langJson) {
                // here we somehow create $translated from $original
                $translateString = $original;
                if (isset($langJson["common"][$original."_".self::$lang])) {
                    $translateString = $langJson["common"][$original."_".self::$lang];
                }else if (isset($langJson["common"][$original])) {
                    $translateString = $langJson["common"][$original];
                }

                if (isset($langJson[$pageName][$original."_".self::$lang])) {
                    $translateString = $langJson[$pageName][$original."_".self::$lang];
                }else if (isset($langJson[$pageName][$original])) {
                    $translateString = $langJson[$pageName][$original];
                }
                return html_entity_decode($translateString);
            });


            $latte->addFilter('dateBetween', function (string $date1, string $date2): string {

                if (Router::getLang() == 'tw' || Router::getLang() == 'cn') {
                    $y1 = date("Y", strtotime($date1));
                    $y2 = date("Y", strtotime($date2));
                    if (Router::getLang() == 'tw') {
                        $y1 = $y1 - 1911;
                        $y2 = $y2 - 1911;
                    }
                    if ($date1 == $date2) {
                        return date("{$y1}/m/d",strtotime($date1));
                    }
                    $d1 = date("d",strtotime($date1));
                    $d2 = date("d",strtotime($date2));
                    $m1 = date("m",strtotime($date1));
                    $m2 = date("m",strtotime($date2));

                    if (date("Y-m",strtotime($date1)) == date("Y-m",strtotime($date2))) {
                        return "{$y1}/{$m1}/{$d1}-{$d2}";
                    }
                    if ($y1 == $y2) {
                        return "{$y1}/{$m1}/{$d1}-{$m2}/{$d2}";
                    }
                    return "{$y1}/{$m1}/{$d1}-{$y2}/{$m2}/{$d2}";
                }


                if ($date1 == $date2) {
                    return date("M. d, Y",strtotime($date1));
                }
                if (date("Y-m",strtotime($date1)) == date("Y-m",strtotime($date2))) {
                    $y = date("Y",strtotime($date1));
                    $d1 = date("d",strtotime($date1));
                    $d2 = date("d",strtotime($date2));
                    $m = date("M",strtotime($date1));
                    return "{$m}. {$d1}-{$d2}, {$y}";
                }
                return date("M. d, Y",strtotime($date1)). "-". date("M. d, Y",strtotime($date2));
            });




            $latte->addFilter('dateFormatTwYear', function($date): string{
                if ($date == "") {
                    return "";
                }
                if (Router::getLang() == 'tw') {
                    $y = date("Y", strtotime($date)) - 1911;
                    return date("{$y}/m/d", strtotime($date));
                }
                return date("Y/m/d", strtotime($date));
            });

            $latte->addFilter('dateForUpdate', function($date): string{
                if ($date == "") {
                    return "";
                }
                switch(Router::getLang()) {
                    case 'tw':
                        $y = date("Y", strtotime($date)) - 1911;
                        return date("{$y}/m/d", strtotime($date));
                        break;
                    case 'cn':
                        return date("Y/m/d", strtotime($date));
                        break;
                    default:
                        return date(" M. jS, Y", strtotime($date));
                }
            });


            $latte->addFilter('dateTimeFormatByLang', function($date): string{
                $wTxt = ['日', '一', '二', '三', '四', '五', '六'];
                switch(Router::getLang()) {
                    case 'tw':
                        $y = date("Y", strtotime($date)) - 1911;
                        $w = $wTxt[date("w", strtotime($date))];
                        return date("{$y}/m/d({$w}) H:i", strtotime($date));
                        break;
                    case 'cn':
                        $w = $wTxt[date("w", strtotime($date))];
                        return date("Y/m/d({$w}) H:i", strtotime($date));
                        break;
                    default:
                        return date("M. d, Y (D) H:i", strtotime($date));
                }
            });



            $latte->render($templateFile, $controller->getAssign());
        } else {
            header("location:error.php");
            exit;
        }
    }

    private static function api()
    {
        $className = Tools::getUpperCamel(self::$urlArr[2]);
        $method = Tools::getUpperCamel(self::$urlArr[3], false);
        $className = Tools::getUpperCamel($className);
        $method = Tools::getUpperCamel($method);
        $class = "controller\\api\\{$className}";
        if (method_exists($class, $method)) {
            $api = new $class;
            $api->$method();
        }
        header("HTTP/1.1 404 Not Found");
        exit;
    }

    public static function getLang()
    {
        return self::$lang;
    }
}