<?php

namespace lib;


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Mail
{
    private static $errMsg = [];

    public static function phpMail($fromEmail, $fromName, $mailTo, $subject, $msg)
    {
        $success = true;
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=utf-8\r\n";
        $headers .= "From: " . $fromEmail . "<" . $fromName . ">\r\n";
        $subject = "=?utf-8?B?" . base64_encode($subject) . "?=";
        //$headers =  iconv("UTF-8" ,"Big5" , $headers);
        if (is_array($mailTo)) {
            $success = true;
            foreach ($mailTo as $mail) {
                if (!mail($mail, $subject, $msg, $headers)){
                    $success = false;
                    self::$errMsg[] = error_get_last()['message'];
                }
            }
        } else {
            if (!mail($mailTo, $subject, $msg, $headers)){
                $success = false;
                self::$errMsg[] = error_get_last()['message'];
            }
        }
        return $success;
    }


    public static function send($mailTo, $subject, $msg)
    {
        $config = self::getSetting();
        if ($config['type'] != 0) {
            return self::phpMailer($config, $mailTo, $subject, $msg);
        } else {
            return self::phpMail($config['from'], $config['from_name'], $mailTo, $subject, $msg);
        }

    }

    public static function mailLayout($filePath, $paramArray)
    {
        //讀取mail內容
        $file = fopen($filePath,"r");
        $mailContent = fread($file, 9999);
        fclose($file);

        $replaceKeys = array_keys($paramArray);

        //套入mail
        $msg = str_replace($replaceKeys, $paramArray, $mailContent);
        return $msg;
    }

    public static function LayoutSend($mailTo, $subject, $filePath, $paramArray)
    {
        return self::send($mailTo, $subject, self::mailLayout($filePath, $paramArray));
    }

    private static function getSetting()
    {
        $sql = "SELECT * FROM admin_mail WHERE id = 1";
        return Db::getRowData($sql) ;
    }

    /**
     * @param $config = self::getSetting()
     * @param $mailTo
     * @param $subject
     * @param $msg
     * @return bool
     */
    public static function phpMailer($config, $mailTo, $subject, $msg)
    {
        $success = true;
        $mail = new PHPMailer(true);
        try {
            //Server settings
            //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            $mail->isSMTP();// Send using SMTP
            $mail->SMTPAutoTLS= $config['auto_tls'];
            $mail->Host       = $config['host'];                    // Set the SMTP server to send through
            $mail->SMTPAuth   = ($config['username'] == '')?false:true;                                   // Enable SMTP authentication
            $mail->Username   = $config['username'];                    // SMTP username
            $mail->Password   = $config['password'];                               // SMTP password
            $mail->SMTPSecure = $config['smtp_secure'];         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->Port       = $config['port'];                                    // TCP port to connect to

            //Recipients
            $mail->setFrom($config['from_email'], $config['from_name']);

            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = "=?utf-8?B?" . base64_encode($subject) . "?=";;
            $mail->Body    = $msg;

            if (is_array($mailTo)) {
                foreach ($mailTo as $to) {
                    $mail->AddAddress($to);
                    if (!$mail->Send()) {
                        $success = false;
                        self::$errMsg[] = $mail->ErrorInfo;
                        \Logger::getLogger(__METHOD__)->error($mail->ErrorInfo);
                    }
                }
            } else {
                $mail->AddAddress($mailTo);
                if (!$mail->Send()) {
                    $success = false;
                    self::$errMsg[] = $mail->ErrorInfo;
                    \Logger::getLogger(__METHOD__)->error($mail->ErrorInfo);
                }
            }
        } catch (Exception $e) {
            $success = false;
            self::$errMsg[] = $mail->ErrorInfo;
            \Logger::getLogger(__METHOD__)->error($mail->ErrorInfo);
        }

        return $success;
    }

    public static function getErrMsg()
    {
        return self::$errMsg;
    }

}