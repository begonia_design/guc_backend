<?php
namespace lib;

use Logger;

class Restful
{
    private static $httpVersion = "HTTP/1.1";

    private static $curlOption = [];

    /**
     * 取得 request 內容
     * @return mixed
     */
    public static function getRequest()
    {
        return json_decode(file_get_contents('php://input'), true);
    }

    /**
     * 取得 header 值
     * @param $name
     * @return string
     */
    public static function getHeader($name)
    {
        if (function_exists('apache_request_headers')) {
            return isset(apache_request_headers()[$name]) ? apache_request_headers()[$name] : '';
        }

        $name = 'HTTP_' . strtoupper(str_replace('-', '_', $name));

        return isset($_SERVER[$name]) ? $_SERVER[$name] : '';
    }

    /**
     * 取得訪問頁面時的請求方法
     * @return string
     */
    public static function getRequestMethod()
    {
        return strtolower($_SERVER['REQUEST_METHOD']);
    }


    /**
     * 回傳成功的預設格式
     */
    public static function responseSuccess($msg = null)
    {
        $data = ['status' => 'success'];
        if ($msg) {
            $data['data'] = $msg;
        }
        self::response($data, 200);
    }

    /**
     * 回傳錯誤的預設格式
     */
    public static function responseError($statusCode = 500, $msg = null)
    {
        $data = ['status' => 'error'];
        if ($msg) {
            $data['data'] = $msg;
        }
        self::response($data, $statusCode);
    }

    /**
     * 回傳格式
     */
    public static function response($msg, $statusCode)
    {
        $contentType = 'application/json';
        $requestBody = json_encode($msg);

        self::setHttpHeaders($contentType, $statusCode);
        echo $requestBody;
        exit;
    }


    public static function setHttpHeaders($contentType, $statusCode)
    {
        header('Access-Control-Allow-Origin:*');
        header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
        header('Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept');

        $statusMessage = self::getStatusCodeMessage($statusCode);
        header(self::$httpVersion . " " . $statusCode . " " . $statusMessage);
        header("Content-Type:" . $contentType);
    }

    /**
     * 定義http status code
     */
    public static function getStatusCodeMessage($statusCode)
    {

        $codes = Array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported'
        );

        return (isset($codes[$statusCode])) ? $codes[$statusCode] : '';
    }


    public static function getApi($url, $param, $isJson = true)
    {
        $urlParam = "";
        if ($param && is_array($param)) {
            foreach ($param as $k => $v) {
                $urlParam .= "&{$k}={$v}";
            }
        }
        $serviceUrl = $url . '?' . $urlParam;
        $refererUrl = '';
        $apiMaxExeTime = 3000; // 限制執行的時間

        $curl = curl_init($serviceUrl);

        $header[] = "Accept: application/json";

        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        //curl_setopt($curl, CURLOPT_ENCODING , "gzip");
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        if ($refererUrl) curl_setopt($curl, CURLOPT_REFERER, $refererUrl); //來源

        curl_setopt($curl, CURLOPT_TIMEOUT_MS, $apiMaxExeTime);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if (self::$curlOption && is_array(self::$curlOption)) {
            foreach (self::$curlOption as $k => $v) {
                curl_setopt($curl, $k, $v);
            }
        }
        $response = curl_exec($curl);

        curl_close($curl);

        Logger::getLogger(__METHOD__)->info(json_encode(array(
            'getUrl' => $serviceUrl,
            'response' => $response)));

        if ($response === false) {
            return false;
        }

        if ($isJson) {
            return json_decode($response, true);
        } else {
            return $response;
        }
    }

    public static function postApi($url, $param)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($param));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if (self::$curlOption && is_array(self::$curlOption)) {
            foreach (self::$curlOption as $k => $v) {
                curl_setopt($ch, $k, $v);
            }
        }
        $response = curl_exec($ch);
        curl_close($ch);

        Logger::getLogger(__METHOD__)->info(json_encode(array(
            'PostUrl' => $url,
            'request' => $param,
            'response' => json_decode($response, true))));

        if ($response === false) {
            return false;
        }
        return json_decode($response, true);
    }

    public static function setCurlOption($curlOption) {
        self::$curlOption = $curlOption;
    }
}