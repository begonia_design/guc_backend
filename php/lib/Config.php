<?php
namespace lib;

class Config
{
    const SITE_NAME = 'GUC';
    const SITE_URL = 'https://guc.demo-site.pro';

    const ADMIN_LOGO = "<img src='/en/images/logo_g_white.png' height='30'>";
    const ADMIN_TITLE = "GUC 品牌官網後端管理系統";
    const ADMIN_FAVICON = "../en/images/favicon.ico";
    const ADMIN_COPYRIGHT = "© 2021 Global Unichip Corp. All rights reserved. Design by BEGONIA Design.";

    const SITE_LANG = ['en'=>'英', 'tw'=>'繁中', 'cn'=>'簡中'];

    const UPLOAD_DIR = 'upload/';
    const UPLOAD_TEMP = self::UPLOAD_DIR . 'temp/';
    const UPLOAD_SIZE = 10; //Mb

    //資料庫設定
    const DB_HOST = 'localhost';
    const DB_PORT = '3307';
    const DB_NAME = 'guc';
    const DB_USERNAME = 'guc_user';
    const DB_PASSWORD = '!QAZ2wsx3e';

    //
    const SESSION_TABLE = '';


    //LOG設定
    const LOG = [
        'appenders' => [
            'default' => [
                'class' => 'LoggerAppenderPDO',
                'params' => [
                    'dsn' => "mysql:host=". self::DB_HOST .":".self::DB_PORT.";dbname=" . self::DB_NAME,
                    'user' => self::DB_USERNAME,
                    'password' => self::DB_PASSWORD,
                    'table' => 'admin_log',
                ],
            ],
        ],
        'rootLogger' => [
            'appenders' => ['default'],
        ],
    ];


    //auth
//    const FB_APP_ID = '';
//    const FB_APP_SECRET = '';
//    const FB_CALLBACK_URL = self::SITE_URL.'/fb.php';
//
//    const GOOGLE_JSON_PATH = SITE_DIR. '/php/credentials.json';
//    const GOOGLE_CALLBACK_URL = self::SITE_URL.'/google.php';


    const RECAPTCHA_SITE_KEY = '6LdGBKcbAAAAACzgyAz6cFAC-gnDGXN1OrgqWmj5';
    const RECAPTCHA_SECRET_KEY = '6LdGBKcbAAAAAG3A8c-zT6fX4PU7RLJo6mZmjP_1';

    const GOOGLE_SEARCH_EN = '6f397d437f596a7f6';
    const GOOGLE_SEARCH_TW = 'ba24f8edb43633520';
    const GOOGLE_SEARCH_CN = '8cd0ba06ae351b671';
    const GOOGLE_SEARCH_API_KEY = 'AIzaSyAEO9Y_Pj7TV0jT3705cqoHVrhtKVKue6E';

    public static function getSearchCode($lang): string
    {
        switch($lang){
            case 'tw':
                return self::GOOGLE_SEARCH_TW;
            default:
                return self::GOOGLE_SEARCH_EN;
        }
    }

    const CURL_OPTION = [];
//    const CURL_OPTION = [
//        CURLOPT_PROXY => 'http://172.20.0.16:8080',
//        CURLOPT_PROXY_CAINFO => '/etc/ssl/cert.pem'
//    ];
}