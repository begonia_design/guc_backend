<?php
namespace lib;

class PageBar
{
    private $pageCount = 10;    //預設一頁筆數
    private $count;    //所有筆數
    private $start;
    private $end;
    private $index;    //目前分頁
    private $pages;    //所有頁數
    private $urlName;        //分頁參數
    private $urlOther;        //其它參數名稱
    private $pageName;        //此頁名稱
    private $sqlLimit;        //分頁sql語法

    function __construct($pageCount = 10, $pageName = null , $urlName = 'page')
    {
        //網頁名稱
        if ($pageName == null) {
            $this->pageName = Tools::getPageNameFromUrl();
        } else {
            $this->pageName = $pageName;
        }


        //一頁筆數
        $this->pageCount = $pageCount;
        //換頁參數名稱
        $this->urlName = $urlName;

        //網址參數轉為陣列
        $urlArr = array();
        parse_str($_SERVER['QUERY_STRING'], $urlArr);
        //排除換頁參數
        unset($urlArr[$this->urlName]);

        //陣列轉為網址，取得其他網址參數
        $this->urlOther = http_build_query($urlArr);

        //取得目前頁數
        $this->index = (is_numeric($_GET[$this->urlName]) && $_GET[$this->urlName] != "") ? $_GET[$this->urlName] : 1;
    }


    //傳進SQL語法，計算分頁並回傳處理後SQL
    public function sql($sql, $bindValue = null)
    {
        $num = stripos($sql, 'FROM');
        $countSql = "SELECT COUNT(1) AS C " . substr($sql, $num);
        $rs = Db::connect()->prepare($countSql);
        $rs->execute($bindValue);

        $this->count = $rs->fetchColumn();

        //計算總頁數
        $this->pages = ceil($this->count / $this->pageCount);
        //如目前頁數大於總頁數，目前頁數設為總頁數
        $this->index = ($this->index > $this->pages) ? $this->pages : $this->index;
        //計算開始資料筆數
        $this->start = ($this->index - 1) * $this->pageCount;
        //
        $this->end = $this->start + $this->pageCount;
        $this->end = ($this->end > $this->count) ? $this->count : $this->end;
        //建立分頁SQL語法
        $this->sqlLimit = ($this->count > 0) ? " LIMIT {$this->start}, {$this->pageCount}" : '';

        return $sql . $this->sqlLimit;
    }

    //資料總筆數
    public function getCount()
    {
        return $this->count;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function getEnd()
    {
        return $this->end;
    }

    //輸出分頁HTML語法
    public function output($showSelect = 3)
    {

        $url = $this->pageName . '.php?' . $this->urlOther . '&' . $this->urlName . '=';

        if ($this->index == 1) {
            $first = sprintf(self::PAGE_BAR_FIRST_ACTIVE, ($url . "1"), 1);
        } else {
            $first = sprintf(self::PAGE_BAR_FIRST, ($url . "1"), 1);
        }

        if ($this->index > 1) {
            $prev = sprintf(self::PAGE_BAR_PREV, ($url . ($this->index - 1)), ($this->index - 1));
        } else {
            $prev = sprintf(self::PAGE_BAR_PREV_ACTIVE, 'javascript:;', ($this->index - 1));
        }

        if ($this->index > 10 && $this->pages > 10) {
            $prev10 = sprintf(self::PAGE_BAR_PREV_10, ($url . ($this->index - 10)), ($this->index - 10));
        } else {
            $prev10 = sprintf(self::PAGE_BAR_PREV_10_ACTIVE, 'javascript:;', ($this->index - 10));
        }

        if ($this->index < $this->pages - 10  && $this->pages > 10) {
            $next10 = sprintf(self::PAGE_BAR_NEXT_10, ($url . ($this->index + 10)), ($this->index + 10));
        } else {
            $next10 = sprintf(self::PAGE_BAR_NEXT_10_ACTIVE, 'javascript:;', ($this->index + 10));
        }

        if ($this->index == $this->pages) {
            $last = sprintf(self::PAGE_BAR_LAST_ACTIVE, ($url . $this->pages), $this->pages);
        } else {
            $last = sprintf(self::PAGE_BAR_LAST, ($url . $this->pages), $this->pages);
        }

        if ($this->index < $this->pages) {
            $next = sprintf(self::PAGE_BAR_NEXT, ($url . ($this->index + 1)), $this->index + 1);
        } else {
            $next = sprintf(self::PAGE_BAR_NEXT_ACTIVE, 'javascript:;', $this->index + 1);
        }

        $start = ($this->index - $showSelect < 1) ? 1 : $this->index - $showSelect;
        $end = ($this->index + $showSelect > $this->pages) ? $this->pages : $this->index + $showSelect;
        $list = '';

        for ($i = $start; $i <= $end; $i++) {
            if ($i == $this->index) {
                $list .= sprintf(self::PAGE_BAR_LIST_INDEX, $i);
            } else {
                $list .= sprintf(self::PAGE_BAR_LIST, ($url . $i), $i, $i);
            }
        }

        $html = $prev . $prev10 . $list . $next10 . $next;

        return $html;
    }

    const PAGE_BAR_FIRST = '';
    const PAGE_BAR_FIRST_ACTIVE = '';
    const PAGE_BAR_LAST = '';
    const PAGE_BAR_LAST_ACTIVE = '';

    const PAGE_BAR_PREV_10 = '';
    const PAGE_BAR_PREV_10_ACTIVE = '';

    const PAGE_BAR_NEXT_10 = '';
    const PAGE_BAR_NEXT_10_ACTIVE = '';

    const PAGE_BAR_PREV = '<li class="pagination-previous"><a href="%s" aria-label="上一頁"><span class="show-for-sr">上一頁</span></a></li>';

    const PAGE_BAR_PREV_ACTIVE = '<li class="pagination-previous"><a href="javascript:;" aria-label="上一頁"><span class="show-for-sr">上一頁</span></a></li>';

    const PAGE_BAR_NEXT = '<li class="pagination-next"><a href="%s" aria-label="下一頁"><span class="show-for-sr">下一頁</span></a></li>';
    const PAGE_BAR_NEXT_ACTIVE = '<li class="pagination-next"><a href="javascript:;" aria-label="下一頁"><span class="show-for-sr">下一頁</span></a></li>';

    const PAGE_BAR_LIST = '<li><a href="%s" aria-label="第%s頁">%s</a></li>';
    const PAGE_BAR_LIST_INDEX = '<li class="current"><span class="show-for-sr">你正在第</span>%s<span class="show-for-sr">頁</span></li>';
}