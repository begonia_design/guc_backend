<?php

namespace lib;

use Phpmailer;
use ReCaptcha;

class Format
{

    /**
     * 轉換成駝峰命名 aaa_bbb => AaaBbb
     * @param $txt
     * @return string
     */
    public static function getUpperCamel($txt)
    {
        $arr = explode("_", $txt);
        $return = "";
        if ($arr) {
            foreach ($arr as $v) {
                $return .= ucfirst($v);
            }
        }
        return $return;
    }


    /**
     * 簡查Email格式是否正確
     * @param $email
     * @return int
     */
    public static function checkEmail($email)
    {
        return preg_match('/^[_.0-9a-z-]+@([0-9a-z-]+.)+[a-z]{2,3}$/', $email);
    }


    /**
     * 簡查手機格式是否正確
     * @param $phone
     * @return int
     */
    public static function checkPhoneNumber($phone)
    {
        return preg_match('/^[0][9][0-9]{8}$/', $phone);
    }

    public static function checkPhone($code, $phone)
    {
        return true;
    }

    /**
     * 身份証字號
     * @param $id
     * @return bool
     */
    public static function checkTwId($id)
    {
        $id = strtoupper($id);
        //建立字母分數陣列
        $headPoint = array(
            'A' => 1, 'I' => 39, 'O' => 48, 'B' => 10, 'C' => 19, 'D' => 28,
            'E' => 37, 'F' => 46, 'G' => 55, 'H' => 64, 'J' => 73, 'K' => 82,
            'L' => 2, 'M' => 11, 'N' => 20, 'P' => 29, 'Q' => 38, 'R' => 47,
            'S' => 56, 'T' => 65, 'U' => 74, 'V' => 83, 'W' => 21, 'X' => 3,
            'Y' => 12, 'Z' => 30
        );
        //建立加權基數陣列
        $multiply = array(8, 7, 6, 5, 4, 3, 2, 1);
        //檢查身份字格式是否正確
        if (preg_match("/^[a-zA-Z][1-2][0-9]+$/", $id) AND strlen($id) == 10) {
            //切開字串
            $stringArray = str_split($id);
            //取得字母分數(取頭)
            $total = $headPoint[array_shift($stringArray)];
            //取得比對碼(取尾)
            $point = array_pop($stringArray);
            //取得數字部分分數
            $len = count($stringArray);
            for ($j = 0; $j < $len; $j++) {
                $total += $stringArray[$j] * $multiply[$j];
            }
            //計算餘數碼並比對
            $last = (($total % 10) == 0) ? 0 : (10 - ($total % 10));
            if ($last != $point) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * 公司統編
     * @param $companyId
     * @return bool
     */
    public static function checkCompanyId($companyId)
    {
        if (!preg_match('/^[0-9]{8}$/', $companyId)) {
            return false;
        }
        $tbNum = array(1, 2, 1, 2, 1, 2, 4, 1);
        $intSum = 0;
        for ($i = 0; $i < 8; $i++) {
            $intMultiply = $companyId[$i] * $tbNum[$i];
            $intAddition = (floor($intMultiply / 10) + ($intMultiply % 10));
            $intSum += $intAddition;
        }
        return ($intSum % 10 == 0) || ($intSum % 10 == 9 && $companyId[6] == 7);
    }


    /**
     * 發票 愛心碼
     * @param $loveCode
     * @return false|int
     */
    public static function checkLoveCode($loveCode)
    {
        return preg_match('/^([xX]{1}[0-9]{2,6}|[0-9]{3,7})$/', $loveCode);
    }

    /**
     * 自然人憑証
     * @param $citizenId
     * @return false|int
     */
    public static function checkCitizen($citizenId)
    {
        return preg_match('/^[a-zA-Z]{2}\d{14}$/', $citizenId);
    }

    /**
     * 手機載具條碼
     * @param $phoneId
     */
    public static function checkPhoneId($phoneId)
    {
        return preg_match('/^\/{1}[0-9a-zA-Z+-.]{7}$/', $phoneId);
    }


    /**
     * 輸出限制字串長度
     * @param $txt
     * @param int $num
     * @param string $addString
     * @return string
     */
    public static function cutString($txt, $num = 20, $addString = "...")
    {
        if ($txt != '') {
            $tt = (mb_strlen($txt, "UTF8") > $num) ? $addString : "";
            return mb_substr(strip_tags($txt), 0, $num, 'UTF-8') . $tt;
        }
    }


    /**
     * 取得星期
     * @param $datetime
     * @param string $txt
     * @return string
     */
    public static function getWeekday($datetime, $txt = '星期')
    {
        $weekday = date('w', strtotime($datetime));
        $week = array('日', '一', '二', '三', '四', '五', '六');
        return $txt . $week[$weekday];
    }


    /**
     * 取得IP
     * @return mixed
     */
    public static function getIp()
    {
        if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else {
            $ip = $_SERVER["REMOTE_ADDR"];
        }
        return $ip;
    }

    public static function replaceSymbol($string, $symbol, $begin_num = 0, $end_num = 0){
        $string_length = strlen($string);
        $begin_num = (int)$begin_num;
        $end_num = (int)$end_num;
        $string_middle = '';

        $check_reduce_num = $begin_num + $end_num;

        if($check_reduce_num >= $string_length){
            for ($i=0; $i < $string_length; $i++) {
                $string_middle .= $symbol;
            }
            return $string_middle;
        }

        $symbol_num = $string_length - ($begin_num + $end_num);
        $string_begin = substr($string, 0,$begin_num);
        $string_end = substr($string, $string_length-$end_num);

        for ($i=0; $i < $symbol_num; $i++) {
            $string_middle .= $symbol;
        }

        return $string_begin.$string_middle.$string_end;
    }


}