<?php

namespace lib;

use Facebook\Exceptions\FacebookSDKException;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Facebook;
use Google_Client;
use Google_Service_Oauth2;

class Auth
{

    private static function fbInit()
    {
        try {
            $fb = new Facebook([
                'app_id' => Config::FB_APP_ID,
                'app_secret' => Config::FB_APP_SECRET,
                'default_graph_version' => 'v2.2',
            ]);
            return $fb;
        } catch (FacebookSDKException $e) {
            \Logger::getLogger(__METHOD__)->error($e->getMessage());
        }
        return false;
    }

    public static function getFbLoginUrl()
    {
        $fb = self::fbInit();
        if ($fb) {
            $helper = $fb->getRedirectLoginHelper();
            $permissions = ['email'];
            $loginUrl = $helper->getLoginUrl(Config::FB_CALLBACK_URL, $permissions);
            return $loginUrl;
        }
    }

    public static function getFbData()
    {
        $fb = self::fbInit();

        if ($fb) {
            try {
                $helper = $fb->getRedirectLoginHelper();
                $accessToken = $helper->getAccessToken();
            } catch (FacebookResponseException $e) {
                \Logger::getLogger(__METHOD__)->error($e->getMessage());
                exit;
            } catch (FacebookSDKException $e) {
                \Logger::getLogger(__METHOD__)->error($e->getMessage());
                exit;
            }

            if (!isset($accessToken)) {
                if ($helper->getError()) {
                    $err = "Error: " . $helper->getError() . "\n";
                    $err .= "Error Code: " . $helper->getErrorCode() . "\n";
                    $err .= "Error Reason: " . $helper->getErrorReason() . "\n";
                    $err .= "Error Description: " . $helper->getErrorDescription() . "\n";
                    \Logger::getLogger(__METHOD__)->error($err);
                } else {
                    \Logger::getLogger(__METHOD__)->error('Bad request');
                }
                exit;
            }

            $oAuth2Client = $fb->getOAuth2Client();
            $tokenMetadata = $oAuth2Client->debugToken($accessToken);

            try {
                $tokenMetadata->validateAppId(Config::FB_APP_ID);
                $tokenMetadata->validateExpiration();
            } catch (FacebookSDKException $e) {
                \Logger::getLogger(__METHOD__)->error($e->getMessage());
                exit;
            }

            if (!$accessToken->isLongLived()) {
                try {
                    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
                } catch (FacebookSDKException $e) {
                    \Logger::getLogger(__METHOD__)->error($e->getMessage());
                    exit;
                }
            }

            $fb->setDefaultAccessToken($accessToken);
            try {
                $response = $fb->get('/me?locale=en_US&fields=id,name,email');
                $userNode = $response->getGraphUser();
                return $userNode;
            } catch (FacebookSDKException $e) {
                \Logger::getLogger(__METHOD__)->error($e->getMessage());
            }
        }
        return false;
    }

    private static function googleInit()
    {
        $gclient = new Google_Client();
        try {
            $gclient->setAuthConfig(Config::GOOGLE_JSON_PATH);
        } catch (\Google_Exception $e) {
            \Logger::getLogger(__METHOD__)->error($e->getMessage());
            return false;
        }
        $gclient->setAccessType('offline'); // offline access
        $gclient->setIncludeGrantedScopes(true); // incremental auth
        $gclient->addScope([Google_Service_Oauth2::USERINFO_EMAIL, Google_Service_Oauth2::USERINFO_PROFILE]);
        $gclient->setRedirectUri(Config::GOOGLE_CALLBACK_URL); // 寫憑證設定：「已授權的重新導向 URI 」的網址
        return $gclient;
    }

    public static function getGoogleLoginUrl()
    {
        $g = self::googleInit();
        if ($g) {
            return $g->createAuthUrl();
        }
    }


    public static function getGoogleData()
    {
        $gclient = self::googleInit();
        if ($gclient) {
            $gclient->authenticate($_GET['code']);
            $token = $gclient->getAccessToken(); // 取得 Token
            // $token data: [
            // 'access_token' => string
            // 'expires_in' => int 3600
            // 'scope' => string 'https://www.googleapis.com/auth/userinfo.email openid https://www.googleapis.com/auth/userinfo.profile' (length=102)
            // 'created' => int 1550000000
            // ];
            $gclient->setAccessToken($token); // 設定 Token

            $oauth = new Google_Service_Oauth2($gclient);
            $profile = $oauth->userinfo->get();
            return $profile;
        }
        return false;
    }
}