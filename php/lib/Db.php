<?php

namespace lib;

use Matrix\Exception;
use PDO;
use PDOException;

class Db
{
    /**
     * @var \PDO
     */
    private static $connect;

    /**
     * @var \PDOStatement
     */
    private $rs;

    function __construct($sql, array $bindValue = null)
    {
        $db = self::connect();
        if ($bindValue == null) {
            $this->rs = $db->query($sql);
        } else {
            $this->rs = $db->prepare($sql);
            $this->rs->execute($bindValue);
            if(!$this->rs) die(json_encode($db->errorInfo()));
        }
        return $this;
    }

    public static function insert($tableName, array $arrayData)
    {
        $fieldArray = array();
        $valueArray = array();
        $bindValueArray = array();
        foreach ($arrayData as $field => $value) {
            $bindValue = ':F_' . $field;
            $fieldArray[] = $field;
            $valueArray[] = $bindValue;
            $bindValueArray[$bindValue] = $value;
        }
        $fieldQuery = join(',', $fieldArray);
        $valueQuery = join(',', $valueArray);
        $sql = "INSERT INTO {$tableName} ({$fieldQuery}) VALUES ({$valueQuery})";
        try {
            $rs = self::connect()->prepare($sql);
            $rs->execute($bindValueArray);
        } catch (PDOException $e) {
            \Logger::getLogger(__METHOD__)->error($e->getMessage(). " ,sql:". $sql);
            return false;
        }
        return self::connect()->lastInsertId();
    }


    public static function update($tableName, array $arrayData, $whereQuery, array $bindValue = null )
    {
        $setArray = array();
        $bindValueArray = array();
        foreach($arrayData as $field => $value){
            $bind = ":F{$field}";
            $setArray[] = $field . " = " . $bind;
            $bindValueArray[$bind] = $value;
        }
        $setQuery = join("," , $setArray);

        $sql = "UPDATE {$tableName} SET {$setQuery} WHERE {$whereQuery}";
        $db = self::connect();
        try {
            $rs = $db->prepare($sql);
            if ($bindValue !== null) {
                $bindValueArray = array_merge($bindValueArray, $bindValue);
            }
            return $rs->execute($bindValueArray);
        } catch (PDOException $e) {
            \Logger::getLogger(__METHOD__)->error($e->getMessage());
            return false;
        }
    }


    public static function bindDelete($table,$whereSql,$bindValue)
    {
        $sql = "DELETE FROM " . $table . " WHERE " . $whereSql;
        $db = self::connect();
        try {
            $rs = $db->prepare($sql);
            return $rs->execute($bindValue);
        } catch (PDOException $e) {
            \Logger::getLogger(__METHOD__)->error($e->getMessage());
            return false;
        }
    }


    public static function getRowArray($sql, $key='id', $value=null)
    {
        $data = array();
        $db = self::connect();
        try {
            $rs = $db->query($sql);
            while ($row = $rs->fetch(PDO::FETCH_ASSOC)) {
                $data[$row[$key]] = ($value == null) ? $row : $row[$value];
            }
            return $data;
        }catch (PDOException $e) {
            \Logger::getLogger(__METHOD__)->error($e->getMessage());
            return false;
        }
    }

    public static function getBindRowArray($sql, array $bindValue, $key, $value=null)
    {
        $data = array();
        $db = self::connect();
        try {
            $rs = $db->prepare($sql);
            $rs->execute($bindValue);
            while ($row = $rs->fetch(PDO::FETCH_ASSOC)) {
                $data[$row[$key]] = ($value == null) ? $row : $row[$value];
            }
            return $data;
        }catch (PDOException $e) {
            \Logger::getLogger(__METHOD__)->error($e->getMessage());
            return false;
        }
    }

    public static function getRowData($sql, $value=null)
    {
        $db = self::connect();
        try {
            $rs = $db->query($sql);
            $row = $rs->fetch(PDO::FETCH_ASSOC);
            return ($value == null) ? $row : $row[$value];
        } catch (PDOException $e) {
            \Logger::getLogger(__METHOD__)->error($e->getMessage());
            return false;
        }
    }

    public static function getBindRowData($sql,array $bindValue,$value=null)
    {
        $db = self::connect();
        try {
            $rs = $db->prepare($sql);
            if (!$rs) die(json_encode($db->errorInfo()));
            $rs->execute($bindValue);
            $row = $rs->fetch(PDO::FETCH_ASSOC);
            return ($value == null) ? $row : $row[$value];
        }catch (PDOException $e) {
            \Logger::getLogger(__METHOD__)->error($e->getMessage());
            return false;
        }
    }

    public static function connect()
    {
        if (self::$connect == null) {
            self::$connect = self::newPDO();
        }
        return self::$connect;
    }

    public static function newPDO()
    {
        $host = Config::DB_HOST;
        $port = Config::DB_PORT;
        $dbName = Config::DB_NAME;
        $user = Config::DB_USERNAME;
        $password = Config::DB_PASSWORD;

        $dsn = "mysql:host={$host};dbname={$dbName};charset=UTF8";
        $dsn .= ($port != '') ? ";port={$port}" : "";
        try {
            $conn = new PDO($dsn, $user, $password, array(PDO::ATTR_PERSISTENT => true));
            $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $conn->exec("set names utf8");
            return $conn;
        } catch (PDOException $e) {
            \Logger::getLogger(__METHOD__)->error($e->getMessage());
            exit;
        }
    }

    public static function query($sql, array $bindValue = null)
    {
        $db = new Db($sql, $bindValue);
        return $db;
    }

    public function getArray($key = null, $fieldName = null)
    {
        if ($key != null) {
            $key = $this->toUnderLine($key);
        }

        if ($fieldName != null) {
            $fieldName = $this->toUnderLine($fieldName);
        }

        $data = array();
        while($row = $this->rs->fetch(PDO::FETCH_ASSOC)) {
            $dataValue = null;
            if ($fieldName == null) {
                foreach ($row as $field => $value) {
                    $dataValue[$this->toUpperCamel($field)] = $value;
                }
            } else {
                $dataValue = $row[$fieldName];
            }

            if ($key == null) {
                $data[] = $dataValue;
            } else {
                $data[$row[$key]] = $dataValue;
            }
        }
        return $data;
    }


    public function get($fieldName = null)
    {
        $dataValue = null;

        if ($fieldName != null) {
            $fieldName = $this->toUnderLine($fieldName);
        }

        $row = $this->rs->fetch(PDO::FETCH_ASSOC);
        if ($fieldName == null) {
            foreach ($row as $field => $value) {
                $dataValue[$this->toUpperCamel($field)] = $value;
            }
        } else {
            $dataValue = $row[$fieldName];
        }

        return $dataValue;
    }


    public static function close()
    {
        self::$connect = null;
    }





    private static function toUnderLine($txt)
    {
        return preg_replace_callback("/[A-Z]/", function($m){
            return "_". strtolower($m[0]);
        }, $txt);
    }

    private static function toUpperCamel($txt)
    {
        $arr = explode("_", $txt);
        $return = "";
        if ($arr) {
            foreach ($arr as $v) {
                $return .= ucfirst($v);
            }
        }
        return lcfirst($return);
    }
}